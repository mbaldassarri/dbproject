﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusicArounD
{
    public partial class testform : DBEngineForm
    {
        public testform()
        {
            InitializeComponent();
        }
        private void updateData()
        {
            if (isValidTelephoneNumber(textBox1.Text, textBox2.Text, false))
            {
                flowLayoutPanel1.BackColor = Color.Transparent;
                label1.Text = "VALID";
            } 
            else
            {
                flowLayoutPanel1.BackColor = Color.Red;
                label1.Text = "BAD";
            }
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            updateData();
        }
        private bool isValidTelephoneNumber(string strIn, string regexp, bool isNullAdmitted)
        {
            try
            {
                return (Regex.IsMatch(strIn, @regexp) || (isNullAdmitted && strIn.Equals("")));

            }
            catch
            {
                return false;
            }

        }
        private void testform_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            updateData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            madEngine.test();
        }
    }
}
