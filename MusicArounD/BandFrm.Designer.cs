﻿using System.Windows.Forms;
namespace MusicArounD
{
    partial class BandFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanelForm = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelComponentiBand = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelComponentiBandDisponibili = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxFiltroMusicistiDisponibili = new System.Windows.Forms.TextBox();
            this.checkedListBoxMusicistiDisponibili = new System.Windows.Forms.CheckedListBox();
            this.tableLayoutPanelComponentiBandBottoni = new System.Windows.Forms.TableLayoutPanel();
            this.deleteMusicistaFromBand = new System.Windows.Forms.Button();
            this.addMusicistaToBand = new System.Windows.Forms.Button();
            this.tableLayoutPanelComponentiBandUtilizzati = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxFiltroMusicistiUtilizzati = new System.Windows.Forms.TextBox();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.listBoxStoricoBand1 = new MusicArounD.ListBoxStoricoBand();
            this.tableLayoutPanelDatiBand = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelButtons = new System.Windows.Forms.TableLayoutPanel();
            this.buttonAction = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonAction_and_new = new System.Windows.Forms.Button();
            this.groupBoxIndirizzo = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelIndirizzo = new System.Windows.Forms.TableLayoutPanel();
            this.comboBoxComune = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxProvincia = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dataFormazione = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNomeBand = new System.Windows.Forms.TextBox();
            this.groupBoxDatiReferente = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelReferente = new System.Windows.Forms.TableLayoutPanel();
            this.groupBoxReferenteInterno = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelReferenteInterno = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxTelefonoReferenteInterno = new System.Windows.Forms.ComboBox();
            this.comboBoxCognomeReferenteInterno = new System.Windows.Forms.ComboBox();
            this.groupBoxReferenteEsterno = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelReferenteEsterno = new System.Windows.Forms.TableLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxCognomeReferenteEsterno = new System.Windows.Forms.TextBox();
            this.textBoxTelefonoReferenteEsterno = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.radioButtonReferenteEsterno = new System.Windows.Forms.RadioButton();
            this.radioButtonReferenteInterno = new System.Windows.Forms.RadioButton();
            this.radioButtonNoReferente = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanelForm.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanelComponentiBand.SuspendLayout();
            this.tableLayoutPanelComponentiBandDisponibili.SuspendLayout();
            this.tableLayoutPanelComponentiBandBottoni.SuspendLayout();
            this.tableLayoutPanelComponentiBandUtilizzati.SuspendLayout();
            this.tableLayoutPanelDatiBand.SuspendLayout();
            this.tableLayoutPanelButtons.SuspendLayout();
            this.groupBoxIndirizzo.SuspendLayout();
            this.tableLayoutPanelIndirizzo.SuspendLayout();
            this.groupBoxDatiReferente.SuspendLayout();
            this.tableLayoutPanelReferente.SuspendLayout();
            this.groupBoxReferenteInterno.SuspendLayout();
            this.tableLayoutPanelReferenteInterno.SuspendLayout();
            this.groupBoxReferenteEsterno.SuspendLayout();
            this.tableLayoutPanelReferenteEsterno.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelForm
            // 
            this.tableLayoutPanelForm.ColumnCount = 3;
            this.tableLayoutPanelForm.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tableLayoutPanelForm.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelForm.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanelForm.Controls.Add(this.groupBox1, 0, 1);
            this.tableLayoutPanelForm.Controls.Add(this.tableLayoutPanelDatiBand, 2, 1);
            this.tableLayoutPanelForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelForm.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelForm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanelForm.Name = "tableLayoutPanelForm";
            this.tableLayoutPanelForm.RowCount = 2;
            this.tableLayoutPanelForm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanelForm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelForm.Size = new System.Drawing.Size(1249, 577);
            this.tableLayoutPanelForm.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanelComponentiBand);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(4, 29);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(667, 544);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Componenti della band";
            // 
            // tableLayoutPanelComponentiBand
            // 
            this.tableLayoutPanelComponentiBand.ColumnCount = 3;
            this.tableLayoutPanelComponentiBand.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelComponentiBand.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tableLayoutPanelComponentiBand.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelComponentiBand.Controls.Add(this.tableLayoutPanelComponentiBandDisponibili, 0, 0);
            this.tableLayoutPanelComponentiBand.Controls.Add(this.tableLayoutPanelComponentiBandBottoni, 1, 0);
            this.tableLayoutPanelComponentiBand.Controls.Add(this.tableLayoutPanelComponentiBandUtilizzati, 2, 0);
            this.tableLayoutPanelComponentiBand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelComponentiBand.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanelComponentiBand.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanelComponentiBand.Name = "tableLayoutPanelComponentiBand";
            this.tableLayoutPanelComponentiBand.RowCount = 1;
            this.tableLayoutPanelComponentiBand.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelComponentiBand.Size = new System.Drawing.Size(659, 521);
            this.tableLayoutPanelComponentiBand.TabIndex = 0;
            // 
            // tableLayoutPanelComponentiBandDisponibili
            // 
            this.tableLayoutPanelComponentiBandDisponibili.ColumnCount = 1;
            this.tableLayoutPanelComponentiBandDisponibili.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelComponentiBandDisponibili.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanelComponentiBandDisponibili.Controls.Add(this.textBoxFiltroMusicistiDisponibili, 0, 1);
            this.tableLayoutPanelComponentiBandDisponibili.Controls.Add(this.checkedListBoxMusicistiDisponibili, 0, 2);
            this.tableLayoutPanelComponentiBandDisponibili.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelComponentiBandDisponibili.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanelComponentiBandDisponibili.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanelComponentiBandDisponibili.Name = "tableLayoutPanelComponentiBandDisponibili";
            this.tableLayoutPanelComponentiBandDisponibili.RowCount = 3;
            this.tableLayoutPanelComponentiBandDisponibili.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelComponentiBandDisponibili.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelComponentiBandDisponibili.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanelComponentiBandDisponibili.Size = new System.Drawing.Size(288, 513);
            this.tableLayoutPanelComponentiBandDisponibili.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Cerca un musicista";
            // 
            // textBoxFiltroMusicistiDisponibili
            // 
            this.textBoxFiltroMusicistiDisponibili.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFiltroMusicistiDisponibili.Location = new System.Drawing.Point(4, 21);
            this.textBoxFiltroMusicistiDisponibili.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxFiltroMusicistiDisponibili.Name = "textBoxFiltroMusicistiDisponibili";
            this.textBoxFiltroMusicistiDisponibili.Size = new System.Drawing.Size(280, 22);
            this.textBoxFiltroMusicistiDisponibili.TabIndex = 1;
            this.textBoxFiltroMusicistiDisponibili.TextChanged += new System.EventHandler(this.textBoxFiltroMusicistiDisponibili_TextChanged);
            // 
            // checkedListBoxMusicistiDisponibili
            // 
            this.checkedListBoxMusicistiDisponibili.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxMusicistiDisponibili.FormattingEnabled = true;
            this.checkedListBoxMusicistiDisponibili.HorizontalScrollbar = true;
            this.checkedListBoxMusicistiDisponibili.Location = new System.Drawing.Point(3, 49);
            this.checkedListBoxMusicistiDisponibili.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkedListBoxMusicistiDisponibili.Name = "checkedListBoxMusicistiDisponibili";
            this.checkedListBoxMusicistiDisponibili.Size = new System.Drawing.Size(282, 462);
            this.checkedListBoxMusicistiDisponibili.TabIndex = 2;
            // 
            // tableLayoutPanelComponentiBandBottoni
            // 
            this.tableLayoutPanelComponentiBandBottoni.ColumnCount = 1;
            this.tableLayoutPanelComponentiBandBottoni.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelComponentiBandBottoni.Controls.Add(this.deleteMusicistaFromBand, 0, 1);
            this.tableLayoutPanelComponentiBandBottoni.Controls.Add(this.addMusicistaToBand, 0, 0);
            this.tableLayoutPanelComponentiBandBottoni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelComponentiBandBottoni.Location = new System.Drawing.Point(300, 4);
            this.tableLayoutPanelComponentiBandBottoni.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanelComponentiBandBottoni.Name = "tableLayoutPanelComponentiBandBottoni";
            this.tableLayoutPanelComponentiBandBottoni.RowCount = 2;
            this.tableLayoutPanelComponentiBandBottoni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelComponentiBandBottoni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelComponentiBandBottoni.Size = new System.Drawing.Size(59, 513);
            this.tableLayoutPanelComponentiBandBottoni.TabIndex = 1;
            // 
            // deleteMusicistaFromBand
            // 
            this.deleteMusicistaFromBand.Location = new System.Drawing.Point(4, 260);
            this.deleteMusicistaFromBand.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.deleteMusicistaFromBand.Name = "deleteMusicistaFromBand";
            this.deleteMusicistaFromBand.Size = new System.Drawing.Size(51, 28);
            this.deleteMusicistaFromBand.TabIndex = 1;
            this.deleteMusicistaFromBand.Text = "<<";
            this.deleteMusicistaFromBand.UseVisualStyleBackColor = true;
            this.deleteMusicistaFromBand.Click += new System.EventHandler(this.deleteMusicistaFromBand_Click);
            // 
            // addMusicistaToBand
            // 
            this.addMusicistaToBand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.addMusicistaToBand.Location = new System.Drawing.Point(4, 224);
            this.addMusicistaToBand.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.addMusicistaToBand.Name = "addMusicistaToBand";
            this.addMusicistaToBand.Size = new System.Drawing.Size(51, 28);
            this.addMusicistaToBand.TabIndex = 0;
            this.addMusicistaToBand.Text = ">>";
            this.addMusicistaToBand.UseVisualStyleBackColor = true;
            this.addMusicistaToBand.Click += new System.EventHandler(this.addMusicistaToBand_Click);
            // 
            // tableLayoutPanelComponentiBandUtilizzati
            // 
            this.tableLayoutPanelComponentiBandUtilizzati.ColumnCount = 1;
            this.tableLayoutPanelComponentiBandUtilizzati.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelComponentiBandUtilizzati.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanelComponentiBandUtilizzati.Controls.Add(this.textBoxFiltroMusicistiUtilizzati, 0, 1);
            this.tableLayoutPanelComponentiBandUtilizzati.Controls.Add(this.elementHost1, 0, 2);
            this.tableLayoutPanelComponentiBandUtilizzati.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelComponentiBandUtilizzati.Location = new System.Drawing.Point(367, 4);
            this.tableLayoutPanelComponentiBandUtilizzati.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanelComponentiBandUtilizzati.Name = "tableLayoutPanelComponentiBandUtilizzati";
            this.tableLayoutPanelComponentiBandUtilizzati.RowCount = 3;
            this.tableLayoutPanelComponentiBandUtilizzati.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelComponentiBandUtilizzati.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelComponentiBandUtilizzati.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanelComponentiBandUtilizzati.Size = new System.Drawing.Size(288, 513);
            this.tableLayoutPanelComponentiBandUtilizzati.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Musicisti della band";
            // 
            // textBoxFiltroMusicistiUtilizzati
            // 
            this.textBoxFiltroMusicistiUtilizzati.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFiltroMusicistiUtilizzati.Location = new System.Drawing.Point(4, 21);
            this.textBoxFiltroMusicistiUtilizzati.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxFiltroMusicistiUtilizzati.Name = "textBoxFiltroMusicistiUtilizzati";
            this.textBoxFiltroMusicistiUtilizzati.Size = new System.Drawing.Size(280, 22);
            this.textBoxFiltroMusicistiUtilizzati.TabIndex = 3;
            this.textBoxFiltroMusicistiUtilizzati.TextChanged += new System.EventHandler(this.textBoxFiltroMusicistiUtilizzati_TextChanged);
            // 
            // elementHost1
            // 
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(4, 51);
            this.elementHost1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(280, 458);
            this.elementHost1.TabIndex = 4;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = this.listBoxStoricoBand1;
            // 
            // tableLayoutPanelDatiBand
            // 
            this.tableLayoutPanelDatiBand.ColumnCount = 1;
            this.tableLayoutPanelDatiBand.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelDatiBand.Controls.Add(this.tableLayoutPanelButtons, 0, 7);
            this.tableLayoutPanelDatiBand.Controls.Add(this.groupBoxIndirizzo, 0, 6);
            this.tableLayoutPanelDatiBand.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanelDatiBand.Controls.Add(this.dataFormazione, 0, 3);
            this.tableLayoutPanelDatiBand.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanelDatiBand.Controls.Add(this.textBoxNomeBand, 0, 1);
            this.tableLayoutPanelDatiBand.Controls.Add(this.groupBoxDatiReferente, 0, 5);
            this.tableLayoutPanelDatiBand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelDatiBand.Location = new System.Drawing.Point(698, 27);
            this.tableLayoutPanelDatiBand.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanelDatiBand.Name = "tableLayoutPanelDatiBand";
            this.tableLayoutPanelDatiBand.RowCount = 8;
            this.tableLayoutPanelDatiBand.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelDatiBand.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelDatiBand.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelDatiBand.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelDatiBand.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanelDatiBand.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelDatiBand.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelDatiBand.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelDatiBand.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanelDatiBand.Size = new System.Drawing.Size(548, 548);
            this.tableLayoutPanelDatiBand.TabIndex = 8;
            // 
            // tableLayoutPanelButtons
            // 
            this.tableLayoutPanelButtons.ColumnCount = 3;
            this.tableLayoutPanelButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelButtons.Controls.Add(this.buttonAction, 1, 0);
            this.tableLayoutPanelButtons.Controls.Add(this.buttonCancel, 2, 0);
            this.tableLayoutPanelButtons.Controls.Add(this.buttonAction_and_new, 0, 0);
            this.tableLayoutPanelButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelButtons.Location = new System.Drawing.Point(4, 475);
            this.tableLayoutPanelButtons.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanelButtons.Name = "tableLayoutPanelButtons";
            this.tableLayoutPanelButtons.RowCount = 1;
            this.tableLayoutPanelButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelButtons.Size = new System.Drawing.Size(540, 69);
            this.tableLayoutPanelButtons.TabIndex = 8;
            // 
            // buttonAction
            // 
            this.buttonAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAction.Location = new System.Drawing.Point(328, 37);
            this.buttonAction.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonAction.Name = "buttonAction";
            this.buttonAction.Size = new System.Drawing.Size(100, 28);
            this.buttonAction.TabIndex = 13;
            this.buttonAction.Text = "Action";
            this.buttonAction.UseVisualStyleBackColor = true;
            this.buttonAction.Click += new System.EventHandler(this.buttonAction_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(436, 37);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(100, 28);
            this.buttonCancel.TabIndex = 14;
            this.buttonCancel.Text = "Cancella";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonAction_and_new
            // 
            this.buttonAction_and_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAction_and_new.AutoSize = true;
            this.buttonAction_and_new.Location = new System.Drawing.Point(152, 32);
            this.buttonAction_and_new.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonAction_and_new.Name = "buttonAction_and_new";
            this.buttonAction_and_new.Size = new System.Drawing.Size(168, 33);
            this.buttonAction_and_new.TabIndex = 15;
            this.buttonAction_and_new.Text = "Action and new...";
            this.buttonAction_and_new.UseVisualStyleBackColor = true;
            this.buttonAction_and_new.Click += new System.EventHandler(this.buttonAction_and_new_Click);
            // 
            // groupBoxIndirizzo
            // 
            this.groupBoxIndirizzo.Controls.Add(this.tableLayoutPanelIndirizzo);
            this.groupBoxIndirizzo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxIndirizzo.Location = new System.Drawing.Point(3, 371);
            this.groupBoxIndirizzo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxIndirizzo.Name = "groupBoxIndirizzo";
            this.groupBoxIndirizzo.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxIndirizzo.Size = new System.Drawing.Size(542, 98);
            this.groupBoxIndirizzo.TabIndex = 1;
            this.groupBoxIndirizzo.TabStop = false;
            this.groupBoxIndirizzo.Text = "Indirizzo";
            // 
            // tableLayoutPanelIndirizzo
            // 
            this.tableLayoutPanelIndirizzo.ColumnCount = 2;
            this.tableLayoutPanelIndirizzo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanelIndirizzo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanelIndirizzo.Controls.Add(this.comboBoxComune, 1, 1);
            this.tableLayoutPanelIndirizzo.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanelIndirizzo.Controls.Add(this.label9, 0, 1);
            this.tableLayoutPanelIndirizzo.Controls.Add(this.comboBoxProvincia, 1, 0);
            this.tableLayoutPanelIndirizzo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelIndirizzo.Location = new System.Drawing.Point(3, 17);
            this.tableLayoutPanelIndirizzo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanelIndirizzo.Name = "tableLayoutPanelIndirizzo";
            this.tableLayoutPanelIndirizzo.RowCount = 2;
            this.tableLayoutPanelIndirizzo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelIndirizzo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelIndirizzo.Size = new System.Drawing.Size(536, 79);
            this.tableLayoutPanelIndirizzo.TabIndex = 0;
            // 
            // comboBoxComune
            // 
            this.comboBoxComune.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxComune.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxComune.FormattingEnabled = true;
            this.comboBoxComune.Location = new System.Drawing.Point(163, 47);
            this.comboBoxComune.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxComune.Name = "comboBoxComune";
            this.comboBoxComune.Size = new System.Drawing.Size(370, 24);
            this.comboBoxComune.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(82, 11);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "Provincia *";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(88, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "Comune *";
            // 
            // comboBoxProvincia
            // 
            this.comboBoxProvincia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxProvincia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxProvincia.FormattingEnabled = true;
            this.comboBoxProvincia.Location = new System.Drawing.Point(163, 7);
            this.comboBoxProvincia.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxProvincia.Name = "comboBoxProvincia";
            this.comboBoxProvincia.Size = new System.Drawing.Size(370, 24);
            this.comboBoxProvincia.TabIndex = 11;
            this.comboBoxProvincia.SelectedIndexChanged += new System.EventHandler(this.comboBoxProvincia_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 47);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Data formazione";
            // 
            // dataFormazione
            // 
            this.dataFormazione.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataFormazione.Location = new System.Drawing.Point(4, 68);
            this.dataFormazione.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataFormazione.Name = "dataFormazione";
            this.dataFormazione.Size = new System.Drawing.Size(540, 22);
            this.dataFormazione.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome band*";
            // 
            // textBoxNomeBand
            // 
            this.textBoxNomeBand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxNomeBand.Location = new System.Drawing.Point(4, 21);
            this.textBoxNomeBand.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxNomeBand.Name = "textBoxNomeBand";
            this.textBoxNomeBand.Size = new System.Drawing.Size(540, 22);
            this.textBoxNomeBand.TabIndex = 5;
            // 
            // groupBoxDatiReferente
            // 
            this.groupBoxDatiReferente.Controls.Add(this.tableLayoutPanelReferente);
            this.groupBoxDatiReferente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxDatiReferente.Location = new System.Drawing.Point(3, 121);
            this.groupBoxDatiReferente.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxDatiReferente.Name = "groupBoxDatiReferente";
            this.groupBoxDatiReferente.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxDatiReferente.Size = new System.Drawing.Size(542, 246);
            this.groupBoxDatiReferente.TabIndex = 0;
            this.groupBoxDatiReferente.TabStop = false;
            this.groupBoxDatiReferente.Text = "Dati Referente";
            // 
            // tableLayoutPanelReferente
            // 
            this.tableLayoutPanelReferente.ColumnCount = 2;
            this.tableLayoutPanelReferente.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelReferente.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelReferente.Controls.Add(this.groupBoxReferenteInterno, 1, 1);
            this.tableLayoutPanelReferente.Controls.Add(this.groupBoxReferenteEsterno, 1, 0);
            this.tableLayoutPanelReferente.Controls.Add(this.radioButtonReferenteEsterno, 0, 0);
            this.tableLayoutPanelReferente.Controls.Add(this.radioButtonReferenteInterno, 0, 1);
            this.tableLayoutPanelReferente.Controls.Add(this.radioButtonNoReferente, 0, 2);
            this.tableLayoutPanelReferente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelReferente.Location = new System.Drawing.Point(3, 17);
            this.tableLayoutPanelReferente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanelReferente.Name = "tableLayoutPanelReferente";
            this.tableLayoutPanelReferente.RowCount = 3;
            this.tableLayoutPanelReferente.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelReferente.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelReferente.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanelReferente.Size = new System.Drawing.Size(536, 227);
            this.tableLayoutPanelReferente.TabIndex = 0;
            // 
            // groupBoxReferenteInterno
            // 
            this.groupBoxReferenteInterno.Controls.Add(this.tableLayoutPanelReferenteInterno);
            this.groupBoxReferenteInterno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxReferenteInterno.Location = new System.Drawing.Point(29, 105);
            this.groupBoxReferenteInterno.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxReferenteInterno.Name = "groupBoxReferenteInterno";
            this.groupBoxReferenteInterno.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxReferenteInterno.Size = new System.Drawing.Size(503, 93);
            this.groupBoxReferenteInterno.TabIndex = 3;
            this.groupBoxReferenteInterno.TabStop = false;
            this.groupBoxReferenteInterno.Text = "Referente interno (musicista)";
            // 
            // tableLayoutPanelReferenteInterno
            // 
            this.tableLayoutPanelReferenteInterno.ColumnCount = 2;
            this.tableLayoutPanelReferenteInterno.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanelReferenteInterno.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanelReferenteInterno.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanelReferenteInterno.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanelReferenteInterno.Controls.Add(this.comboBoxTelefonoReferenteInterno, 1, 1);
            this.tableLayoutPanelReferenteInterno.Controls.Add(this.comboBoxCognomeReferenteInterno, 1, 0);
            this.tableLayoutPanelReferenteInterno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelReferenteInterno.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanelReferenteInterno.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanelReferenteInterno.Name = "tableLayoutPanelReferenteInterno";
            this.tableLayoutPanelReferenteInterno.RowCount = 2;
            this.tableLayoutPanelReferenteInterno.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelReferenteInterno.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelReferenteInterno.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanelReferenteInterno.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanelReferenteInterno.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanelReferenteInterno.Size = new System.Drawing.Size(495, 70);
            this.tableLayoutPanelReferenteInterno.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Cognome Referente*";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(14, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 17);
            this.label6.TabIndex = 1;
            this.label6.Text = "Telefono Referente";
            // 
            // comboBoxTelefonoReferenteInterno
            // 
            this.comboBoxTelefonoReferenteInterno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxTelefonoReferenteInterno.FormattingEnabled = true;
            this.comboBoxTelefonoReferenteInterno.Location = new System.Drawing.Point(152, 40);
            this.comboBoxTelefonoReferenteInterno.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxTelefonoReferenteInterno.Name = "comboBoxTelefonoReferenteInterno";
            this.comboBoxTelefonoReferenteInterno.Size = new System.Drawing.Size(339, 24);
            this.comboBoxTelefonoReferenteInterno.TabIndex = 10;
            this.comboBoxTelefonoReferenteInterno.SelectedIndexChanged += new System.EventHandler(this.comboBoxTelefonoReferenteInterno_SelectedIndexChanged);
            this.comboBoxTelefonoReferenteInterno.TextChanged += new System.EventHandler(this.comboBoxTelefonoReferenteInterno_TextChanged);
            // 
            // comboBoxCognomeReferenteInterno
            // 
            this.comboBoxCognomeReferenteInterno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxCognomeReferenteInterno.FormattingEnabled = true;
            this.comboBoxCognomeReferenteInterno.Location = new System.Drawing.Point(152, 4);
            this.comboBoxCognomeReferenteInterno.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxCognomeReferenteInterno.Name = "comboBoxCognomeReferenteInterno";
            this.comboBoxCognomeReferenteInterno.Size = new System.Drawing.Size(339, 24);
            this.comboBoxCognomeReferenteInterno.TabIndex = 9;
            this.comboBoxCognomeReferenteInterno.SelectedIndexChanged += new System.EventHandler(this.comboBoxCognomeReferenteInterno_SelectedIndexChanged);
            // 
            // groupBoxReferenteEsterno
            // 
            this.groupBoxReferenteEsterno.Controls.Add(this.tableLayoutPanelReferenteEsterno);
            this.groupBoxReferenteEsterno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxReferenteEsterno.Enabled = false;
            this.groupBoxReferenteEsterno.Location = new System.Drawing.Point(29, 4);
            this.groupBoxReferenteEsterno.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxReferenteEsterno.Name = "groupBoxReferenteEsterno";
            this.groupBoxReferenteEsterno.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxReferenteEsterno.Size = new System.Drawing.Size(503, 93);
            this.groupBoxReferenteEsterno.TabIndex = 0;
            this.groupBoxReferenteEsterno.TabStop = false;
            this.groupBoxReferenteEsterno.Text = "Referente esterno";
            // 
            // tableLayoutPanelReferenteEsterno
            // 
            this.tableLayoutPanelReferenteEsterno.ColumnCount = 2;
            this.tableLayoutPanelReferenteEsterno.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanelReferenteEsterno.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanelReferenteEsterno.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanelReferenteEsterno.Controls.Add(this.textBoxCognomeReferenteEsterno, 1, 0);
            this.tableLayoutPanelReferenteEsterno.Controls.Add(this.textBoxTelefonoReferenteEsterno, 1, 1);
            this.tableLayoutPanelReferenteEsterno.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanelReferenteEsterno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelReferenteEsterno.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanelReferenteEsterno.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanelReferenteEsterno.Name = "tableLayoutPanelReferenteEsterno";
            this.tableLayoutPanelReferenteEsterno.RowCount = 2;
            this.tableLayoutPanelReferenteEsterno.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelReferenteEsterno.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelReferenteEsterno.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanelReferenteEsterno.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanelReferenteEsterno.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanelReferenteEsterno.Size = new System.Drawing.Size(495, 70);
            this.tableLayoutPanelReferenteEsterno.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 17);
            this.label8.TabIndex = 1;
            this.label8.Text = "Telefono Referente*";
            // 
            // textBoxCognomeReferenteEsterno
            // 
            this.textBoxCognomeReferenteEsterno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxCognomeReferenteEsterno.Location = new System.Drawing.Point(152, 6);
            this.textBoxCognomeReferenteEsterno.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxCognomeReferenteEsterno.Name = "textBoxCognomeReferenteEsterno";
            this.textBoxCognomeReferenteEsterno.Size = new System.Drawing.Size(339, 22);
            this.textBoxCognomeReferenteEsterno.TabIndex = 7;
            // 
            // textBoxTelefonoReferenteEsterno
            // 
            this.textBoxTelefonoReferenteEsterno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxTelefonoReferenteEsterno.Location = new System.Drawing.Point(152, 39);
            this.textBoxTelefonoReferenteEsterno.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxTelefonoReferenteEsterno.Name = "textBoxTelefonoReferenteEsterno";
            this.textBoxTelefonoReferenteEsterno.Size = new System.Drawing.Size(339, 22);
            this.textBoxTelefonoReferenteEsterno.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Cognome Referente*";
            // 
            // radioButtonReferenteEsterno
            // 
            this.radioButtonReferenteEsterno.AutoSize = true;
            this.radioButtonReferenteEsterno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radioButtonReferenteEsterno.Location = new System.Drawing.Point(4, 4);
            this.radioButtonReferenteEsterno.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButtonReferenteEsterno.Name = "radioButtonReferenteEsterno";
            this.radioButtonReferenteEsterno.Size = new System.Drawing.Size(17, 93);
            this.radioButtonReferenteEsterno.TabIndex = 1;
            this.radioButtonReferenteEsterno.UseVisualStyleBackColor = true;
            this.radioButtonReferenteEsterno.CheckedChanged += new System.EventHandler(this.radioButtonReferenteEsterno_CheckedChanged);
            // 
            // radioButtonReferenteInterno
            // 
            this.radioButtonReferenteInterno.AutoSize = true;
            this.radioButtonReferenteInterno.Checked = true;
            this.radioButtonReferenteInterno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radioButtonReferenteInterno.Location = new System.Drawing.Point(4, 105);
            this.radioButtonReferenteInterno.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButtonReferenteInterno.Name = "radioButtonReferenteInterno";
            this.radioButtonReferenteInterno.Size = new System.Drawing.Size(17, 93);
            this.radioButtonReferenteInterno.TabIndex = 2;
            this.radioButtonReferenteInterno.TabStop = true;
            this.radioButtonReferenteInterno.UseVisualStyleBackColor = true;
            this.radioButtonReferenteInterno.CheckedChanged += new System.EventHandler(this.radioButtonReferenteInterno_CheckedChanged);
            // 
            // radioButtonNoReferente
            // 
            this.radioButtonNoReferente.AutoSize = true;
            this.tableLayoutPanelReferente.SetColumnSpan(this.radioButtonNoReferente, 2);
            this.radioButtonNoReferente.Location = new System.Drawing.Point(4, 206);
            this.radioButtonNoReferente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButtonNoReferente.Name = "radioButtonNoReferente";
            this.radioButtonNoReferente.Size = new System.Drawing.Size(139, 17);
            this.radioButtonNoReferente.TabIndex = 4;
            this.radioButtonNoReferente.TabStop = true;
            this.radioButtonNoReferente.Text = "Nessun referente";
            this.radioButtonNoReferente.UseVisualStyleBackColor = true;
            this.radioButtonNoReferente.CheckedChanged += new System.EventHandler(this.radioButtonNoReferente_CheckedChanged);
            // 
            // BandFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1249, 577);
            this.Controls.Add(this.tableLayoutPanelForm);
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.MinimumSize = new System.Drawing.Size(1242, 539);
            this.Name = "BandFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nuova band...";
            this.Load += new System.EventHandler(this.BandFrm_Load);
            this.tableLayoutPanelForm.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanelComponentiBand.ResumeLayout(false);
            this.tableLayoutPanelComponentiBandDisponibili.ResumeLayout(false);
            this.tableLayoutPanelComponentiBandDisponibili.PerformLayout();
            this.tableLayoutPanelComponentiBandBottoni.ResumeLayout(false);
            this.tableLayoutPanelComponentiBandUtilizzati.ResumeLayout(false);
            this.tableLayoutPanelComponentiBandUtilizzati.PerformLayout();
            this.tableLayoutPanelDatiBand.ResumeLayout(false);
            this.tableLayoutPanelDatiBand.PerformLayout();
            this.tableLayoutPanelButtons.ResumeLayout(false);
            this.tableLayoutPanelButtons.PerformLayout();
            this.groupBoxIndirizzo.ResumeLayout(false);
            this.tableLayoutPanelIndirizzo.ResumeLayout(false);
            this.tableLayoutPanelIndirizzo.PerformLayout();
            this.groupBoxDatiReferente.ResumeLayout(false);
            this.tableLayoutPanelReferente.ResumeLayout(false);
            this.tableLayoutPanelReferente.PerformLayout();
            this.groupBoxReferenteInterno.ResumeLayout(false);
            this.tableLayoutPanelReferenteInterno.ResumeLayout(false);
            this.tableLayoutPanelReferenteInterno.PerformLayout();
            this.groupBoxReferenteEsterno.ResumeLayout(false);
            this.tableLayoutPanelReferenteEsterno.ResumeLayout(false);
            this.tableLayoutPanelReferenteEsterno.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelForm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNomeBand;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dataFormazione;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelComponentiBand;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelComponentiBandDisponibili;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelComponentiBandBottoni;
        private System.Windows.Forms.Button addMusicistaToBand;
        private System.Windows.Forms.Button deleteMusicistaFromBand;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelComponentiBandUtilizzati;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxFiltroMusicistiUtilizzati;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelDatiBand;
        private System.Windows.Forms.GroupBox groupBoxIndirizzo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelIndirizzo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBoxDatiReferente;
        private ComboBox comboBoxComune;
        private ComboBox comboBoxProvincia;
        private TextBox textBoxFiltroMusicistiDisponibili;
        private CheckedListBox checkedListBoxMusicistiDisponibili;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private ListBoxStoricoBand listBoxStoricoBand1;
        private TableLayoutPanel tableLayoutPanelReferente;
        private GroupBox groupBoxReferenteEsterno;
        private RadioButton radioButtonReferenteEsterno;
        private RadioButton radioButtonReferenteInterno;
        private GroupBox groupBoxReferenteInterno;
        private TableLayoutPanel tableLayoutPanelReferenteInterno;
        private Label label5;
        private Label label6;
        private ComboBox comboBoxTelefonoReferenteInterno;
        private ComboBox comboBoxCognomeReferenteInterno;
        private TableLayoutPanel tableLayoutPanelReferenteEsterno;
        private Label label8;
        private TextBox textBoxCognomeReferenteEsterno;
        private TextBox textBoxTelefonoReferenteEsterno;
        private Label label7;
        private TableLayoutPanel tableLayoutPanelButtons;
        private Button buttonCancel;
        private Button buttonAction;
        private Button buttonAction_and_new;
        private RadioButton radioButtonNoReferente;
    }
}