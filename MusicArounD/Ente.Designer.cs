﻿namespace MusicArounD
{
    partial class Ente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxCategoriaEnte = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxTipologiaEnte = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelDatiEnte = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.comboBoxCatena = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxNomeEnte = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelIndirizzo = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxCivico = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxVia = new System.Windows.Forms.TextBox();
            this.comboBoxComune = new System.Windows.Forms.ComboBox();
            this.textBoxCAP = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBoxSedePrincipale = new System.Windows.Forms.CheckBox();
            this.checkBoxAdmin = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxTel1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxTel2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxTel3 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxSitoWeb = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.comboBoxPaternitaEnte = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBoxFormazione = new System.Windows.Forms.CheckBox();
            this.checkBoxStabile = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanelDatiEnte.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanelIndirizzo.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxCategoriaEnte, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxTipologiaEnte, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 0, 6);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(879, 489);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Scegli la categoria dell\'ente:";
            // 
            // comboBoxCategoriaEnte
            // 
            this.comboBoxCategoriaEnte.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxCategoriaEnte.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCategoriaEnte.FormattingEnabled = true;
            this.comboBoxCategoriaEnte.Location = new System.Drawing.Point(4, 29);
            this.comboBoxCategoriaEnte.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxCategoriaEnte.Name = "comboBoxCategoriaEnte";
            this.comboBoxCategoriaEnte.Size = new System.Drawing.Size(871, 24);
            this.comboBoxCategoriaEnte.TabIndex = 1;
            this.comboBoxCategoriaEnte.SelectedIndexChanged += new System.EventHandler(this.comboBoxCategoriaEnte_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 57);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Scegli la tipologia di ente:";
            // 
            // comboBoxTipologiaEnte
            // 
            this.comboBoxTipologiaEnte.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxTipologiaEnte.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTipologiaEnte.FormattingEnabled = true;
            this.comboBoxTipologiaEnte.Location = new System.Drawing.Point(4, 78);
            this.comboBoxTipologiaEnte.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxTipologiaEnte.Name = "comboBoxTipologiaEnte";
            this.comboBoxTipologiaEnte.Size = new System.Drawing.Size(871, 24);
            this.comboBoxTipologiaEnte.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanelDatiEnte);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(4, 122);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(871, 275);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dati generali del nuovo ente";
            // 
            // tableLayoutPanelDatiEnte
            // 
            this.tableLayoutPanelDatiEnte.ColumnCount = 5;
            this.tableLayoutPanelDatiEnte.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelDatiEnte.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelDatiEnte.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanelDatiEnte.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelDatiEnte.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelDatiEnte.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.textBoxEmail, 1, 0);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.comboBoxCatena, 4, 1);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.label8, 3, 0);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.textBoxNomeEnte, 4, 0);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.groupBox2, 3, 2);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.textBoxTel1, 1, 2);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.textBoxTel2, 1, 3);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.textBoxTel3, 1, 4);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.label7, 0, 5);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.textBoxSitoWeb, 1, 5);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.label13, 0, 6);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.comboBoxPaternitaEnte, 1, 6);
            this.tableLayoutPanelDatiEnte.Controls.Add(this.label14, 3, 1);
            this.tableLayoutPanelDatiEnte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelDatiEnte.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanelDatiEnte.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanelDatiEnte.Name = "tableLayoutPanelDatiEnte";
            this.tableLayoutPanelDatiEnte.RowCount = 7;
            this.tableLayoutPanelDatiEnte.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanelDatiEnte.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanelDatiEnte.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanelDatiEnte.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanelDatiEnte.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanelDatiEnte.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanelDatiEnte.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanelDatiEnte.Size = new System.Drawing.Size(863, 252);
            this.tableLayoutPanelDatiEnte.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Indirizzo email: *";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxEmail.Location = new System.Drawing.Point(122, 4);
            this.textBoxEmail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxEmail.MaxLength = 50;
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(292, 22);
            this.textBoxEmail.TabIndex = 1;
            // 
            // comboBoxCatena
            // 
            this.comboBoxCatena.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxCatena.FormattingEnabled = true;
            this.comboBoxCatena.Location = new System.Drawing.Point(567, 40);
            this.comboBoxCatena.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxCatena.Name = "comboBoxCatena";
            this.comboBoxCatena.Size = new System.Drawing.Size(292, 24);
            this.comboBoxCatena.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(449, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 36);
            this.label8.TabIndex = 12;
            this.label8.Text = "Nome ente: *";
            // 
            // textBoxNomeEnte
            // 
            this.textBoxNomeEnte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxNomeEnte.Location = new System.Drawing.Point(567, 4);
            this.textBoxNomeEnte.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxNomeEnte.MaxLength = 50;
            this.textBoxNomeEnte.Name = "textBoxNomeEnte";
            this.textBoxNomeEnte.Size = new System.Drawing.Size(292, 22);
            this.textBoxNomeEnte.TabIndex = 13;
            // 
            // groupBox2
            // 
            this.tableLayoutPanelDatiEnte.SetColumnSpan(this.groupBox2, 2);
            this.groupBox2.Controls.Add(this.tableLayoutPanelIndirizzo);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(449, 76);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanelDatiEnte.SetRowSpan(this.groupBox2, 5);
            this.groupBox2.Size = new System.Drawing.Size(410, 172);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Indirizzo";
            // 
            // tableLayoutPanelIndirizzo
            // 
            this.tableLayoutPanelIndirizzo.ColumnCount = 2;
            this.tableLayoutPanelIndirizzo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelIndirizzo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelIndirizzo.Controls.Add(this.textBoxCivico, 1, 1);
            this.tableLayoutPanelIndirizzo.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanelIndirizzo.Controls.Add(this.label10, 0, 1);
            this.tableLayoutPanelIndirizzo.Controls.Add(this.label11, 0, 2);
            this.tableLayoutPanelIndirizzo.Controls.Add(this.label12, 0, 3);
            this.tableLayoutPanelIndirizzo.Controls.Add(this.textBoxVia, 1, 0);
            this.tableLayoutPanelIndirizzo.Controls.Add(this.comboBoxComune, 1, 2);
            this.tableLayoutPanelIndirizzo.Controls.Add(this.textBoxCAP, 1, 3);
            this.tableLayoutPanelIndirizzo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelIndirizzo.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanelIndirizzo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanelIndirizzo.Name = "tableLayoutPanelIndirizzo";
            this.tableLayoutPanelIndirizzo.RowCount = 4;
            this.tableLayoutPanelIndirizzo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelIndirizzo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelIndirizzo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelIndirizzo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelIndirizzo.Size = new System.Drawing.Size(402, 149);
            this.tableLayoutPanelIndirizzo.TabIndex = 0;
            // 
            // textBoxCivico
            // 
            this.textBoxCivico.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxCivico.Location = new System.Drawing.Point(81, 41);
            this.textBoxCivico.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxCivico.MaxLength = 8;
            this.textBoxCivico.Name = "textBoxCivico";
            this.textBoxCivico.Size = new System.Drawing.Size(317, 22);
            this.textBoxCivico.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 0);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 17);
            this.label9.TabIndex = 0;
            this.label9.Text = "Via";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 37);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 17);
            this.label10.TabIndex = 1;
            this.label10.Text = "Civico";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 74);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 17);
            this.label11.TabIndex = 2;
            this.label11.Text = "Comune *";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(4, 111);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 17);
            this.label12.TabIndex = 3;
            this.label12.Text = "Cap *";
            // 
            // textBoxVia
            // 
            this.textBoxVia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVia.Location = new System.Drawing.Point(81, 4);
            this.textBoxVia.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxVia.MaxLength = 50;
            this.textBoxVia.Name = "textBoxVia";
            this.textBoxVia.Size = new System.Drawing.Size(317, 22);
            this.textBoxVia.TabIndex = 4;
            // 
            // comboBoxComune
            // 
            this.comboBoxComune.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxComune.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxComune.FormattingEnabled = true;
            this.comboBoxComune.Location = new System.Drawing.Point(81, 78);
            this.comboBoxComune.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxComune.MaxLength = 60;
            this.comboBoxComune.Name = "comboBoxComune";
            this.comboBoxComune.Size = new System.Drawing.Size(317, 24);
            this.comboBoxComune.TabIndex = 6;
            // 
            // textBoxCAP
            // 
            this.textBoxCAP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxCAP.Location = new System.Drawing.Point(81, 115);
            this.textBoxCAP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxCAP.MaxLength = 10;
            this.textBoxCAP.Name = "textBoxCAP";
            this.textBoxCAP.Size = new System.Drawing.Size(317, 22);
            this.textBoxCAP.TabIndex = 7;
            this.textBoxCAP.TextChanged += new System.EventHandler(this.textBoxCAP_TextChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanelDatiEnte.SetColumnSpan(this.tableLayoutPanel3, 2);
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.checkBoxSedePrincipale, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.checkBoxAdmin, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(4, 40);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(410, 28);
            this.tableLayoutPanel3.TabIndex = 19;
            // 
            // checkBoxSedePrincipale
            // 
            this.checkBoxSedePrincipale.AutoSize = true;
            this.checkBoxSedePrincipale.Location = new System.Drawing.Point(4, 4);
            this.checkBoxSedePrincipale.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxSedePrincipale.Name = "checkBoxSedePrincipale";
            this.checkBoxSedePrincipale.Size = new System.Drawing.Size(128, 20);
            this.checkBoxSedePrincipale.TabIndex = 3;
            this.checkBoxSedePrincipale.Text = "Sede principale";
            this.checkBoxSedePrincipale.UseVisualStyleBackColor = true;
            // 
            // checkBoxAdmin
            // 
            this.checkBoxAdmin.AutoSize = true;
            this.checkBoxAdmin.Location = new System.Drawing.Point(209, 4);
            this.checkBoxAdmin.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxAdmin.Name = "checkBoxAdmin";
            this.checkBoxAdmin.Size = new System.Drawing.Size(149, 20);
            this.checkBoxAdmin.TabIndex = 2;
            this.checkBoxAdmin.Text = "Sei admin dell\'ente";
            this.checkBoxAdmin.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 72);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Telefono 1 *";
            // 
            // textBoxTel1
            // 
            this.textBoxTel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTel1.Location = new System.Drawing.Point(122, 76);
            this.textBoxTel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxTel1.MaxLength = 20;
            this.textBoxTel1.Name = "textBoxTel1";
            this.textBoxTel1.Size = new System.Drawing.Size(292, 22);
            this.textBoxTel1.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 108);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Telefono 2";
            // 
            // textBoxTel2
            // 
            this.textBoxTel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTel2.Location = new System.Drawing.Point(122, 112);
            this.textBoxTel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxTel2.MaxLength = 20;
            this.textBoxTel2.Name = "textBoxTel2";
            this.textBoxTel2.Size = new System.Drawing.Size(292, 22);
            this.textBoxTel2.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 144);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Telefono 3";
            // 
            // textBoxTel3
            // 
            this.textBoxTel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTel3.Location = new System.Drawing.Point(122, 148);
            this.textBoxTel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxTel3.MaxLength = 20;
            this.textBoxTel3.Name = "textBoxTel3";
            this.textBoxTel3.Size = new System.Drawing.Size(292, 22);
            this.textBoxTel3.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 180);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 17);
            this.label7.TabIndex = 10;
            this.label7.Text = "Sito web:";
            // 
            // textBoxSitoWeb
            // 
            this.textBoxSitoWeb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSitoWeb.Location = new System.Drawing.Point(122, 184);
            this.textBoxSitoWeb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxSitoWeb.MaxLength = 50;
            this.textBoxSitoWeb.Name = "textBoxSitoWeb";
            this.textBoxSitoWeb.Size = new System.Drawing.Size(292, 22);
            this.textBoxSitoWeb.TabIndex = 11;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(4, 216);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(103, 17);
            this.label13.TabIndex = 15;
            this.label13.Text = "Padre dell\'ente";
            // 
            // comboBoxPaternitaEnte
            // 
            this.comboBoxPaternitaEnte.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxPaternitaEnte.FormattingEnabled = true;
            this.comboBoxPaternitaEnte.Location = new System.Drawing.Point(122, 220);
            this.comboBoxPaternitaEnte.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxPaternitaEnte.Name = "comboBoxPaternitaEnte";
            this.comboBoxPaternitaEnte.Size = new System.Drawing.Size(292, 24);
            this.comboBoxPaternitaEnte.TabIndex = 16;
            this.comboBoxPaternitaEnte.SelectedIndexChanged += new System.EventHandler(this.comboBoxPaternitaEnte_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(449, 36);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(110, 17);
            this.label14.TabIndex = 17;
            this.label14.Text = "Catena dell\'ente";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.Controls.Add(this.button1, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.buttonNext, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(4, 444);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(871, 41);
            this.tableLayoutPanel4.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(767, 9);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 0;
            this.button1.Text = "Cancella";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNext.Location = new System.Drawing.Point(659, 9);
            this.buttonNext.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(100, 28);
            this.buttonNext.TabIndex = 1;
            this.buttonNext.Text = "Next...";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.checkBoxFormazione, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.checkBoxStabile, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(4, 405);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.Size = new System.Drawing.Size(871, 31);
            this.tableLayoutPanel5.TabIndex = 7;
            // 
            // checkBoxFormazione
            // 
            this.checkBoxFormazione.AutoSize = true;
            this.checkBoxFormazione.Location = new System.Drawing.Point(4, 4);
            this.checkBoxFormazione.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxFormazione.Name = "checkBoxFormazione";
            this.checkBoxFormazione.Size = new System.Drawing.Size(268, 21);
            this.checkBoxFormazione.TabIndex = 0;
            this.checkBoxFormazione.Text = "Ente formativo abilitato a certificazioni";
            this.checkBoxFormazione.UseVisualStyleBackColor = true;
            this.checkBoxFormazione.Visible = false;
            // 
            // checkBoxStabile
            // 
            this.checkBoxStabile.AutoSize = true;
            this.checkBoxStabile.Location = new System.Drawing.Point(280, 4);
            this.checkBoxStabile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxStabile.Name = "checkBoxStabile";
            this.checkBoxStabile.Size = new System.Drawing.Size(310, 21);
            this.checkBoxStabile.TabIndex = 1;
            this.checkBoxStabile.Text = "Abilitare gestione delle stanze dello stabile...";
            this.checkBoxStabile.UseVisualStyleBackColor = true;
            this.checkBoxStabile.Visible = false;
            // 
            // Ente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 489);
            this.Controls.Add(this.tableLayoutPanel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.MinimumSize = new System.Drawing.Size(893, 520);
            this.Name = "Ente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nuovo Ente...";
            this.Load += new System.EventHandler(this.NuovoEnte_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanelDatiEnte.ResumeLayout(false);
            this.tableLayoutPanelDatiEnte.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanelIndirizzo.ResumeLayout(false);
            this.tableLayoutPanelIndirizzo.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxCategoriaEnte;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxTipologiaEnte;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelDatiEnte;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.ComboBox comboBoxCatena;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxNomeEnte;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelIndirizzo;
        private System.Windows.Forms.TextBox textBoxCivico;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxVia;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.CheckBox checkBoxSedePrincipale;
        private System.Windows.Forms.CheckBox checkBoxAdmin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxTel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxTel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxSitoWeb;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox comboBoxPaternitaEnte;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.CheckBox checkBoxFormazione;
        private System.Windows.Forms.CheckBox checkBoxStabile;
        private System.Windows.Forms.ComboBox comboBoxComune;
        private System.Windows.Forms.TextBox textBoxCAP;
        private System.Windows.Forms.TextBox textBoxTel3;



    }
}