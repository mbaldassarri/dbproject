﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace MusicArounD
{
    public partial class CalendarioEnte : DBEngineForm
    {
        private int weekCount = 0;
        private DOMINIO_ENTI enteObj;
        private bool newEnte;

        private List<CALENDARIO> resultCALENDARIO = new List<CALENDARIO>();
        private Dictionary<PERIODO, Dictionary<String, ORARIO_GIORNALIERO>> orariPeriodo = new Dictionary<PERIODO, Dictionary<string, ORARIO_GIORNALIERO>>();

        public CalendarioEnte() : this(null, true) { }
      
        public CalendarioEnte(DOMINIO_ENTI enteObj, bool newEnte)
        {
            InitializeComponent();
            this.enteObj = enteObj;
            this.newEnte = newEnte;
            //if (!newEnte)
            //{
                loadDataEnte();
                fillUI();
            //}
        }
        private void loadDataEnte()
        {
            Dictionary<String, ORARIO_GIORNALIERO> orarioGiornaliero;
            foreach (CALENDARIO calendario in enteObj.CALENDARIO)
            {
                orarioGiornaliero = new Dictionary<string, ORARIO_GIORNALIERO>();
                foreach (PERIODO periodo in calendario.PERIODO)
                {

                    foreach (ORARIO_GIORNALIERO orario in calendario.ORARIO_GIORNALIERO)
                    {
                        orarioGiornaliero.Add(orario.giornoSettimana, orario);
                    }
                    orariPeriodo[periodo] = orarioGiornaliero;
                }
            }
        }
        private void fillUI()
        {
            comboBoxPeriodoValidita.Items.AddRange(orariPeriodo.Keys.ToArray());
        }
        private void weekCounter(bool activated)
        {
            if (activated) weekCount++;
            if (!activated) weekCount--;
            if (weekCount == 0)
            {
                buttonAggiungiOrario.Enabled = false;
            }
            else
            {
                buttonAggiungiOrario.Enabled = true;
            }

        }
        private void CalendarioEnte_Load(object sender, EventArgs e)
        {
            dateTimePickerValiditaFine.Value = DateTime.Now.AddDays(1);
            if (!newEnte) comboBoxPeriodoValidita.Items.AddRange( madEngine.getPERIODOs_byDomainEntityID(enteObj).ToArray());
        }

        private void checkBoxLUN_CheckedChanged(object sender, EventArgs e)
        {
            weekCounter(checkBoxLUN.Checked);
        }

        private void checkBoxMAR_CheckedChanged(object sender, EventArgs e)
        {
            weekCounter(checkBoxMAR.Checked);
        }

        private void checkBoxMER_CheckedChanged(object sender, EventArgs e)
        {
            weekCounter(checkBoxMER.Checked);
        }

        private void checkBoxGIO_CheckedChanged(object sender, EventArgs e)
        {
            weekCounter(checkBoxGIO.Checked);
        }

        private void checkBoxVEN_CheckedChanged(object sender, EventArgs e)
        {
            weekCounter(checkBoxVEN.Checked);
        }

        private void checkBoxSAB_CheckedChanged(object sender, EventArgs e)
        {
            weekCounter(checkBoxSAB.Checked);
        }

        private void checkBoxDOM_CheckedChanged(object sender, EventArgs e)
        {
            weekCounter(checkBoxDOM.Checked);
        }
        private void buttonCreaPeriodo_Click(object sender, EventArgs e)
        {
            PERIODO newPeriodo = new PERIODO { dataInizio = dateTimePickerValiditaInizio.Value.Date, dataFine = dateTimePickerValiditaFine.Value.Date };
            List<PERIODO> searchList = madEngine.searchPERIODOs(newPeriodo.dataInizio, newPeriodo.dataFine, enteObj.email);
 
            bool isOverlapped = isNewDateRangeOverlapped(comboBoxPeriodoValidita, newPeriodo);
            if (!isOverlapped && searchList.Count() == 0)
            {
                comboBoxPeriodoValidita.Items.Add(newPeriodo);
                comboBoxPeriodoValidita.SelectedItem = newPeriodo;
            }
        }

        private void comboBoxPeriodoValidita_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxPeriodoValidita.SelectedIndex > -1)
            {
                cleanAllLBs();
                tableLayoutPanelCalendarioSettimanale.Enabled = true;
                groupBoxCreazioneOrario.Enabled = true;
                try
                {
                    setOrari(orariPeriodo[(PERIODO)comboBoxPeriodoValidita.SelectedItem]);
                }
                catch { }
            }
        }

        private void dateTimePickerOraInizio_ValueChanged(object sender, EventArgs e)
        {
            //dateTimePickerOraFine.Value = dateTimePickerOraInizio.Value.AddHours(1);
        }
        private bool isNewTimeRangeOverlapped(ListBox lb, MDE.TimeRange t)
        {
            bool matched = false;
            foreach (MDE.TimeRange tr in lb.Items)
            {
                if (tr.Time1 <= t.Time1 && t.Time1 <= tr.Time2) matched = true;
                if (tr.Time1 <= t.Time2 && t.Time2 <= tr.Time2) matched = true;
            }
            return matched;
        }
        private bool isNewDateRangeOverlapped(ComboBox cb, PERIODO checkedPeriod)
        {
            bool matched = false;
            foreach (PERIODO againstPeriod in cb.Items)
            {
                if (againstPeriod.dataInizio <= checkedPeriod.dataInizio && checkedPeriod.dataInizio <= againstPeriod.dataFine) matched = true;
                if (againstPeriod.dataInizio <= checkedPeriod.dataFine && checkedPeriod.dataFine <= againstPeriod.dataFine) matched = true;
            }
            return matched;
        }
        private bool isDate1LTDate2(MDE.TimeRange tr)
        {
            bool result = true;
            
            return result;
        }
        private void buttonAggiungiOrario_Click(object sender, EventArgs e)
        {
            bool matched;
            Dictionary<String, ORARIO_GIORNALIERO> resultGetOrari;
            MDE.TimeRange nt;
            if (dateTimePickerOraInizio.Value <= dateTimePickerOraFine.Value)
                nt = new MDE.TimeRange(MDE.getSQLTime (dateTimePickerOraInizio.Value), MDE.getSQLTime(dateTimePickerOraFine.Value));
            else
                nt = new MDE.TimeRange(MDE.getSQLTime(dateTimePickerOraFine.Value), MDE.getSQLTime(dateTimePickerOraInizio.Value));
            if (dateTimePickerOraInizio.Value.ToShortTimeString().Equals(dateTimePickerOraFine.Value.ToShortTimeString()))
                return;

            if (checkBoxLUN.Checked)
            {
                matched = isNewTimeRangeOverlapped(listBoxLUN, nt);
                if (!matched && listBoxLUN.Items.Count < 3) listBoxLUN.Items.Add(nt);
            }
            if (checkBoxMAR.Checked)
            {
                matched = isNewTimeRangeOverlapped(listBoxMAR, nt);
                if (!matched && listBoxMAR.Items.Count < 3) listBoxMAR.Items.Add(nt);
            }
            if (checkBoxMER.Checked)
            {
                matched = isNewTimeRangeOverlapped(listBoxMER, nt);
                if (!matched && listBoxMER.Items.Count < 3) listBoxMER.Items.Add(nt);
            }
            if (checkBoxGIO.Checked)
            {
                matched = isNewTimeRangeOverlapped(listBoxGIO, nt);
                if (!matched && listBoxGIO.Items.Count < 3) listBoxGIO.Items.Add(nt);
            }
            if (checkBoxVEN.Checked)
            {
                matched = isNewTimeRangeOverlapped(listBoxVEN, nt);
                if (!matched && listBoxVEN.Items.Count < 3) listBoxVEN.Items.Add(nt);
            }
            if (checkBoxSAB.Checked)
            {
                matched = isNewTimeRangeOverlapped(listBoxSAB, nt);
                if (!matched && listBoxSAB.Items.Count < 3) listBoxSAB.Items.Add(nt);
            }
            if (checkBoxDOM.Checked)
            {
                matched = isNewTimeRangeOverlapped(listBoxDOM, nt);
                if (!matched && listBoxDOM.Items.Count < 3) listBoxDOM.Items.Add(nt);
            }
            resultGetOrari = getOrari();
            if (resultGetOrari != null && resultGetOrari.Count > 0)
            {
                orariPeriodo[(PERIODO)comboBoxPeriodoValidita.SelectedItem] = resultGetOrari;
            }
        }

        private void buttonRimuoviOrarioLUN_Click(object sender, EventArgs e)
        {
            listBoxLUN.Items.Remove(listBoxLUN.SelectedItem);
        }

        private void buttonRimuoviOrarioMAR_Click(object sender, EventArgs e)
        {
            listBoxMAR.Items.Remove(listBoxMAR.SelectedItem);
        }

        private void buttonRimuoviOrarioMER_Click(object sender, EventArgs e)
        {
            listBoxMER.Items.Remove(listBoxMER.SelectedItem);
        }

        private void buttonRimuoviOrarioGIO_Click(object sender, EventArgs e)
        {
            listBoxGIO.Items.Remove(listBoxGIO.SelectedItem);
        }

        private void buttonRimuoviOrarioVEN_Click(object sender, EventArgs e)
        {
            listBoxVEN.Items.Remove(listBoxVEN.SelectedItem);
        }

        private void buttonRimuoviOrarioSAB_Click(object sender, EventArgs e)
        {
            listBoxSAB.Items.Remove(listBoxSAB.SelectedItem);
        }

        private void buttonRimuoviOrarioDOM_Click(object sender, EventArgs e)
        {
            listBoxDOM.Items.Remove(listBoxDOM.SelectedItem);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {

        }
        private ORARIO_GIORNALIERO getOrarioGiornaliero(ListBox lb, String giorno)
        {
            ORARIO_GIORNALIERO result = null;
            int x=0;
            if (lb.Items.Count > 0)
            {
                result = new ORARIO_GIORNALIERO();
                //result.ente = idEnte;

                foreach (MDE.TimeRange tr in lb.Items)
                {
                    switch (x++)
                    {
                        case 0:
                            result.ora1_inizio = tr.Time1;
                            result.ora1_fine = tr.Time2;
                            break;
                        case 1:
                            result.ora2_inizio = tr.Time1;
                            result.ora2_fine = tr.Time2;
                            break;
                        case 2:
                            result.ora3_inizio = tr.Time1;
                            result.ora3_fine = tr.Time2;
                            break;
                    }
                }
                result.giornoSettimana = giorno;
            }
            
            return result;
        }
        private Dictionary<String, ORARIO_GIORNALIERO> getOrari ()
        {
            Dictionary<String, ORARIO_GIORNALIERO> result = new Dictionary<string, ORARIO_GIORNALIERO>();

            ORARIO_GIORNALIERO oLUN = getOrarioGiornaliero(listBoxLUN, MDE.Days.LUN);
            if (oLUN != null)
            {
                result.Add(oLUN.giornoSettimana, oLUN);
            }
            
            ORARIO_GIORNALIERO oMAR = getOrarioGiornaliero(listBoxMAR, MDE.Days.MAR);
            if (oMAR != null)
            {
                result.Add(oMAR.giornoSettimana, oMAR);
            }
            
            ORARIO_GIORNALIERO oMER = getOrarioGiornaliero(listBoxMER, MDE.Days.MER);
            if (oMER != null)
            {
                result.Add(oMER.giornoSettimana, oMER);
            }

            ORARIO_GIORNALIERO oGIO = getOrarioGiornaliero(listBoxGIO, MDE.Days.GIO);
            if (oGIO != null)
            {
                result.Add(oGIO.giornoSettimana, oGIO);
            }

            ORARIO_GIORNALIERO oVEN = getOrarioGiornaliero(listBoxVEN, MDE.Days.VEN);
            if (oVEN != null)
            {
                result.Add(oVEN.giornoSettimana, oVEN);
            }

            ORARIO_GIORNALIERO oSAB = getOrarioGiornaliero(listBoxSAB, MDE.Days.SAB);
            if (oSAB != null)
            {
                result.Add(oSAB.giornoSettimana, oSAB);
            }

            ORARIO_GIORNALIERO oDOM = getOrarioGiornaliero(listBoxDOM, MDE.Days.DOM);
            if (oDOM != null)
            {
                result.Add(oDOM.giornoSettimana, oDOM);
            }

            return result;
        }
        private void setOrarioGiornaliero(ListBox lb, ORARIO_GIORNALIERO o)
        {
            lb.Items.Clear();
            lb.Items.Add(new MDE.TimeRange (o.ora1_inizio, o.ora1_fine));
            if (o.ora2_inizio != null)
            {
                lb.Items.Add(new MDE.TimeRange((TimeSpan)o.ora2_inizio, (TimeSpan)o.ora2_fine));
            }
            if (o.ora3_inizio != null)
            {
                lb.Items.Add(new MDE.TimeRange((TimeSpan)o.ora3_inizio, (TimeSpan)o.ora3_fine));
            }
        }
        private void cleanAllLBs()
        {
            listBoxLUN.Items.Clear();
            checkBoxLUN.Checked = false;
            listBoxMAR.Items.Clear();
            checkBoxMAR.Checked = false;
            listBoxMER.Items.Clear();
            checkBoxMER.Checked = false;
            listBoxGIO.Items.Clear();
            checkBoxGIO.Checked = false;
            listBoxVEN.Items.Clear();
            checkBoxVEN.Checked = false;
            listBoxSAB.Items.Clear();
            checkBoxSAB.Checked = false;
            listBoxDOM.Items.Clear();
            checkBoxDOM.Checked = false;
        }
        private void setOrari(Dictionary<String, ORARIO_GIORNALIERO> og)
        {
            cleanAllLBs();
            foreach (KeyValuePair<String, ORARIO_GIORNALIERO> entry in og)
            {
                switch (entry.Key)
                {
                    case MDE.Days.LUN:
                        setOrarioGiornaliero(listBoxLUN, entry.Value);
                        break;
                    case MDE.Days.MAR:
                        setOrarioGiornaliero(listBoxMAR, entry.Value);
                        break;
                    case MDE.Days.MER:
                        setOrarioGiornaliero(listBoxMER, entry.Value);
                        break;
                    case MDE.Days.GIO:
                        setOrarioGiornaliero(listBoxGIO, entry.Value);
                        break;
                    case MDE.Days.VEN:
                        setOrarioGiornaliero(listBoxVEN, entry.Value);
                        break;
                    case MDE.Days.SAB:
                        setOrarioGiornaliero(listBoxSAB, entry.Value);
                        break;
                    case MDE.Days.DOM:
                        setOrarioGiornaliero(listBoxDOM, entry.Value);
                        break;
                }
            }
            
        }
 
        private void buttonSave_Click(object sender, EventArgs e)
        {
            bool matched = false;
            foreach(PERIODO periodo in comboBoxPeriodoValidita.Items)
            {
                CALENDARIO dbCAL = madEngine.getSameCalendar(orariPeriodo[periodo], enteObj.email);
                if (dbCAL == null)
                {
                    foreach (CALENDARIO intCAL in resultCALENDARIO)
                    {
                        matched = madEngine.isSameWeeklyHours(orariPeriodo[periodo].Values.ToList(), intCAL.ORARIO_GIORNALIERO.ToList());
                        if (matched)
                        {
                            dbCAL = intCAL;
                            break;
                        }
                    }
                    if (!matched)
                    {
                        dbCAL = new CALENDARIO();
                    }
                }
                dbCAL.PERIODO.Add(periodo);
                if (!matched)
                {
                    dbCAL.ORARIO_GIORNALIERO.AddRange(orariPeriodo[periodo].Values.ToList());
                    resultCALENDARIO.Add(dbCAL);
                }
            }
            enteObj.CALENDARIO.AddRange(resultCALENDARIO);
        }
 
    }
}
