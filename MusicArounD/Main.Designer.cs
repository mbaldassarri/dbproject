﻿namespace MusicArounD
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.inserimentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuovoEnteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.musicistiBandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuovoMusicistaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuovaBandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.storicoBandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enteFormazioneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuovoCorsoToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.nuovaEdizioneCorsoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enteServiziToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.liutatioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.strumentiLavoratiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.localeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.negozioToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fornituraNegozioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.nuovoServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enteStabiliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuovaSalaProveToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.eventiToolStripMenuItem = new System.Windows.Forms.ToolStripSeparator();
            this.nuovoEventoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.partecipazioneEventoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.nuovoConcertoStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultazioneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.entiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.scuoleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studioDiRegistrazioneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.localeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salaProveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.musicistiBandToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cercaMusicistaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cercaBandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inserimentoToolStripMenuItem,
            this.consultazioneToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(804, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // inserimentoToolStripMenuItem
            // 
            this.inserimentoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuovoEnteToolStripMenuItem,
            this.musicistiBandToolStripMenuItem,
            this.toolStripMenuItem3,
            this.storicoBandToolStripMenuItem,
            this.enteFormazioneToolStripMenuItem,
            this.enteServiziToolStripMenuItem,
            this.enteStabiliToolStripMenuItem,
            this.eventiToolStripMenuItem,
            this.nuovoEventoToolStripMenuItem,
            this.partecipazioneEventoToolStripMenuItem1,
            this.toolStripMenuItem4,
            this.nuovoConcertoStripMenuItem});
            this.inserimentoToolStripMenuItem.Name = "inserimentoToolStripMenuItem";
            this.inserimentoToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.inserimentoToolStripMenuItem.Text = "Menu";
            // 
            // nuovoEnteToolStripMenuItem
            // 
            this.nuovoEnteToolStripMenuItem.Name = "nuovoEnteToolStripMenuItem";
            this.nuovoEnteToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.nuovoEnteToolStripMenuItem.Text = "Nuovo Ente...";
            this.nuovoEnteToolStripMenuItem.Click += new System.EventHandler(this.nuovoEnteToolStripMenuItem_Click);
            // 
            // musicistiBandToolStripMenuItem
            // 
            this.musicistiBandToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuovoMusicistaToolStripMenuItem,
            this.nuovaBandToolStripMenuItem});
            this.musicistiBandToolStripMenuItem.Name = "musicistiBandToolStripMenuItem";
            this.musicistiBandToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.musicistiBandToolStripMenuItem.Text = "Musicisti/Band";
            // 
            // nuovoMusicistaToolStripMenuItem
            // 
            this.nuovoMusicistaToolStripMenuItem.Name = "nuovoMusicistaToolStripMenuItem";
            this.nuovoMusicistaToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.nuovoMusicistaToolStripMenuItem.Text = "Nuovo Musicista...";
            this.nuovoMusicistaToolStripMenuItem.Click += new System.EventHandler(this.nuovoMusicistaToolStripMenuItem_Click);
            // 
            // nuovaBandToolStripMenuItem
            // 
            this.nuovaBandToolStripMenuItem.Name = "nuovaBandToolStripMenuItem";
            this.nuovaBandToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.nuovaBandToolStripMenuItem.Text = "Nuova Band...";
            this.nuovaBandToolStripMenuItem.Click += new System.EventHandler(this.nuovaBandToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(196, 6);
            // 
            // storicoBandToolStripMenuItem
            // 
            this.storicoBandToolStripMenuItem.Enabled = false;
            this.storicoBandToolStripMenuItem.Name = "storicoBandToolStripMenuItem";
            this.storicoBandToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.storicoBandToolStripMenuItem.Text = "Storico Band";
            this.storicoBandToolStripMenuItem.Click += new System.EventHandler(this.storicoBandToolStripMenuItem_Click);
            // 
            // enteFormazioneToolStripMenuItem
            // 
            this.enteFormazioneToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuovoCorsoToolStripMenuItem2,
            this.nuovaEdizioneCorsoToolStripMenuItem});
            this.enteFormazioneToolStripMenuItem.Enabled = false;
            this.enteFormazioneToolStripMenuItem.Name = "enteFormazioneToolStripMenuItem";
            this.enteFormazioneToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.enteFormazioneToolStripMenuItem.Text = "Ente di formazione";
            // 
            // nuovoCorsoToolStripMenuItem2
            // 
            this.nuovoCorsoToolStripMenuItem2.Name = "nuovoCorsoToolStripMenuItem2";
            this.nuovoCorsoToolStripMenuItem2.Size = new System.Drawing.Size(199, 22);
            this.nuovoCorsoToolStripMenuItem2.Text = "Nuovo Corso...";
            // 
            // nuovaEdizioneCorsoToolStripMenuItem
            // 
            this.nuovaEdizioneCorsoToolStripMenuItem.Name = "nuovaEdizioneCorsoToolStripMenuItem";
            this.nuovaEdizioneCorsoToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.nuovaEdizioneCorsoToolStripMenuItem.Text = "Nuova Edizione Corso...";
            // 
            // enteServiziToolStripMenuItem
            // 
            this.enteServiziToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.liutatioToolStripMenuItem,
            this.localeToolStripMenuItem1,
            this.negozioToolStripMenuItem1,
            this.serviceToolStripMenuItem1});
            this.enteServiziToolStripMenuItem.Enabled = false;
            this.enteServiziToolStripMenuItem.Name = "enteServiziToolStripMenuItem";
            this.enteServiziToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.enteServiziToolStripMenuItem.Text = "Ente di servizi";
            // 
            // liutatioToolStripMenuItem
            // 
            this.liutatioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.strumentiLavoratiToolStripMenuItem});
            this.liutatioToolStripMenuItem.Name = "liutatioToolStripMenuItem";
            this.liutatioToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.liutatioToolStripMenuItem.Text = "Liutatio";
            // 
            // strumentiLavoratiToolStripMenuItem
            // 
            this.strumentiLavoratiToolStripMenuItem.Name = "strumentiLavoratiToolStripMenuItem";
            this.strumentiLavoratiToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.strumentiLavoratiToolStripMenuItem.Text = "Strumenti Lavorati...";
            // 
            // localeToolStripMenuItem1
            // 
            this.localeToolStripMenuItem1.Name = "localeToolStripMenuItem1";
            this.localeToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.localeToolStripMenuItem1.Text = "Locale";
            // 
            // negozioToolStripMenuItem1
            // 
            this.negozioToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fornituraNegozioToolStripMenuItem});
            this.negozioToolStripMenuItem1.Name = "negozioToolStripMenuItem1";
            this.negozioToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.negozioToolStripMenuItem1.Text = "Negozio";
            // 
            // fornituraNegozioToolStripMenuItem
            // 
            this.fornituraNegozioToolStripMenuItem.Name = "fornituraNegozioToolStripMenuItem";
            this.fornituraNegozioToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.fornituraNegozioToolStripMenuItem.Text = "Fornitura negozio...";
            // 
            // serviceToolStripMenuItem1
            // 
            this.serviceToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuovoServiceToolStripMenuItem});
            this.serviceToolStripMenuItem1.Name = "serviceToolStripMenuItem1";
            this.serviceToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.serviceToolStripMenuItem1.Text = "Service";
            // 
            // nuovoServiceToolStripMenuItem
            // 
            this.nuovoServiceToolStripMenuItem.Name = "nuovoServiceToolStripMenuItem";
            this.nuovoServiceToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.nuovoServiceToolStripMenuItem.Text = "Nuovo service...";
            // 
            // enteStabiliToolStripMenuItem
            // 
            this.enteStabiliToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuovaSalaProveToolStripMenuItem1});
            this.enteStabiliToolStripMenuItem.Enabled = false;
            this.enteStabiliToolStripMenuItem.Name = "enteStabiliToolStripMenuItem";
            this.enteStabiliToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.enteStabiliToolStripMenuItem.Text = "Ente di stabili";
            // 
            // nuovaSalaProveToolStripMenuItem1
            // 
            this.nuovaSalaProveToolStripMenuItem1.Name = "nuovaSalaProveToolStripMenuItem1";
            this.nuovaSalaProveToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.nuovaSalaProveToolStripMenuItem1.Text = "Sala prove";
            // 
            // eventiToolStripMenuItem
            // 
            this.eventiToolStripMenuItem.Name = "eventiToolStripMenuItem";
            this.eventiToolStripMenuItem.Size = new System.Drawing.Size(196, 6);
            // 
            // nuovoEventoToolStripMenuItem
            // 
            this.nuovoEventoToolStripMenuItem.Enabled = false;
            this.nuovoEventoToolStripMenuItem.Name = "nuovoEventoToolStripMenuItem";
            this.nuovoEventoToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.nuovoEventoToolStripMenuItem.Text = "Nuovo evento..";
            // 
            // partecipazioneEventoToolStripMenuItem1
            // 
            this.partecipazioneEventoToolStripMenuItem1.Enabled = false;
            this.partecipazioneEventoToolStripMenuItem1.Name = "partecipazioneEventoToolStripMenuItem1";
            this.partecipazioneEventoToolStripMenuItem1.Size = new System.Drawing.Size(199, 22);
            this.partecipazioneEventoToolStripMenuItem1.Text = "Partecipazione Evento...";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(196, 6);
            // 
            // nuovoConcertoStripMenuItem
            // 
            this.nuovoConcertoStripMenuItem.Enabled = false;
            this.nuovoConcertoStripMenuItem.Name = "nuovoConcertoStripMenuItem";
            this.nuovoConcertoStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.nuovoConcertoStripMenuItem.Text = "Nuovo Concerto...";
            // 
            // consultazioneToolStripMenuItem
            // 
            this.consultazioneToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.entiToolStripMenuItem1,
            this.musicistiBandToolStripMenuItem1});
            this.consultazioneToolStripMenuItem.Name = "consultazioneToolStripMenuItem";
            this.consultazioneToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.consultazioneToolStripMenuItem.Text = "Consultazione";
            // 
            // entiToolStripMenuItem1
            // 
            this.entiToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.scuoleToolStripMenuItem,
            this.serviceToolStripMenuItem,
            this.studioDiRegistrazioneToolStripMenuItem,
            this.localeToolStripMenuItem,
            this.salaProveToolStripMenuItem});
            this.entiToolStripMenuItem1.Enabled = false;
            this.entiToolStripMenuItem1.Name = "entiToolStripMenuItem1";
            this.entiToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.entiToolStripMenuItem1.Text = "Enti";
            // 
            // scuoleToolStripMenuItem
            // 
            this.scuoleToolStripMenuItem.Name = "scuoleToolStripMenuItem";
            this.scuoleToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.scuoleToolStripMenuItem.Text = "Scuole";
            // 
            // serviceToolStripMenuItem
            // 
            this.serviceToolStripMenuItem.Name = "serviceToolStripMenuItem";
            this.serviceToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.serviceToolStripMenuItem.Text = "Service";
            // 
            // studioDiRegistrazioneToolStripMenuItem
            // 
            this.studioDiRegistrazioneToolStripMenuItem.Name = "studioDiRegistrazioneToolStripMenuItem";
            this.studioDiRegistrazioneToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.studioDiRegistrazioneToolStripMenuItem.Text = "Studio di registrazione";
            // 
            // localeToolStripMenuItem
            // 
            this.localeToolStripMenuItem.Name = "localeToolStripMenuItem";
            this.localeToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.localeToolStripMenuItem.Text = "Locale";
            // 
            // salaProveToolStripMenuItem
            // 
            this.salaProveToolStripMenuItem.Name = "salaProveToolStripMenuItem";
            this.salaProveToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.salaProveToolStripMenuItem.Text = "Sala prove";
            // 
            // musicistiBandToolStripMenuItem1
            // 
            this.musicistiBandToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cercaMusicistaToolStripMenuItem,
            this.cercaBandToolStripMenuItem});
            this.musicistiBandToolStripMenuItem1.Name = "musicistiBandToolStripMenuItem1";
            this.musicistiBandToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.musicistiBandToolStripMenuItem1.Text = "Musicisti/Band";
            this.musicistiBandToolStripMenuItem1.Click += new System.EventHandler(this.musicistiBandToolStripMenuItem1_Click);
            // 
            // cercaMusicistaToolStripMenuItem
            // 
            this.cercaMusicistaToolStripMenuItem.Name = "cercaMusicistaToolStripMenuItem";
            this.cercaMusicistaToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.cercaMusicistaToolStripMenuItem.Text = "Modifica Musicista";
            this.cercaMusicistaToolStripMenuItem.Click += new System.EventHandler(this.cercaMusicistaToolStripMenuItem_Click);
            // 
            // cercaBandToolStripMenuItem
            // 
            this.cercaBandToolStripMenuItem.Name = "cercaBandToolStripMenuItem";
            this.cercaBandToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.cercaBandToolStripMenuItem.Text = "Modifica Band";
            this.cercaBandToolStripMenuItem.Click += new System.EventHandler(this.cercaBandToolStripMenuItem_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 314);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Main";
            this.Text = "Main";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem consultazioneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inserimentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem musicistiBandToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuovoMusicistaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuovaBandToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator eventiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuovoEventoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem entiToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem scuoleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serviceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studioDiRegistrazioneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem localeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salaProveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem musicistiBandToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem enteServiziToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enteStabiliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enteFormazioneToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem nuovoCorsoToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem nuovaEdizioneCorsoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem negozioToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fornituraNegozioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem localeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem serviceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem nuovaSalaProveToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem partecipazioneEventoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem nuovoConcertoStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuovoServiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem liutatioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem strumentiLavoratiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuovoEnteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem storicoBandToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cercaMusicistaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cercaBandToolStripMenuItem;
    }
}