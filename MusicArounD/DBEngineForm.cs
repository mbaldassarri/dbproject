﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Transactions;

namespace MusicArounD
{
    public partial class DBEngineForm : Form, IEnlistmentNotification
    {
        protected MDE madEngine;
        public DBEngineForm()
        {
            InitializeComponent();
            madEngine = MDE.Instance;
            madEngine.renewDataContext();
        }

        private void DBEngineForm_Load(object sender, EventArgs e)
        {

        }

        public virtual void Commit(Enlistment enlistment)
        {
            throw new NotImplementedException();
        }

        public virtual void InDoubt(Enlistment enlistment)
        {
            throw new NotImplementedException();
        }

        public virtual void Prepare(PreparingEnlistment preparingEnlistment)
        {
            throw new NotImplementedException();
        }

        public virtual void Rollback(Enlistment enlistment)
        {
            throw new NotImplementedException();
        }
    }
}
