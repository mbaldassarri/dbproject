-- *********************************************
-- * Standard SQL generation                   
-- *--------------------------------------------
-- * DB-MAIN version: 9.1.6              
-- * Generator date: Feb 25 2013              
-- * Generation date: Mon Jul 14 01:17:52 2014 
-- * LUN file: C:\Users\anthartic\Documents\Visual Studio 2013\Projects\MusicArounD\MusicArounD\db-main-luns\DPROJECT.lun 
-- * Schema: madDB/Logical-1 
-- ********************************************* 


-- Database Section
-- ________________ 

--create database madDB;


-- DBSpace Section
-- _______________


-- Tables Section
-- _____________ 

create table ALBUM (
     idAlbum INT not null IDENTITY,
     annoPubblicazione date not null,
     nome varchar(50) not null,
     incisore varchar(50),
     constraint IDALBUM_ID primary key (idAlbum));

create table APPARTENENZA_CAP (
     cap varchar(10) not null,
     idProvincia char(2) not null,
     nomeComune varchar(60) not null,
     constraint IDAPPARTENENZA_CAP primary key (nomeComune, idProvincia, cap));

create table BAND (
     idBand INT not null IDENTITY,
     solista bit not null,
     nome varchar(60),
     dataFormazione date not null,
     nomeReferente varchar(30),
     telefonoReferente varchar(15),
     musicistaReferente varchar(60),
     provinciaBand char(2) not null,
     comuneBand varchar(60) not null,
     constraint IDBAND_ID primary key (idBand));

create table BRANO (
     idBrano INT not null IDENTITY,
     titolo varchar(70) not null,
     durata INT not null,
     incisore varchar(50),
     constraint IDBRANO_ID primary key (idBrano));

create table CALENDARIO (
     ente varchar(50) not null,
     idCal INT not null IDENTITY,
     constraint IDCALENDARIO_ID primary key (ente, idCal));

create table CAP (
     cap varchar(10) not null,
     constraint IDCAP_ID primary key (cap));

create table CATEGORIA (
     idCat INT not null IDENTITY,
     nome varchar(40) not null,
     strumentoMusicale bit not null,
     paternitaCategoria INT not null,
     constraint IDCATEGORIA_ID primary key (idCat));

create table CATENA (
     idCatena INT not null IDENTITY,
     nome varchar(60) not null,
     constraint IDCATENA_ID primary key (idCatena));

create table COMUNE (
     nome varchar(60) not null,
     idProvincia char(2) not null,
     constraint IDCOMUNE_ID primary key (idProvincia, nome));

create table CONCERTO (
     entePartecipante varchar(50) not null,
     idEvento INT not null,
     inizioEvento date not null,
     fineEvento date not null,
     idBand INT not null,
     data date not null,
     ora char(1),
     annullato bit not null,
     idService varchar(50),
     constraint IDCONCERTO primary key (entePartecipante, idEvento, inizioEvento, fineEvento, idBand, data));

create table CORSO (
     idEnteFormativo varchar(50) not null,
     nome varchar(60) not null,
     descrizione varchar(1000) not null,
     constraint IDCORSO primary key (idEnteFormativo, nome));

create table DOMINIO_ENTI (
     email varchar(50) not null,
     amministratore bit not null,
     sedePrincipale bit not null,
     telefono1 varchar(20) not null,
     telefono2 varchar(20),
     telefono3 varchar(20),
     sitoWeb varchar(50),
     nome varchar(50) not null,
     via varchar(50),
     civico varchar(8),
     idCatena INT,
     padreFiglio varchar(50) not null,
     provinciaResidenza char(2) not null,
     comuneResidenza varchar(60) not null,
     constraint IDDOMINIO_ENTI primary key (email));

create table EDIZIONE_CORSO (
     idEnteFormativo varchar(50) not null,
     nomeCorso varchar(60) not null,
     dataInizio date not null,
     dataFine date not null,
     argomentiTrattati varchar(1000) not null,
     musicistaDocente varchar(60) not null,
     constraint IDEDIZIONE_CORSO primary key (idEnteFormativo, nomeCorso, dataInizio, dataFine));

create table EDIZIONE_EVENTO (
     idEvento INT not null,
     dataInizio date not null,
     dataFine date not null,
     idService varchar(50),
     enteCreatore varchar(50) not null,
     provinciaEvento char(2) not null,
     comuneEvento varchar(60) not null,
     constraint IDSVOLGIMENTO primary key (idEvento, dataInizio, dataFine));

create table ENTE_FORMATIVO (
     idEnteFormativo varchar(50) not null,
     abilitataCertificazioni bit not null,
     tipo varchar(60) not null,
     constraint FKDominioEnteFormativo_ID primary key (idEnteFormativo));

create table ENTE_SERVIZIO (
     idEnteServizio varchar(50) not null,
     constraint FKDominioEnteServizio_ID primary key (idEnteServizio));

create table ENTE_STABILE  (
     idEnteStabile varchar(50) not null,
     gestisceStanze bit not null,
     idxGranularita INT not null,
     tipo varchar(60) not null,
     constraint FKDominioEnteStabile_ID primary key (idEnteStabile));

create table ENTRATA_USCITA (
     musicista varchar(60) not null,
     idBand INT not null,
     e_u bit not null,
     data date not null,
     constraint IDENTRATA_USCITA primary key (idBand, musicista, data));

create table EVENTO (
     idEvento INT not null IDENTITY,
     nome varchar(70) not null,
     constraint IDEVENTO_ID primary key (idEvento));

create table GENERE (
     nome varchar(20) not null,
     constraint IDGENERE_ID primary key (nome));

create table GENERI_ALBUM (
     idAlbum INT not null,
     nomeGenere varchar(20) not null,
     constraint IDGENERI_ALBUM primary key (nomeGenere, idAlbum));

create table INCLUSIONE (
     idBrano INT not null,
     idAlbum INT not null,
     constraint IDINCLUSIONE primary key (idAlbum, idBrano));

create table LAVORAZIONE (
     dominioEnte varchar(50) not null,
     idCategoria INT not null,
     creazione bit not null,
     constraint IDLAVORAZIONE primary key (idCategoria, dominioEnte));

create table LINEE_PRODOTTO (
     idMarca INT not null,
     idCategoria INT not null,
     constraint IDPROPOSTA primary key (idMarca, idCategoria));

create table LIUTAIO (
     idLiutatio varchar(50) not null,
     constraint FKEnteServizioLiutatio_ID primary key (idLiutatio));

create table LOCALE (
     idLocale varchar(50) not null,
     constraint FKEnteServizioLocale_ID primary key (idLocale));

create table MARCA (
     idMarca INT not null IDENTITY,
     nome varchar(40) not null,
     constraint IDMARCA_ID primary key (idMarca));

create table MUSICISTA (
     email varchar(60) not null,
     sitoWeb varchar(100),
     nomeArte varchar(70),
     nome varchar(50) not null,
     cognome varchar(50) not null,
     dataNascita date,
     telefono1 varchar(15),
     telefono2 varchar(15),
     telefono3 varchar(15),
     constraint IDMUSICISTA_ID primary key (email));

create table NEGOZIO (
     idNegozio varchar(50) not null,
     constraint FKEnteServizioNegozio_ID primary key (idNegozio));

create table ORARIO_GIORNALIERO (
     ente varchar(50) not null,
     idCal INT not null,
     giornoSettimana char(3) not null,
     ora1_inizio date not null,
     ora1_fine date not null,
     ora2_inizio date,
     ora2_fine date,
     ora3_inizio date,
     ora3_fine date,
     constraint IDORARIO_GIORNALIERO primary key (ente, idCal, giornoSettimana));

create table PARTECIPAZIONE (
     entePartecipante varchar(50) not null,
     idEvento INT not null,
     inizioEvento date not null,
     fineEvento date not null,
     amministratore bit not null,
     moderato bit not null,
     constraint IDPRESENZA_ATTIVITA_PER_EVENTO primary key (entePartecipante, idEvento, inizioEvento, fineEvento));

create table PERIODO (
     ente varchar(50) not null,
     idCal INT not null,
     dataInizio date not null,
     dataFine date not null,
     constraint IDPERIODO primary key (ente, idCal, dataInizio, dataFine));

create table PROVINCIA (
     id char(2) not null,
     nome varchar(30) not null,
     idRegione char(3) not null,
     constraint IDPROVINCIA_ID primary key (id));

create table PUBBLICAZIONE_ALBUM (
     idBand INT not null,
     idAlbum INT not null,
     constraint IDPUBBLICAZIONE_ALBUM primary key (idBand, idAlbum));

create table REGIONE (
     idRegione char(3) not null,
     nome varchar(30) not null,
     constraint IDREGIONE_ID primary key (idRegione));

create table SCRITTURA_BRANO (
     musicista varchar(60) not null,
     idBrano INT not null,
     constraint IDSCRITTURA_BRANO primary key (musicista, idBrano));

create table SERVICE (
     idService varchar(50) not null,
     constraint FKEnteServizioService_ID primary key (idService));

create table SLOT_ORARIO (
     ora date not null,
     idxGranularita INT not null,
     constraint IDTABELLA_ORARI primary key (ora));

create table SLOT_PRENOTATI_STANZA (
     idEnteStabile varchar(50) not null,
     idStanza varchar(30) not null,
     slot_orario date not null,
     inizio_fine bit not null,
     dataPrenotazione date not null,
     moderata bit not null,
     idBand INT not null,
     constraint IDPRENOTAZIONE_STANZA primary key (idEnteStabile, idStanza, slot_orario, inizio_fine, dataPrenotazione));

create table STANZA (
     idEnteStabile varchar(50) not null,
     idStanza varchar(30) not null,
     constraint IDSTANZA primary key (idEnteStabile, idStanza));

create table STRUMENTO_SUONATO  (
     musicista varchar(60) not null,
     idCategoria INT not null,
     anniEsperienza INT not null,
     constraint IDSTRUMENTO_SUONATO  primary key (musicista, idCategoria));

create table TAGS (
     nome varchar(20) not null,
     constraint IDTAGS primary key (nome));

create table TAGS_EVENTO (
     idEvento INT not null,
     nomeTag varchar(20) not null,
     constraint IDTAGS_EVENTO primary key (nomeTag, idEvento));

create table TIPOLOGIE_ENTE_FORMATIVO (
     nomeTipologia varchar(60) not null,
     constraint IDTIPOLOGIE_ENTE_FORMATIVO primary key (nomeTipologia));

create table TIPOLOGIE_ENTE_STABILE (
     nomeTipologia varchar(60) not null,
     constraint IDTIPOLOGIE_ENTE_STABILE primary key (nomeTipologia));

create table VENDITA (
     idMarca INT not null,
     idCategoria INT not null,
     idNegozio varchar(50) not null,
     affitto bit not null,
     constraint IDVENDITA primary key (idNegozio, idMarca, idCategoria));


-- Constraints Section
-- ___________________ 

alter table ALBUM add constraint FKIncisoreAlbum
     foreign key (incisore)
     references ENTE_STABILE ;

alter table APPARTENENZA_CAP add constraint FKR_CapAppartenenzaCap
     foreign key (cap)
     references CAP;

alter table APPARTENENZA_CAP add constraint FKComuneAppartenenzaCap
     foreign key (idProvincia, nomeComune)
     references COMUNE;

alter table BAND add constraint FKReferente
     foreign key (musicistaReferente)
     references MUSICISTA;

alter table BAND add constraint COEXBAND
     check((nomeReferente is not null and telefonoReferente is not null)
           or (nomeReferente is null and telefonoReferente is null)); 

alter table BAND add constraint FKResidenzaBand
     foreign key (provinciaBand, comuneBand)
     references COMUNE;


alter table BRANO add constraint FKIncisoreBrano
     foreign key (incisore)
     references ENTE_STABILE ;

alter table CALENDARIO add constraint FKPaternitaDominioEnte
     foreign key (ente)
     references DOMINIO_ENTI;

alter table CATEGORIA add constraint FKPaternitaCategoria
     foreign key (paternitaCategoria)
     references CATEGORIA;

alter table COMUNE add constraint FKAppartenenzaComune
     foreign key (idProvincia)
     references PROVINCIA;

alter table CONCERTO add constraint FKPaternitaConcerto
     foreign key (idBand)
     references BAND;

alter table CONCERTO add constraint FKService
     foreign key (idService)
     references SERVICE;

alter table CONCERTO add constraint FKOspitante
     foreign key (entePartecipante, idEvento, inizioEvento, fineEvento)
     references PARTECIPAZIONE;

alter table CORSO add constraint FKPaternitaCorso
     foreign key (idEnteFormativo)
     references ENTE_FORMATIVO;

alter table DOMINIO_ENTI add constraint FKGestoreDominio
     foreign key (idCatena)
     references CATENA;

alter table DOMINIO_ENTI add constraint FKPaternitaDominio
     foreign key (padreFiglio)
     references DOMINIO_ENTI;

alter table DOMINIO_ENTI add constraint COEXDOMINIO_ENTI
     check((via is not null and civico is not null)
           or (via is null and civico is null)); 

alter table DOMINIO_ENTI add constraint FKComuneResidenza
     foreign key (provinciaResidenza, comuneResidenza)
     references COMUNE;

alter table EDIZIONE_CORSO add constraint FKPaternitaEdizioneCorso
     foreign key (idEnteFormativo, nomeCorso)
     references CORSO;

alter table EDIZIONE_CORSO add constraint FKDocenzaEdizioneCorso
     foreign key (musicistaDocente)
     references MUSICISTA;

alter table EDIZIONE_EVENTO add constraint FKCreatoreEdizioneEvento
     foreign key (enteCreatore)
     references DOMINIO_ENTI;

alter table EDIZIONE_EVENTO add constraint FKEventoEdizioneEvento
     foreign key (idEvento)
     references EVENTO;

alter table EDIZIONE_EVENTO add constraint FKServiceEdizioneEvento
     foreign key (idService)
     references SERVICE;

alter table EDIZIONE_EVENTO add constraint FKResidenzaEvento
     foreign key (provinciaEvento, comuneEvento)
     references COMUNE;

alter table ENTE_FORMATIVO add constraint FKDominioEnteFormativo_FK
     foreign key (idEnteFormativo)
     references DOMINIO_ENTI;

alter table ENTE_FORMATIVO add constraint FKTipologiaEnteFormativo
     foreign key (tipo)
     references TIPOLOGIE_ENTE_FORMATIVO;

alter table ENTE_SERVIZIO add constraint FKDominioEnteServizio_FK
     foreign key (idEnteServizio)
     references DOMINIO_ENTI;

alter table ENTE_STABILE  add constraint FKDominioEnteStabile_FK
     foreign key (idEnteStabile)
     references DOMINIO_ENTI;

alter table ENTE_STABILE  add constraint FKTipologiaEnteStabile
     foreign key (tipo)
     references TIPOLOGIE_ENTE_STABILE;

alter table ENTRATA_USCITA add constraint FKBandEntrataUscita
     foreign key (idBand)
     references BAND;

alter table ENTRATA_USCITA add constraint FKMusicistaEntrataUscita
     foreign key (musicista)
     references MUSICISTA;

alter table GENERI_ALBUM add constraint FKGenereGenereAlbum
     foreign key (nomeGenere)
     references GENERE;

alter table GENERI_ALBUM add constraint FKAlbumGenereAlbum
     foreign key (idAlbum)
     references ALBUM;

alter table INCLUSIONE add constraint FKAlbumInclusione
     foreign key (idAlbum)
     references ALBUM;

alter table INCLUSIONE add constraint FKBranoInclusione
     foreign key (idBrano)
     references BRANO;

alter table LAVORAZIONE add constraint FKCategoriaLavorazione
     foreign key (idCategoria)
     references CATEGORIA;

alter table LAVORAZIONE add constraint FKLiutatioLavorazione
     foreign key (dominioEnte)
     references LIUTAIO;

alter table LINEE_PRODOTTO add constraint FKCategoriaLineaProdotto
     foreign key (idCategoria)
     references CATEGORIA;

alter table LINEE_PRODOTTO add constraint FKMarcaLineaProdotto
     foreign key (idMarca)
     references MARCA;

alter table LIUTAIO add constraint FKEnteServizioLiutatio_FK
     foreign key (idLiutatio)
     references ENTE_SERVIZIO;

alter table LOCALE add constraint FKEnteServizioLocale_FK
     foreign key (idLocale)
     references ENTE_SERVIZIO;

alter table NEGOZIO add constraint FKEnteServizioNegozio_FK
     foreign key (idNegozio)
     references ENTE_SERVIZIO;

alter table ORARIO_GIORNALIERO add constraint COEXORARIO_GIORNALIERO
     check((ora2_inizio is not null and ora2_fine is not null)
           or (ora2_inizio is null and ora2_fine is null)); 

alter table ORARIO_GIORNALIERO add constraint COEXORARIO_GIORNALIERO_1
     check((ora3_inizio is not null and ora3_fine is not null)
           or (ora3_inizio is null and ora3_fine is null)); 

alter table ORARIO_GIORNALIERO add constraint FKCalendarioOrarioGiornaliero
     foreign key (ente, idCal)
     references CALENDARIO;

alter table PARTECIPAZIONE add constraint FKEventoPartecipazione
     foreign key (idEvento, inizioEvento, fineEvento)
     references EDIZIONE_EVENTO;

alter table PARTECIPAZIONE add constraint FKEntePartecipazione
     foreign key (entePartecipante)
     references DOMINIO_ENTI;

alter table PERIODO add constraint FKCalendarioPeriodo
     foreign key (ente, idCal)
     references CALENDARIO;

alter table PROVINCIA add constraint FKAppartenenzaProvincia
     foreign key (idRegione)
     references REGIONE;

alter table PUBBLICAZIONE_ALBUM add constraint FKAlbumPubblicazioneAlbum
     foreign key (idAlbum)
     references ALBUM;

alter table PUBBLICAZIONE_ALBUM add constraint FKBandPubblicazioneAlbum
     foreign key (idBand)
     references BAND;

alter table SCRITTURA_BRANO add constraint FKBranoScritturaBrano
     foreign key (idBrano)
     references BRANO;

alter table SCRITTURA_BRANO add constraint FKMusicistaScritturaBrano
     foreign key (musicista)
     references MUSICISTA;

alter table SERVICE add constraint FKEnteServizioService_FK
     foreign key (idService)
     references ENTE_SERVIZIO;

alter table SLOT_PRENOTATI_STANZA add constraint FKBandSlotPrenotato
     foreign key (idBand)
     references BAND;

alter table SLOT_PRENOTATI_STANZA add constraint FKSlotOrarioSlotPrenotato
     foreign key (slot_orario)
     references SLOT_ORARIO;

alter table SLOT_PRENOTATI_STANZA add constraint FKStanzaSlotPrenotato
     foreign key (idEnteStabile, idStanza)
     references STANZA;

alter table STANZA add constraint FKEnteStabileStanza
     foreign key (idEnteStabile)
     references ENTE_STABILE ;

alter table STRUMENTO_SUONATO  add constraint FKCategoriaStumentoSuonato
     foreign key (idCategoria)
     references CATEGORIA;

alter table STRUMENTO_SUONATO  add constraint FKMusicistaStrumentoSuonato
     foreign key (musicista)
     references MUSICISTA;

alter table TAGS_EVENTO add constraint FKTagsTagsEvento
     foreign key (nomeTag)
     references TAGS;

alter table TAGS_EVENTO add constraint FKEventoTagsEvento
     foreign key (idEvento)
     references EVENTO;

alter table VENDITA add constraint FKNegozioVendita
     foreign key (idNegozio)
     references NEGOZIO;

alter table VENDITA add constraint FKLineaProdottoVendita
     foreign key (idMarca, idCategoria)
     references LINEE_PRODOTTO;


-- Index Section
-- _____________ 

