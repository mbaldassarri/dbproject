﻿namespace MusicArounD
{
    partial class CercaMusicista
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.madDBMapperDataContextBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet11 = new MusicArounD.DataSet1();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxRicercaMusicista = new System.Windows.Forms.TextBox();
            this.btOk = new System.Windows.Forms.Button();
            this.dataGridViewMusicista = new System.Windows.Forms.DataGridView();
            this.check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cognome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomeArte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataNascita = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Telefono1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comune = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Provincia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.madDBMapperDataContextBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet11)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMusicista)).BeginInit();
            this.SuspendLayout();
            // 
            // madDBMapperDataContextBindingSource
            // 
            this.madDBMapperDataContextBindingSource.DataSource = typeof(MusicArounD.MadDBMapperDataContext);
            // 
            // dataSet11
            // 
            this.dataSet11.DataSetName = "DataSet1";
            this.dataSet11.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.textBoxRicercaMusicista, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btOk, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.dataGridViewMusicista, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.044626F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 97.95538F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1016, 514);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // textBoxRicercaMusicista
            // 
            this.textBoxRicercaMusicista.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxRicercaMusicista.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRicercaMusicista.ForeColor = System.Drawing.Color.Black;
            this.textBoxRicercaMusicista.Location = new System.Drawing.Point(2, 30);
            this.textBoxRicercaMusicista.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxRicercaMusicista.Name = "textBoxRicercaMusicista";
            this.textBoxRicercaMusicista.Size = new System.Drawing.Size(1012, 24);
            this.textBoxRicercaMusicista.TabIndex = 1;
            this.textBoxRicercaMusicista.TextChanged += new System.EventHandler(this.searchEvent);
            // 
            // btOk
            // 
            this.btOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btOk.Location = new System.Drawing.Point(932, 489);
            this.btOk.Margin = new System.Windows.Forms.Padding(2);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(82, 23);
            this.btOk.TabIndex = 3;
            this.btOk.Text = "Avanti";
            this.btOk.UseVisualStyleBackColor = true;
            this.btOk.Click += new System.EventHandler(this.btOK_Click);
            // 
            // dataGridViewMusicista
            // 
            this.dataGridViewMusicista.AllowUserToAddRows = false;
            this.dataGridViewMusicista.AllowUserToDeleteRows = false;
            this.dataGridViewMusicista.AllowUserToOrderColumns = true;
            this.dataGridViewMusicista.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewMusicista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMusicista.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.check,
            this.Nome,
            this.Cognome,
            this.nomeArte,
            this.email,
            this.dataNascita,
            this.Telefono1,
            this.Comune,
            this.Provincia});
            this.dataGridViewMusicista.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewMusicista.Location = new System.Drawing.Point(2, 70);
            this.dataGridViewMusicista.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewMusicista.Name = "dataGridViewMusicista";
            this.dataGridViewMusicista.RowTemplate.Height = 24;
            this.dataGridViewMusicista.Size = new System.Drawing.Size(1012, 391);
            this.dataGridViewMusicista.TabIndex = 2;
            this.dataGridViewMusicista.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewClickEvent);
            // 
            // check
            // 
            this.check.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.check.HeaderText = "";
            this.check.Name = "check";
            this.check.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.check.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.check.Width = 40;
            // 
            // Nome
            // 
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            this.Nome.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Nome.Width = 200;
            // 
            // Cognome
            // 
            this.Cognome.HeaderText = "Cognome";
            this.Cognome.Name = "Cognome";
            this.Cognome.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Cognome.Width = 200;
            // 
            // nomeArte
            // 
            this.nomeArte.HeaderText = "Nome d\'Arte";
            this.nomeArte.Name = "nomeArte";
            this.nomeArte.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.nomeArte.Width = 200;
            // 
            // email
            // 
            this.email.HeaderText = "Email";
            this.email.Name = "email";
            this.email.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.email.Width = 200;
            // 
            // dataNascita
            // 
            this.dataNascita.HeaderText = "Data di Nascita";
            this.dataNascita.Name = "dataNascita";
            this.dataNascita.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataNascita.Width = 200;
            // 
            // Telefono1
            // 
            this.Telefono1.HeaderText = "Telefono";
            this.Telefono1.Name = "Telefono1";
            this.Telefono1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Telefono1.Width = 200;
            // 
            // Comune
            // 
            this.Comune.HeaderText = "Comune";
            this.Comune.Name = "Comune";
            this.Comune.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Comune.Width = 200;
            // 
            // Provincia
            // 
            this.Provincia.HeaderText = "Provincia";
            this.Provincia.Name = "Provincia";
            this.Provincia.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Provincia.Width = 200;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(538, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Inserire Musicista (nome, cognome, nome d\'arte, email, provicia, comune, telefono" +
    "..)";
            // 
            // CercaMusicista
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 514);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CercaMusicista";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = ".. Cerca un Musicista ..";
            this.Load += new System.EventHandler(this.CercaMusicista_Load);
            ((System.ComponentModel.ISupportInitialize)(this.madDBMapperDataContextBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet11)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMusicista)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource madDBMapperDataContextBindingSource;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox textBoxRicercaMusicista;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.DataGridView dataGridViewMusicista;
        private DataSet1 dataSet11;
        private System.Windows.Forms.Button btOk;
        private System.Windows.Forms.DataGridViewCheckBoxColumn check;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cognome;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeArte;
        private System.Windows.Forms.DataGridViewTextBoxColumn email;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataNascita;
        private System.Windows.Forms.DataGridViewTextBoxColumn Telefono1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comune;
        private System.Windows.Forms.DataGridViewTextBoxColumn Provincia;
    }
}