﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Transactions;

namespace MusicArounD
{
    public partial class Ente : DBEngineForm
    {
        private DOMINIO_ENTI ente = new DOMINIO_ENTI();
        public Ente()
        {
            InitializeComponent();
            comboBoxCategoriaEnte.Items.AddRange(madEngine.getEntityTypes().ToArray());

            comboBoxCatena.Items.Add(new MDE.Item("--NO CATENA--", -1));
            comboBoxCatena.SelectedIndex = 0;
            comboBoxCatena.Items.AddRange(madEngine.getCATENAs().ToArray());

            comboBoxPaternitaEnte.Items.Add(new MDE.Item("--NO PADRE--", -1));

            comboBoxPaternitaEnte.SelectedIndex = 0;
        }

 
        private void NuovoEnte_Load(object sender, EventArgs e)
        {
        }

        private void comboBoxCategoriaEnte_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCategoriaEnte.SelectedItem != null)
            {
                switch (((MDE.Item)comboBoxCategoriaEnte.SelectedItem).ID)
                {
                    case MDE.EFORMAZIONE_IDX:
                        checkBoxFormazione.Visible = true;
                        checkBoxStabile.Visible = false;
                        break;
                    case MDE.ESTABILE_IDX:
                        checkBoxFormazione.Visible = false;
                        checkBoxStabile.Visible = true;
                        break;
                    default:
                        checkBoxFormazione.Visible = false;
                        checkBoxStabile.Visible = false;
                        break;
                }
            }
            comboBoxTipologiaEnte.Items.Clear();
            comboBoxTipologiaEnte.Items.AddRange( madEngine.getTipologyTypes_byCategoryTypeID(((MDE.Item)comboBoxCategoriaEnte.SelectedItem).ID).ToArray());
        }

        private bool isStringsCoupleValid(string str1, string str2, bool isNullAdmitted)
        {
            return ((!str1.Equals("") && !str2.Equals("")) ||
                (str1.Equals("") && str2.Equals("") && isNullAdmitted));
        }

        private bool isValidAddress()
        {
            bool resultViaCivico = isStringsCoupleValid(textBoxVia.Text, textBoxCivico.Text, true);
            bool resultComuneCap = (comboBoxComune.SelectedIndex > -1) && !textBoxCAP.Text.Equals("");

            if (resultComuneCap)
            {
                if (resultViaCivico)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private bool checkControls()
        {
            bool result = true;
            String message = "";
            if (comboBoxCategoriaEnte.SelectedIndex == -1)
            {
                message += "- " + MusicArounD.Properties.Resources.ERROR_NOENTE + "\n";
                result = false;
            }
            if (comboBoxTipologiaEnte.SelectedIndex == -1)
            {
                message += "- " + MusicArounD.Properties.Resources.ERROR_NOTIPOLOGIA + "\n";
                result = false;
            }
            if (!MDE.RegexUtils.IsValidEmail(textBoxEmail.Text))
            {
                message += "- " + MusicArounD.Properties.Resources.ERROR_NOTMAIL + "\n";
                result = false;
            }
            if (textBoxNomeEnte.Text.Equals("")) {
                message += "- " + MusicArounD.Properties.Resources.ERROR_NOENTENOME + "\n";
                result = false;
            }
            if (!MDE.RegexUtils.isValidTelephoneNumber( textBoxTel1.Text, false ) ||
                !MDE.RegexUtils.isValidTelephoneNumber( textBoxTel2.Text, true  ) ||
                !MDE.RegexUtils.isValidTelephoneNumber( textBoxTel3.Text, true  ))
            {
                message += "- " + MusicArounD.Properties.Resources.ERROR_NOTTEL + "\n";
                result = false;
            }
            if (!MDE.RegexUtils.isValidURI( textBoxSitoWeb.Text, true))
            {
                message += "- " + MusicArounD.Properties.Resources.ERROR_NOTURI + "\n";
                result = false;
            }
            if (!isValidAddress())
            {
                message += "- " + MusicArounD.Properties.Resources.ERROR_NOTADDRESS + "\n";
                result = false;                                                
            }
            if (!result)
            {
                MessageBox.Show(message, MusicArounD.Properties.Resources.ERROR_NODATA, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return result;
        }

        private void fillUI()
        {
            //DOMINIO_ENTI ente =  new DOMINIO_ENTI();
            checkBoxAdmin.Checked = ente.amministratore;
            textBoxVia.Text = ente.via != null ? ente.via : "";
            textBoxCivico.Text = ente.civico != null ? ente.civico : "";
            textBoxCAP.Text = madEngine.getCAP_byComune(ente.comuneResidenza).Single().cap1;
            comboBoxComune.SelectedItem = ente.comuneResidenza;
            //ente.provinciaResidenza = indirizzoAttivato ? madEngine.getProvincia_byCAP(textBoxCAP.Text).id : null;

            ente.email = textBoxEmail.Text;
            ente.idCatena = ((MDE.Item)comboBoxCatena.SelectedItem).ID != -1 ? ((MDE.Item)comboBoxCatena.SelectedItem).ID : (int?)null;
            ente.nome = textBoxNomeEnte.Text;
            ente.padreFiglio = ((MDE.Item)comboBoxPaternitaEnte.SelectedItem).ID != -1 ? ((MDE.Item)comboBoxCatena.SelectedItem).Value : null;
            ente.sedePrincipale = checkBoxSedePrincipale.Checked;
            ente.sitoWeb = textBoxSitoWeb.Text.Equals("") ? null : textBoxSitoWeb.Text;
            ente.telefono1 = textBoxTel1.Text;
            ente.telefono2 = textBoxTel2.Text.Equals("") ? null : textBoxTel2.Text;
            ente.telefono3 = textBoxTel3.Text.Equals("") ? null : textBoxTel3.Text;
            

        }
        private DOMINIO_ENTI setData()
        {
            bool indirizzoAttivato = isValidAddress() ;
            //DOMINIO_ENTI ente =  new DOMINIO_ENTI();
            ente.amministratore = checkBoxAdmin.Checked;
            ente.via = indirizzoAttivato && !textBoxVia.Text.Equals("") ? textBoxVia.Text : null;
            ente.civico = indirizzoAttivato && !textBoxCivico.Text.Equals("") ? textBoxCivico.Text : null;
            ente.comuneResidenza = indirizzoAttivato ? ((COMUNE)comboBoxComune.SelectedItem).nome : null;
            ente.provinciaResidenza = indirizzoAttivato ? madEngine.getPROVINCIA_byCAP(textBoxCAP.Text).id : null;
            
            ente.email = textBoxEmail.Text;
            ente.idCatena = ((MDE.Item) comboBoxCatena.SelectedItem).ID != -1 ? ((MDE.Item) comboBoxCatena.SelectedItem).ID : (int?) null;
            ente.nome = textBoxNomeEnte.Text;
            ente.padreFiglio = ((MDE.Item) comboBoxPaternitaEnte.SelectedItem).ID != -1 ? ((MDE.Item) comboBoxCatena.SelectedItem).Value : null;
            ente.sedePrincipale = checkBoxSedePrincipale.Checked;
            ente.sitoWeb = textBoxSitoWeb.Text.Equals("") ? null : textBoxSitoWeb.Text;
            ente.telefono1 = textBoxTel1.Text;
            ente.telefono2 = textBoxTel2.Text.Equals("") ? null : textBoxTel2.Text;
            ente.telefono3 = textBoxTel3.Text.Equals("") ? null : textBoxTel3.Text;


            switch (((MDE.Item)comboBoxCategoriaEnte.SelectedItem).ID)
            {
                case MDE.EFORMAZIONE_IDX:
                    ente.ENTE_FORMATIVO = new ENTE_FORMATIVO
                    {
                        abilitataCertificazioni = checkBoxFormazione.Checked,
                        tipo = ((MDE.Item)comboBoxTipologiaEnte.SelectedItem).Value
                    };
                    ente.ENTE_SERVIZIO = null;
                    ente.ENTE_STABILE = null;
                    break;
                case MDE.ESTABILE_IDX:
                    ente.ENTE_STABILE = new ENTE_STABILE
                    {
                        gestisceStanze = checkBoxStabile.Checked,
                        idxGranularita = 2,
                        tipo = ((MDE.Item)comboBoxTipologiaEnte.SelectedItem).Value
                    };
                    ente.ENTE_FORMATIVO = null;
                    ente.ENTE_SERVIZIO = null;
                    break;
                case MDE.ESERVIZIO_IDX:
                    ente.ENTE_SERVIZIO = new ENTE_SERVIZIO();    
                    switch (((MDE.Item)comboBoxTipologiaEnte.SelectedItem).ID)
                    {
                        case MDE.ESERVIZIO_LIUTATIO_IDX:
                            ente.ENTE_SERVIZIO.LIUTAIO = new LIUTAIO();
                            break;
                        case MDE.ESERVIZIO_LOCALE_IDX:
                            ente.ENTE_SERVIZIO.LOCALE = new LOCALE();
                            break;
                        case MDE.ESERVIZIO_NEGOZIO_IDX:
                            ente.ENTE_SERVIZIO.NEGOZIO = new NEGOZIO();
                            break;
                        case MDE.ESERVIZIO_SERVICE_IDX:
                            ente.ENTE_SERVIZIO.SERVICE = new SERVICE();
                            break;
                    }
                    ente.ENTE_FORMATIVO = null;
                    ente.ENTE_STABILE = null;
                    break;
            }
            return ente;
        }
        private void buttonNext_Click(object sender, EventArgs e)
        {
            try
            {
                DOMINIO_ENTI de;
                if (checkControls())
                {
                    de = setData();

                    CalendarioEnte calFrm = new CalendarioEnte(de, true);
                    if (calFrm.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                    {
                        madEngine.getDataContext().DOMINIO_ENTI.InsertOnSubmit(de);
                        madEngine.getDataContext().SubmitChanges();                           
                    }
                }
            }
            catch (System.Transactions.TransactionException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                madEngine.getDataContext().Dispose();
            }
        }

        private void textBoxCAP_TextChanged(object sender, EventArgs e)
        {

            if (textBoxCAP.Text.Length == 5)
            {
                comboBoxComune.Items.Clear();
                comboBoxComune.Items.AddRange(madEngine.getCOMUNE_byCAP(textBoxCAP.Text).ToArray());
            }
        }



        override public void Commit(Enlistment enlistment)
        {
            MessageBox.Show("Committed");
            enlistment.Done();
        }

        override public void InDoubt(Enlistment enlistment)
        {
            throw new NotImplementedException();
        }

        override public void Prepare(PreparingEnlistment preparingEnlistment)
        {
            MessageBox.Show("Prepare in corso");
            //throw new NotImplementedException();
            preparingEnlistment.Prepared();
        }

        override public void Rollback(Enlistment enlistment)
        {
            enlistment.Done();
            //throw new NotImplementedException();
        }

        private void comboBoxPaternitaEnte_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
