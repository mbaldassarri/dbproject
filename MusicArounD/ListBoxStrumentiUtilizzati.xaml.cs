﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MusicArounD
{
    /// <summary>
    /// Logica di interazione per ListBoxStrumentiUtilizzati.xaml
    /// </summary>
    public partial class ListBoxStrumentiUtilizzati : System.Windows.Controls.UserControl
    {
        private List<StrumentiUtilizzatiItem> fullSet = new List<StrumentiUtilizzatiItem>();
        private String _Filter;
        public System.Collections.IList SelectedItems
        {
            get
            {
                return lbStrumentiUtilizzati.SelectedItems;
            }
        }
        public object SelectedItem
        {
            get
            {
                return lbStrumentiUtilizzati.SelectedItem;
            }
        }
        public ItemCollection Items
        {
            get
            {
                return lbStrumentiUtilizzati.Items;
            }
        }
        public List<STRUMENTO_SUONATO> getStrumentiSuonati ()
        {
            return fullSet.Select(p => p.strumentoSuonato).ToList();
        }
        public void Remove(object removeItem)
        {
            lbStrumentiUtilizzati.Items.Remove(removeItem);
            fullSet.Remove((StrumentiUtilizzatiItem)removeItem);
        }
        public void Clear()
        {
            fullSet.Clear();
            lbStrumentiUtilizzati.Items.Clear();
            _Filter = "";
        }
        public ListBoxStrumentiUtilizzati()
        {
            InitializeComponent();

            //Inizializzazione del filtro
            _Filter = "";
        }



        private bool isAlreadyUsedCategory(CATEGORIA cat)
        {
            var result = false;
            foreach (StrumentiUtilizzatiItem strumentoItem in fullSet)
            {
                if (cat.Equals(strumentoItem.strumentoSuonato.CATEGORIA))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        public STRUMENTO_SUONATO Add(CATEGORIA categoria)
        {
            // Esecuzione bare metal
            return Add(categoria, false);
        }
        public STRUMENTO_SUONATO Add(STRUMENTO_SUONATO strumento)
        {
            // Esecuzione bare metal
            return Add(strumento, false);
        }
        public STRUMENTO_SUONATO Add(CATEGORIA categoria, bool dryRun)
        {
            bool isAlreadyUsed;
            StrumentiUtilizzatiItem strumentoItem = null;

            isAlreadyUsed = isAlreadyUsedCategory(categoria);

            if (!isAlreadyUsed)
            {
                strumentoItem = new StrumentiUtilizzatiItem(categoria);
                strumentoItem.Experience = 1;
                // Se è dryRun allora l'elemento non viene aggiunto realmente al controllo
                if (!dryRun)
                {
                    if (strumentoItem.Title.ToLower().Contains(Filter.ToLower()))
                    {
                        lbStrumentiUtilizzati.Items.Add(strumentoItem);
                    }
                    fullSet.Add(strumentoItem);
                }
            }
            return !isAlreadyUsed ? strumentoItem.strumentoSuonato : null;
        }

        public STRUMENTO_SUONATO Add(STRUMENTO_SUONATO strumento, bool dryRun)
        {
            bool isAlreadyUsed;
            StrumentiUtilizzatiItem strumentoItem = null;

            isAlreadyUsed = isAlreadyUsedCategory(strumento.CATEGORIA);

            if (!isAlreadyUsed)
            {
                strumentoItem = new StrumentiUtilizzatiItem(strumento);
                // Se è dryRun allora l'elemento non viene aggiunto realmente al controllo
                if (!dryRun)
                {
                    if (strumentoItem.Title.ToLower().Contains(Filter.ToLower()))
                    {
                        lbStrumentiUtilizzati.Items.Add(strumentoItem);
                    }
                    fullSet.Add(strumentoItem);
                }
            }
            return !isAlreadyUsed ? strumentoItem.strumentoSuonato : null;
        }

        public List<STRUMENTO_SUONATO> AddRange(List<CATEGORIA> listaCat)
        {
            STRUMENTO_SUONATO strumentoSuonato = null;
            List<STRUMENTO_SUONATO> result = new List<STRUMENTO_SUONATO>();
            foreach (CATEGORIA cat in listaCat)
            {
                strumentoSuonato = Add(cat);
                if (strumentoSuonato != null)
                {
                    result.Add(strumentoSuonato);
                }
            }
            return result;
        }

        public List<STRUMENTO_SUONATO> AddRange(List<STRUMENTO_SUONATO> listaStrum)
        {
            STRUMENTO_SUONATO strumentoSuonato = null;
            List<STRUMENTO_SUONATO> result = new List<STRUMENTO_SUONATO>();
            foreach (STRUMENTO_SUONATO strumento in listaStrum)
            {
                strumentoSuonato = Add(strumento);
                if (strumentoSuonato != null)
                {
                    result.Add(strumentoSuonato);
                }
            }
            return result;
        }
        public String Filter {
            get
            {
                return _Filter;
            }
            set
            {
                this._Filter = value;
                
                lbStrumentiUtilizzati.Items.Clear();
                foreach (StrumentiUtilizzatiItem su in fullSet)
                {
                    if (su.Title.ToLower().Contains(Filter.ToLower()))
                    {
                        lbStrumentiUtilizzati.Items.Add(su);
                    }
                }

            }
        }
    }

    public class StrumentiUtilizzatiItem
    {
        public STRUMENTO_SUONATO strumentoSuonato;
        public StrumentiUtilizzatiItem(CATEGORIA categoria)
        {
            this.strumentoSuonato = new STRUMENTO_SUONATO
            {
                CATEGORIA = categoria
            };
        }
        public StrumentiUtilizzatiItem(STRUMENTO_SUONATO strumento)
        {
            this.strumentoSuonato = strumento;
        }

        public string Title
        {
            get
            {
                return strumentoSuonato.CATEGORIA.ToString();
            }
        }
        public int Experience { 
            get
            {
                return strumentoSuonato.anniEsperienza;
            }
            set
            {
                strumentoSuonato.anniEsperienza = value;
            }
        }
        public bool ToRemove { get; set; }
    }
}
