﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusicArounD
{
    
    public partial class BandFrm : DBEngineForm
    {
        List<MUSICISTA> listaMusicistiDisponibili;

        private callType internalCallType;
        private BAND internalBand;

        public enum callType
        {
            UPDATE,
            UPDATE_AND_NEW,
            INSERT,
            INSERT_AND_NEW
        }

        // Costruttore di insert.
        // Se isNewActionEnabled è true allora viene abilitato il bottone "Inserisci e crea nuovo..."
        // Se isNewActionEnabled è false allora viene disabilitato il bottone "Inserisci e crea nuovo..."
        public BandFrm(bool isNewActionEnabled)
        {
            InitializeComponent();
            internalCallType = isNewActionEnabled ? callType.INSERT_AND_NEW : callType.INSERT;
            internalBand = new BAND()
            {
                dataFormazione = DateTime.Now
            };
            loadData(internalBand);
            fillUI(internalBand);
        }

        // Costruttore di insert.
        // Viene disabilitato il bottone "Inserisci e crea nuovo..."
        public BandFrm() : this(false) { }

        // Costruttore di update.
        // Viene disabilitato il bottone "Aggiorna e continua a modificare..."
        public BandFrm(int idBand) : this(idBand, false) { }

        // Costruttore di update.
        // Se isNewActionEnabled è true allora viene abilitato il bottone "Aggiorna e continua a modificare..."
        // Se isNewActionEnabled è false allora viene disabilitato il bottone "Aggiorna e continua a modificare..."
        public BandFrm(int idBand, bool isNewActionEnabled) : this()
        {
            internalCallType = isNewActionEnabled ? callType.UPDATE_AND_NEW : callType.UPDATE;
            internalBand = madEngine.getBAND_byID(idBand);
            loadData(internalBand);
            fillUI(internalBand);
        }
        private void loadData(BAND band)
        {
            // Carica la cache della lista degli artisti
            listaMusicistiDisponibili = madEngine.getMusicista_byFilter("").ToList();

            // Impone il maxdate
            dataFormazione.MaxDate = DateTime.Now;
        }

        private void fillUI(BAND band)
        {
            // Carica i dati base
            textBoxNomeBand.Text = band.nome;
            dataFormazione.Value = band.dataFormazione;

            // Carica lista musicisti nella ricerca dei componenti disponibili
            checkedListBoxMusicistiDisponibili.Items.Clear();
            checkedListBoxMusicistiDisponibili.Items.AddRange(listaMusicistiDisponibili.ToArray());

            // Carica lista musicisti nella ricerca dei referenti disponibili per la band
            comboBoxCognomeReferenteInterno.Items.Clear();
            comboBoxCognomeReferenteInterno.Items.AddRange(listaMusicistiDisponibili.ToArray());

            // Carica storico della band
            listBoxStoricoBand1.Clear();
            listBoxStoricoBand1.AddRange(band.ENTRATA_USCITA.ToList());
            

            // Caricamento del referente in base alla tipologia (referente esterno o interno o assente)
            if (band.MUSICISTA == null &&
                band.nomeReferente == null &&
                band.telefonoReferente == null)
            {
                radioButtonNoReferente.Checked = true;
                comboBoxCognomeReferenteInterno.Text = "";
                comboBoxTelefonoReferenteInterno.Items.Clear();
                comboBoxTelefonoReferenteInterno.Text = "";
            }
            else
            {
                if (band.MUSICISTA != null)
                {
                    radioButtonReferenteInterno.Checked = true;
                    comboBoxCognomeReferenteInterno.SelectedItem = band.MUSICISTA;
                    textBoxCognomeReferenteEsterno.Text = "";
                    textBoxTelefonoReferenteEsterno.Text = "";
                } else {
                    radioButtonReferenteEsterno.Checked = true;
                    textBoxCognomeReferenteEsterno.Text = band.nomeReferente;
                    textBoxTelefonoReferenteEsterno.Text = band.telefonoReferente;
                    comboBoxCognomeReferenteInterno.Text = "";
                    comboBoxTelefonoReferenteInterno.Items.Clear();
                    comboBoxTelefonoReferenteInterno.Text = "";
                }
            }

            // Carica lista provincie (ed implicitamente in cascata i comuni)
            comboBoxProvincia.Items.Clear();
            comboBoxComune.Items.Clear();
            comboBoxProvincia.Items.AddRange(madEngine.getProvincie().ToArray());

            // Seleziona la provincia ed il comune
            if (band.COMUNE != null)
            {
                comboBoxProvincia.SelectedItem = band.COMUNE.PROVINCIA;
                comboBoxComune.SelectedItem = band.COMUNE;
            }
            else
            {
                comboBoxProvincia.Text = "";
                comboBoxComune.Text = "";
            }

            // Pulisce filtri di ricerca per lo storico e la sua ricerca
            textBoxFiltroMusicistiDisponibili.Text = "";
            textBoxFiltroMusicistiUtilizzati.Text = "";
        }

        private void changeActionButtons()
        {
            switch (internalCallType)
            {
                case callType.INSERT:
                    buttonAction.Text = MusicArounD.Properties.Resources.BUTTON_INSERT;
                    buttonAction_and_new.Visible = false;
                    break;
                case callType.INSERT_AND_NEW:
                    buttonAction.Text = MusicArounD.Properties.Resources.BUTTON_INSERT;
                    buttonAction_and_new.Text = MusicArounD.Properties.Resources.BUTTON_INSERT_AND_NEW;
                    buttonAction_and_new.Visible = true;
                    break;
                case callType.UPDATE:
                    buttonAction.Text = MusicArounD.Properties.Resources.BUTTON_UPDATE;
                    buttonAction_and_new.Visible = false;
                    break;
                case callType.UPDATE_AND_NEW:
                    buttonAction.Text = MusicArounD.Properties.Resources.BUTTON_UPDATE;
                    buttonAction_and_new.Text = MusicArounD.Properties.Resources.BUTTON_UPDATE_AND_NEW;
                    buttonAction_and_new.Visible = true;
                    break;

            }
        }
        private BAND doInsertOrUpdateData(BAND band)
        {
            // Compilazione dei dati base
            band.nome = textBoxNomeBand.Text;
            band.dataFormazione = dataFormazione.Value.Date;
            band.solista = false;
            band.COMUNE = (COMUNE)comboBoxComune.SelectedItem;

            // Aggiunta del referente (libero oppure musicista interno)
            if (radioButtonReferenteInterno.Checked)
            {
                band.MUSICISTA = (MUSICISTA)comboBoxCognomeReferenteInterno.SelectedItem;
                band.nomeReferente = null;
                band.telefonoReferente = null;
                //se l'utente non seleziona nessun telefono ma solo il nome del referente interno, prendi comunque nota del nome e del primo telefono. 
                //se non esiste telefono,salva solo il nome del referente

                //if (comboBoxCognomeReferenteInterno.SelectedItem != null && comboBoxTelefonoReferenteInterno.SelectedItem == null)
                //{
                //    result.nomeReferente = (string)comboBoxCognomeReferenteInterno.SelectedItem;
                //   List<string> telefoni = MDE.getTelefonos_byMUSICISTA((MUSICISTA)comboBoxCognomeReferenteInterno.SelectedItem);
                    
                //    result.telefonoReferente = madEngine.getTelefono_byMusicista();
                //}
            }
            if (radioButtonReferenteEsterno.Checked)
            {
                band.MUSICISTA = null;
                band.nomeReferente = textBoxCognomeReferenteEsterno.Text;
                band.telefonoReferente = textBoxTelefonoReferenteEsterno.Text;
            }
            if (radioButtonNoReferente.Checked)
            {
                band.MUSICISTA = null;
                band.nomeReferente = null;
                band.telefonoReferente = null;
            }

            // Aggiunta delle tuple dello storico ENTRATA_USCITA alla band (vengono aggiunte solo le tuple non ancora assegnate)
            List<ENTRATA_USCITA> listaRecreate = listBoxStoricoBand1.RecreateItems;
            madEngine.getDataContext().ENTRATA_USCITA.DeleteAllOnSubmit(listaRecreate);
            listaRecreate.ForEach(p => band.ENTRATA_USCITA.Add(new ENTRATA_USCITA
                {
                    BAND = p.BAND,
                    MUSICISTA1 = p.MUSICISTA1,
                    data = p.data,
                    e_u = p.e_u
                }));
            band.ENTRATA_USCITA.AddRange(listBoxStoricoBand1.StoricoSanedItems.Where(p => p.BAND == null));
            return band;
        }

        private void BandFrm_Load(object sender, EventArgs e)
        {
            // Cambia il nome ai bottoni in base al callType
            changeActionButtons();
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool doSaveData()
        {
            bool result = false;

            try
            {
                madEngine.getDataContext().SubmitChanges(ConflictMode.FailOnFirstConflict);
                switch (internalCallType)
                {
                    case callType.INSERT:
                    case callType.INSERT_AND_NEW:
                        MessageBox.Show(MusicArounD.Properties.Resources.SUCCESS_QUERY_INSERT,
                            MusicArounD.Properties.Resources.MESSAGETITLE_BAND_NEW,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    case callType.UPDATE:
                    case callType.UPDATE_AND_NEW:
                        MessageBox.Show(MusicArounD.Properties.Resources.SUCCESS_QUERY_UPDATE,
                            MusicArounD.Properties.Resources.MESSAGETITLE_BAND_UPDATE,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    default:
                        break;
                }
                result = true;

            }
            catch (SqlException sqlex)
            {
                switch (internalCallType)
                {
                    case callType.INSERT:
                    case callType.INSERT_AND_NEW:
                        MessageBox.Show(MusicArounD.Properties.Resources.ERROR_QUERY_INSERT + "\n" + sqlex,
                            MusicArounD.Properties.Resources.MESSAGETITLE_ERRORNAME,
                            MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        break;
                    case callType.UPDATE:
                    case callType.UPDATE_AND_NEW:
                        MessageBox.Show(MusicArounD.Properties.Resources.ERROR_QUERY_UPDATE + "\n" + sqlex,
                            MusicArounD.Properties.Resources.MESSAGETITLE_ERRORNAME,
                            MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        break;
                    default:
                        break;
                }
                result = false;
            }
            return result;
        }
        private bool doSaveProcedure()
        {
            bool result = false;
            if (checkFields())
            {
                //switch (internalCallType)
                //{
                //    case callType.INSERT:
                //    case callType.INSERT_AND_NEW:
                //        band = this.getData(new BAND());
                //        break;
                //    case callType.UPDATE:
                //    case callType.UPDATE_AND_NEW:
                //        band = this.getData(internalBand);
                //        break;
                //    default:
                //        // codice che non dovrebbe mai essere raggiunto
                //        band = null;
                //        break;
                //}
                doInsertOrUpdateData(internalBand);
                result = doSaveData();
            }
            else
            {
                result = false;
            }
            return result;
        }
        private void buttonAction_Click(object sender, EventArgs e)
        {
            if (doSaveProcedure())
            {
                this.Close();
            }
        }

        private bool checkFields()
        {
            bool result = true;
            String message = "";
           
            //nome band
            if (textBoxNomeBand.Text.Equals(""))
            {
                message += "- " + MusicArounD.Properties.Resources.ERROR_BAND_NONAME + "\n";
                result = false;
            }
            
            //referente esterno checked
            if (radioButtonReferenteEsterno.Checked)
            {
                if (textBoxCognomeReferenteEsterno.Text.Equals("")) 
                {
                    message += "- " + MusicArounD.Properties.Resources.ERROR_BAND_NONAME_REFERENTE + "\n";
                    result = false;
                }
                if (!MDE.RegexUtils.isValidTelephoneNumber(textBoxTelefonoReferenteEsterno.Text, false))
                {
                    message += "- " + MusicArounD.Properties.Resources.ERROR_BAND_NOTTEL_REFERENTE + "\n";
                    result = false;
                }
            }

            //referente interno checked
            if (radioButtonReferenteInterno.Checked)
            {
                if (comboBoxCognomeReferenteInterno.SelectedIndex == -1)
                {
                    message += "- " + MusicArounD.Properties.Resources.ERROR_BAND_NONAME_REFERENTE + "\n";
                    result = false;
                }
            }

            //nessun comune o provincia selezionati
            if (comboBoxComune.SelectedIndex == -1 || comboBoxProvincia.SelectedIndex == -1)
            {
                message += "- " + MusicArounD.Properties.Resources.ERROR_BAND_NOTADDRESS + "\n";
                result = false;
            }
            if (!isValidStoricoBand())
            {
                message += "- " + MusicArounD.Properties.Resources.ERROR_BAND_DUPLICATEDDATE + "\n";
                result = false;
            }
            if (!result)
            {
                MessageBox.Show(message, MusicArounD.Properties.Resources.ERROR_NODATA, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return result;

        }

        private bool isValidStoricoBand()
        {
            bool result = true;
            List<DateTime> listaDateTime;
            List<string> listaShortDateTime = new List<string>();
            List<MUSICISTA> listaMusicisti = listBoxStoricoBand1.StoricoItems.Select(p => p.MUSICISTA1).Distinct().ToList();
            foreach (MUSICISTA musicista in listaMusicisti)
            {
                listaDateTime = listBoxStoricoBand1.getENTRATA_USCITAs_byMUSICISTA(musicista).OrderBy(p => p.data).Select(p => p.data).ToList();
                
                listaShortDateTime.Clear();
                listaDateTime.ForEach(p => listaShortDateTime.Add(p.ToString("d")));

                // Se la lista delle date del musicista e differente nel count dalla sua omonima calcolata con distinct allora c'è duplicazione
                if (listaShortDateTime.Count() != listaShortDateTime.Distinct().Count())
                {
                    result = false;
                    break;
                }
            }
            return result;
        }

        private void comboBoxProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxProvincia.SelectedIndex != -1)
            {
                comboBoxComune.Items.Clear();
                comboBoxComune.Items.AddRange(madEngine.getComune_ByProvincia(
                    ((PROVINCIA)comboBoxProvincia.SelectedItem).nome).ToArray());
            }
        }
        
        private void textBoxFiltroMusicistiDisponibili_TextChanged(object sender, EventArgs e)
        {
            if (textBoxFiltroMusicistiDisponibili.Text.Length == 0)
            {
                checkedListBoxMusicistiDisponibili.Items.Clear();
                
            }
            if (textBoxFiltroMusicistiDisponibili.Text.Length >= 3)
            {
                checkedListBoxMusicistiDisponibili.Items.Clear();
                checkedListBoxMusicistiDisponibili.Items.AddRange(
                    listaMusicistiDisponibili.Where(
                    c => c.cognome.ToLower().Contains(textBoxFiltroMusicistiDisponibili.Text.ToLower()) || 
                        c.nome.ToLower().Contains(textBoxFiltroMusicistiDisponibili.Text.ToLower()))
                    .ToArray());
            }
        }


        private void addMusicistaToBand_Click(object sender, EventArgs e)
        {
            List<MUSICISTA> listaMusicistiInserimento = checkedListBoxMusicistiDisponibili.CheckedItems.Cast<MUSICISTA>().ToList();
            listBoxStoricoBand1.AddRange(listaMusicistiInserimento);
            madEngine.getDataContext().ENTRATA_USCITA.InsertAllOnSubmit(
                listBoxStoricoBand1.StoricoItems.Where(p => p.BAND == null));
        }

        private void deleteMusicistaFromBand_Click(object sender, EventArgs e)
        {
            foreach (StoricoBandItem item in new System.Collections.ArrayList(listBoxStoricoBand1.Items))
            {
                if (item.ToRemove)
                {
                    madEngine.getDataContext().ENTRATA_USCITA.DeleteOnSubmit(item.entrata_uscita);
                    listBoxStoricoBand1.Remove(item);
                }
            }
        }

        private void textBoxFiltroMusicistiUtilizzati_TextChanged(object sender, EventArgs e)
        {
            listBoxStoricoBand1.Filter = textBoxFiltroMusicistiUtilizzati.Text;
        }

        private void radioButtonReferenteEsterno_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonReferenteEsterno.Checked)
            {
                groupBoxReferenteEsterno.Enabled = true;
                groupBoxReferenteInterno.Enabled = false;
            }
        }

        private void radioButtonReferenteInterno_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonReferenteInterno.Checked)
            {
                groupBoxReferenteInterno.Enabled = true;
                groupBoxReferenteEsterno.Enabled = false;
            }
        }

        private List<MUSICISTA> getMUSICISTA_byToStringFilter(string filter)
        {
            return listaMusicistiDisponibili.Where(p => p.ToString().ToLower().Contains(filter.ToLower())).ToList();
        }
        private void comboBoxCognomeReferenteInterno_TextChanged(object sender, EventArgs e)
        {
            if (comboBoxCognomeReferenteInterno.Text.Length >= 3)
            {
                comboBoxCognomeReferenteInterno.Items.Clear();
                comboBoxCognomeReferenteInterno.Items.AddRange(getMUSICISTA_byToStringFilter(comboBoxCognomeReferenteInterno.Text).ToArray());
                comboBoxCognomeReferenteInterno.DroppedDown = true;
                comboBoxCognomeReferenteInterno.Select(comboBoxCognomeReferenteInterno.Text.Length, 0);
            }
            else
            {
                comboBoxCognomeReferenteInterno.Items.AddRange(madEngine.getMusicista_byFilter("").ToArray());
            }
        }

        private void comboBoxCognomeReferenteInterno_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCognomeReferenteInterno.SelectedIndex != -1)
            {
                comboBoxTelefonoReferenteInterno.Items.Clear();
                comboBoxTelefonoReferenteInterno.Items.AddRange(MDE.getTelefonos_byMUSICISTA((MUSICISTA)comboBoxCognomeReferenteInterno.SelectedItem).ToArray());

            }
        }

        private void comboBoxTelefonoReferenteInterno_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void comboBoxTelefonoReferenteInterno_TextChanged(object sender, EventArgs e)
        {
            comboBoxCognomeReferenteInterno.Items.Clear();
            comboBoxCognomeReferenteInterno.Items.AddRange(
                listaMusicistiDisponibili.Where(p => 
                p.telefono1 != null && p.telefono1.Contains(comboBoxTelefonoReferenteInterno.Text) ||
                p.telefono2 != null && p.telefono2.Contains(comboBoxTelefonoReferenteInterno.Text) ||
                p.telefono3 != null && p.telefono3.Contains(comboBoxTelefonoReferenteInterno.Text))
                .ToArray());
            
            comboBoxCognomeReferenteInterno.Select(comboBoxTelefonoReferenteInterno.Text.Length, 0);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAction_and_new_Click(object sender, EventArgs e)
        {
            bool result = doSaveProcedure();
            if (internalCallType == callType.INSERT_AND_NEW && result) 
            {
                madEngine.renewDataContext();
                internalBand = new BAND()
                {
                    dataFormazione = DateTime.Now
                };
                // Refresh del maxdate
                dataFormazione.MaxDate = DateTime.Now;

                loadData(internalBand);
                fillUI(internalBand);
            }
        }

        private void radioButtonNoReferente_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxReferenteEsterno.Enabled = false;
            groupBoxReferenteInterno.Enabled = false;
        }

       
    }
}
