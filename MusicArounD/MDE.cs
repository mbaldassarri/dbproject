﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Forms;


namespace MusicArounD
{
    public class MDE
    {
        public const int EFORMAZIONE_IDX = 1;
        public const String EFORMAZIONE_VAL = "Ente di formazione";
        
        public const int ESERVIZIO_IDX = 2;
        public const String ESERVIZIO_VAL = "Ente di servizi";
        public const int ESERVIZIO_LIUTATIO_IDX = 1;
        public const String ESERVIZIO_LIUTATIO_VAL = "Liutatio";
        public const int ESERVIZIO_LOCALE_IDX = 2;
        public const String ESERVIZIO_LOCALE_VAL = "Locale";
        public const int ESERVIZIO_NEGOZIO_IDX = 3;
        public const String ESERVIZIO_NEGOZIO_VAL = "Negozio";
        public const int ESERVIZIO_SERVICE_IDX = 4;
        public const String ESERVIZIO_SERVICE_VAL = "Service";
       
        public const int ESTABILE_IDX = 3;
        public const String ESTABILE_VAL = "Ente di stabili";

        private static MDE instance;
        private static RegexUtilitiesClass regexUtilities;
        private MadDBMapperDataContext _db;
        private MadDBMapperDataContext db
        {
            get
            {
                if (_db == null)
                {
                    _db = new MadDBMapperDataContext();

                }
                return _db;
            }
            set
            {
                _db = value;
            }
        }
        
        private MDE() {
            db = new MadDBMapperDataContext();
        }
        public void renewDataContext()
        {
            db = null;
        }
        public MadDBMapperDataContext getDataContext()
        {
            return db;
        }

        // Classe statica di supporto per i giorni della settimana secondo abbreviazione di tre lettere
        public static class Days
        {
            public const string LUN = "LUN";
            public const string MAR = "MAR";
            public const string MER = "MER";
            public const string GIO = "GIO";
            public const string VEN = "VEN";
            public const string SAB = "SAB";
            public const string DOM = "DOM";

        }
        public class RegexUtilitiesClass
        {
            bool invalid = false;
            // Metodo di controllo per validare un indirizzo internet.
            // Se isNullAdmitted è true allora la funzione trova corretto anche un url vuoto
            public bool isValidURI(string strIn, bool isNullAdmitted)
            {
                bool validUri = Uri.IsWellFormedUriString(strIn, UriKind.Absolute);
                return (validUri  || (isNullAdmitted && strIn.Equals("")));
            }
            // Metodo di controllo per validare un numero di telefono.
            // Se isNullAdmitted è true allora la funzione trova corretti anche i numeri di telefono vuoti
            public bool isValidTelephoneNumber(string strIn, bool isNullAdmitted)
            {
                //return (Regex.IsMatch(strIn, @"^[0-9]{5,15}$") || (isNullAdmitted && strIn.Equals("")));
                return (Regex.IsMatch(strIn, @"^((\([+]?[0-9]{2,6}\))|[+]?[0-9]{2,6})?( ?| - |-)[0-9](( | - |-)?[0-9]){4,14}$") || (isNullAdmitted && strIn.Equals("")));

            }
            
            // Metodo di controllo della validità delle caselle email ripreso dalla MSDN Microsoft
            public bool IsValidEmail(string strIn)
            {
                invalid = false;
                if (String.IsNullOrEmpty(strIn))
                    return false;

                // Use IdnMapping class to convert Unicode domain names. 
                try
                {
                    strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
                                          RegexOptions.None, TimeSpan.FromMilliseconds(200));
                }
                catch (RegexMatchTimeoutException)
                {
                    return false;
                }

                if (invalid)
                    return false;

                // Return true if strIn is in valid e-mail format. 
                try
                {
                    return Regex.IsMatch(strIn,
                          @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                          @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                          RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
                }
                catch (RegexMatchTimeoutException)
                {
                    return false;
                }
            }

            private string DomainMapper(Match match)
            {
                // IdnMapping class with default property values.
                IdnMapping idn = new IdnMapping();

                string domainName = match.Groups[2].Value;
                try
                {
                    domainName = idn.GetAscii(domainName);
                }
                catch (ArgumentException)
                {
                    invalid = true;
                }
                return match.Groups[1].Value + domainName;
            }
        }
        // Classe di aiuto per immagazzinare coppie (int) "key" / (string) "value"
        public class Item
        {
            public string Value;
            public int ID;
            public Item(string value, int id)
            {
                Value = value; ID = id;
            }
            public override string ToString()
            {
                return Value;
            }
        }
        // Classe di aiuto per esprimere range temporali di tipo data + orario
        public class DateTimeRange
        {
            private DateTime _Date1;
            private DateTime _Date2;
            public DateTime Date1
            {
                get { return _Date1; }
                set { _Date1 = value; }
            }
            public DateTime Date2
            {
                get { return _Date2; }
                set { _Date2 = value; }
            }
            public DateTimeRange(DateTime date1, DateTime date2)
            {
                Date1 = date1; Date2 = date2;
            }
            public override string ToString()
            {
                return Date1 + " - " + Date2;
            }
        }
        
        // Classe di aiuto per esprimere range temporali di tipo orario
        public class TimeRange
        {
            private TimeSpan _Time1;
            private TimeSpan _Time2;
            public TimeSpan Time1
            {
                get { return _Time1; }
                set { _Time1 = value; }
            }
            public TimeSpan Time2
            {
                get { return _Time2; }
                set { _Time2 = value; }
            }
            public TimeRange(TimeSpan time1, TimeSpan time2)
            {
                Time1 = time1; Time2 = time2;
            }
            public override string ToString()
            {
                return Time1.ToString(@"hh\:mm") + " - " + Time2.ToString(@"hh\:mm");
            }
        }
        // Singleton di questo modello dati
        public static MDE Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MDE();
                }
                return instance;
            }
        }
        // Singleton per la classe di supporto alle espressioni regolari utilizzate
        // nella validazione dei dati delle form del programma
        public static RegexUtilitiesClass RegexUtils
        {
            get
            {
                if (regexUtilities == null)
                {
                    regexUtilities = new RegexUtilitiesClass();
                }
                return regexUtilities;
            }

        }

        // Utility per creare le date del db secondo la specifica TimeSpan
        public static TimeSpan getSQLTime(DateTime dt)
        {
            return new TimeSpan(dt.Hour, dt.Minute, 0);
        }
        
        // Ritorna la lista dei tipi di ente
        public List<Item> getEntityTypes()
        {
            return new List<Item>() 
            { 
                new Item(EFORMAZIONE_VAL, EFORMAZIONE_IDX), 
                new Item(ESERVIZIO_VAL, ESERVIZIO_IDX), 
                new Item(ESTABILE_VAL, ESTABILE_IDX)
            };
        }

        // Ritorna la lista delle tipologie di ente secondo la sua categoria
        public List<Item> getTipologyTypes_byCategoryTypeID(int id)
        {
            List<Item> myCollection = new List<Item>();
            int x = 0;


            switch (id)
            {
                case 0:
                    break;
                case EFORMAZIONE_IDX:
                    x = 0;
                    var queryFormazione = from t in db.TIPOLOGIE_ENTE_FORMATIVO
                                  select t;
                    foreach (var tipo in queryFormazione)
                        myCollection.Add(new Item(tipo.nomeTipologia, ++x));
                    break;
                case ESERVIZIO_IDX:
                    myCollection.Add(new Item(ESERVIZIO_LIUTATIO_VAL,ESERVIZIO_LIUTATIO_IDX));
                    myCollection.Add(new Item(ESERVIZIO_LOCALE_VAL, ESERVIZIO_LOCALE_IDX));
                    myCollection.Add(new Item(ESERVIZIO_NEGOZIO_VAL,ESERVIZIO_NEGOZIO_IDX));
                    myCollection.Add(new Item(ESERVIZIO_SERVICE_VAL,ESERVIZIO_SERVICE_IDX));
                    break;
                case ESTABILE_IDX:
                    x = 0;
                    var queryStabile = from t in db.TIPOLOGIE_ENTE_STABILE
                                  select t;
                    foreach (var tipo in queryStabile)
                        myCollection.Add(new Item(tipo.nomeTipologia, ++x));
                    break;
            }
            return myCollection;
        }

        // Ritorna i periodi calendario appartenenti ad un ente specificato
        public List<PERIODO> getPERIODOs_byDomainEntityID(DOMINIO_ENTI ente)
        {
            return getPERIODOs_byDomainEntityID(ente.email);
        }
        
        // Ritorna i periodi calendario appartenenti ad un ente specificato
        public List<PERIODO> getPERIODOs_byDomainEntityID(string idEnte)
        {
            return (from c in db.PERIODO
                                  where c.CALENDARIO.ente == idEnte
                                  select c).ToList();
        }
        public List<CATENA> getCATENAs () {
            return  (from c in db.CATENA
                select c).ToList();
        }

        
        //dato un musicista viene restituito il suo numero di telefono o null se non è stato inserito precedentemente
        public List<String> getTelefono_byMusicista(String emailMusicista)
        {
            List<MUSICISTA> query = null;
            List<String> numbers = new List<String>();
            try
            {
                query = (from m in db.MUSICISTA
                        where m.cognome == emailMusicista
                        select m).ToList();
                foreach (MUSICISTA mus in query)
                {
                    numbers.Add(mus.telefono1);

                }
                
            }
            catch (Exception ex) { }

            return numbers;
       
        }

        //Restituisce un insieme di stringhe contenenti tutti i cognomi dei musicisti
        public AutoCompleteStringCollection getMusicistaAutoComplete(String cognome)
        {
            var infos = db.MUSICISTA.Where(c => c.cognome.ToLower().StartsWith(cognome.ToLower()))
                .Select(c => c.cognome)
                .Distinct().ToArray();

            AutoCompleteStringCollection source = new AutoCompleteStringCollection();
            source.AddRange(infos);
            return source;
        }

        //seleziona tutte le provincie e le inserisce in una lista
        public List<PROVINCIA> getProvincie()
        {
            return db.PROVINCIA.ToList();
           
        }
        //data una provincia, restituisce i comuni associati
        public List<COMUNE> getComune_ByProvincia(String prov)
        {
            return (from c in db.COMUNE
                    where c.PROVINCIA.nome.ToLower().Contains(prov.ToLower())
                    select c).ToList();
                        
        }


        //Data una stringa di ricerca, restituisce tutti i musicisti che contengono quella stringa
        public List<MUSICISTA> getMusicista_byFilter(String filter)
        {
            return (from c in db.MUSICISTA
                    where c.cognome.ToLower().Contains(filter.ToLower()) || c.nome.ToLower().Contains(filter.ToLower())
                    select c).ToList();
        }

        public List<BAND> getBand_byFilter(String filter)
        {
            return (from c in db.BAND
                    where c.nome.ToLower().Contains(filter.ToLower()) || c.provinciaBand.ToLower().Contains(filter.ToLower()) || c.comuneBand.ToLower().Contains(filter.ToLower())
                        || c.musicistaReferente.ToLower().Contains(filter.ToLower()) || c.telefonoReferente.ToLower().Contains(filter.ToLower()) || c.nomeReferente.ToLower().Contains(filter.ToLower())
                    select c).ToList();

        }

        // Ritorna la lista di tutti i cap presenti nel db
        public List<CAP> getCAPS()
        {
            return db.CAP.ToList();
        }
        // Ritorna la lista di comuni che hanno come cap quello indicato
        public List<COMUNE> getCOMUNE_byCAP(String cap) 
        {
            List<COMUNE> myCollection = new List<COMUNE>();
            List<CAP> capList = filterCAP(cap);
            foreach (var cl in capList)
            {
                var query = from c in db.COMUNE
                             join a in db.APPARTENENZA_CAP on c.nome equals a.nomeComune
                             where a.cap == cl.cap1
                             select c;
                myCollection.AddRange(query.ToList());
            }
            return myCollection;
        }

        // Ritorna la lista di cap del comune indicato (se la stringa è parziale fa match con più comuni e di conseguenza con più cap)
        public List<CAP> getCAP_byComune(String comune)
        {
            return (from c in db.CAP
                    join a in db.APPARTENENZA_CAP on c.cap1 equals a.cap
                    where a.COMUNE.nome.ToLower().Contains(comune.ToLower())
                    orderby c.cap1
                    select c).ToList();
  
        }

        // Ritorna la lista di cap che matchano parzialmente il cap indicato
        // Tiene conto dei caratteri jolly "x" nella tabella dei cap del db.
        // Ad esempio il cap di Roma è 001xx, che indica che al posto delle x ci possono essere due numeri (da 00100 a 00199)
        private List<CAP> filterCAP(String cap, int depth, int maxdepth)
        {
            int x=0;
            String xString = "";
            var query = from c in db.CAP
                             where c.cap1.StartsWith(cap)
                             select c;
            if (query.Count() == 0) 
            {
                if (depth < maxdepth && cap.Length >= maxdepth)
                {
                    for (x = 0; x <= depth; x++)
                    {
                        xString += "x";
                    }
                    return filterCAP((cap.Substring(0, cap.Length - (depth + 1)) + xString), depth + 1, maxdepth);
                }
            }
            return query.ToList();
        }
        // Ritorna la lista di cap che matchano parzialmente il cap indicato
        public List<CAP> filterCAP(String cap)
        {
                return filterCAP (cap, 0, 3);
        }
        // Ritorna la lista di cap che matchano parzialmente il cap indicato
        public List<CAP> filterCAP(CAP cap)
        {
            return filterCAP(cap.cap1);
        }

        // Ritorna la provincia che possiede il cap indicato
        public PROVINCIA getPROVINCIA_byCAP(String cap)
        {
            CAP tmpCAP;
            List<CAP> listCAP = filterCAP(cap);
            if (listCAP.Count > 0)
            {
                tmpCAP = listCAP.Single();
            }
            else
            {
                tmpCAP = new CAP { cap1 = cap };
            }

            var queryCAP = from a in db.APPARTENENZA_CAP
                        where a.cap == tmpCAP.cap1
                        select new { a.idProvincia };
            if (queryCAP.Count() != 0)
            {
                return (from p in db.PROVINCIA
                        where p.id == queryCAP.First().idProvincia
                        select p).Single();
            }
            return null;
        }
        // Ritorna la provincia che possiede il cap indicato
        public PROVINCIA getPROVINCIA_byCAP(CAP cap)
        {
            return getPROVINCIA_byCAP(cap.cap1);
        }
        
        // Ritorna tutti i periodi secondo uno dei tre parametri presi singolamrente oppure in combinazione libera tra essi
        public List<PERIODO> searchPERIODOs(DateTime dataInizio, DateTime dataFine, String ente)
        {
            var query = from p in db.PERIODO
                          select p;
            if (dataInizio != null)
                query = query.Where(p => p.dataInizio == dataInizio.Date);
            if (dataFine != null)
                query = query.Where(p => p.dataFine == dataFine.Date);
            if (ente != null && !ente.Equals(""))
                query = query.Where(p => p.ente.Equals(ente));

            return query.ToList();
        }

        // Ritorna tutti i calendari appartenenti all'ente specificato
        public List<CALENDARIO> getCALENDARIOs_byDomainEntityID(string idEnte)
        {
            return (from c in db.CALENDARIO
                    where c.ente.Equals(idEnte)
                    select c).ToList();
        }
        
        // Controlla se le due liste di orari coincidono esattamente
        public bool isSameWeeklyHours(List<ORARIO_GIORNALIERO> list1, List<ORARIO_GIORNALIERO> list2)
        {
            bool matched = false;
            int matchedNum = 0;
            if (list1.Count == list2.Count)
            {
                foreach (ORARIO_GIORNALIERO orario1 in list1)
                {
                    foreach (ORARIO_GIORNALIERO orario2 in list2)
                    {
                        if (orario1.EqualsWithoutPK(orario2))
                        {
                            matchedNum++;
                            break;
                        }
                    }
                }
                if (list1.Count == matchedNum)
                {
                    matched = true;
                }
            }
            return matched;
        }


        // Se esiste, trova nel db un calendario appartenente allo stesso ente, e i cui orari settimanali corrispondono
        // esattamente a quelli passati in dict
        // Serve ad evitare di creare duplicazioni inutili di calendario nel database.
        public CALENDARIO getSameCalendar (Dictionary<String, ORARIO_GIORNALIERO> dict, string idEnte)
        {
            bool matched = false;
            CALENDARIO resultCAL = null;
            List<CALENDARIO> listCals = getCALENDARIOs_byDomainEntityID(idEnte);
            List<ORARIO_GIORNALIERO> listDict = dict.Values.ToList();
            foreach (CALENDARIO cal in listCals)
            {
                resultCAL = cal;
                List<ORARIO_GIORNALIERO> listOrarios = 
                    (from o in db.ORARIO_GIORNALIERO
                     where o.idCal == cal.idCal
                     select o).ToList();

                matched = isSameWeeklyHours(listOrarios, listDict);

                if (matched)
                {
                    break;
                }
            }
            return matched ? resultCAL : null;
        }

        // Data un id di categoria (strumenti musicali) vengono trovate le sottocategorie direttamente discendenti
        public List<TreeNode> getCATEGORIAChilds(int? fromId)
        {
            List<TreeNode> result = new List<TreeNode>();
            TreeNode tmpNode;
            var childCategories = from c in db.CATEGORIA
                                               select c;
            if (fromId == null)
            {
                childCategories = childCategories.Where(c => c.paternitaCategoria == null);
            }
            else
            {
                childCategories = childCategories.Where(c => c.paternitaCategoria == fromId);
            }
            foreach (CATEGORIA cc in childCategories.ToList())
            {
                tmpNode = new TreeNode(cc.nome, getCATEGORIAChilds(cc.idCat).ToArray());
                tmpNode.Tag = cc;
                result.Add(tmpNode);
            }
            
            return result;

        }

        // Funzione di ricerca che filtra via gli elementi "tree" secondo la chiave di ricerca "filter". E' possibile specificare
        // con onlyMusicalInstrument se si vogliono includere anche le categorie di intermezzo oppure ricercare solo sugli strumenti reali.
        public static List<TreeNode> treeNodeFilter(IEnumerable<TreeNode> tree, String filter, bool onlyMusicalInstruments)
        {
            bool matched = false;
            List<TreeNode> result = new List<TreeNode>();
            foreach (TreeNode tn in tree)
            {
                TreeNode tmpNode = new TreeNode(tn.Text);
                CATEGORIA tmpTag = (CATEGORIA)tn.Tag;
                tmpNode.Tag = tn;
                tmpNode.Checked = tn.Checked;
                if (tn.Nodes.Count != 0)
                {
                    List<TreeNode> tmpNodes = treeNodeFilter(tn.Nodes.Cast<TreeNode>(), filter, onlyMusicalInstruments);
                    if (tmpNodes.Count > 0)
                    {
                        tmpNode.Nodes.AddRange(tmpNodes.ToArray());
                        result.Add(tmpNode);
                    }
                }
                else
                {
                    matched = (tn.Text.ToLower().Contains(filter.ToLower()) && (onlyMusicalInstruments ? tmpTag.strumentoMusicale == onlyMusicalInstruments : true));
                    if (matched)
                    {
                        result.Add(tmpNode);
                    }
                }
            }
            return result;
        }

        // Funzione di ricerca per gli strumenti utilizzati. Risulta utile qualora la lista di strumenti suonati da un musicista
        // sia molto lunga e si voglia cercare agevolmente uno di questi strumenti.
        // Prende in input la lista completa degli strumenti suonati e la parola chiave da cercare
        public static List<StrumentiUtilizzatiItem> strumentiUtilizzatiItemFilter(List<StrumentiUtilizzatiItem> lista, String filter)
        {
            List<StrumentiUtilizzatiItem> result = new List<StrumentiUtilizzatiItem>();
            foreach (StrumentiUtilizzatiItem su in lista)
            {
                if (su.Title.Contains(filter))
                {
                    result.Add(su);
                }
            }
            return result;
        }

        // Ritorna la lista delle band escluse quelle da solista
        public List<BAND> getStandardBANDs()
        {
            return (from b in db.BAND
                    where b.solista == false
                    select b).ToList();
        }

        // Ritorna la lista delle band di cui il musicista (idMusicista) fa ancora _attivamente_ parte.
        // Le band in cui il musicista è uscito dalla formazione senza farne ritorno _non_ vengono incluse
        public List<BAND> getUsedBANDs(string idMusicista)
        {
            var query = from  b in db.BAND
                        join k in 
                            ((from b in db.BAND
                            join e in db.ENTRATA_USCITA on b.idBand equals e.idBand
                            where e.musicista.Equals(idMusicista)
                            group b by b.idBand into grouping
                            select new { Key = grouping.Key , Value = grouping.Count() }).Where(p => p.Value % 2 != 0))
                            on b.idBand equals k.Key
                            select b;
            return query.ToList();

        }

        // Ritorna la lista dei musicisti che hanno un determinato numero telefonico (il controllo viene fatto su tutti e tre i numeri
        public List<MUSICISTA> getMUSICISTAs_byTelefono(string numerotelefonico)
        {
            return db.MUSICISTA.Where(p => p.telefono1.Contains(numerotelefonico) ||
                p.telefono2.Contains(numerotelefonico) ||
                p.telefono3.Contains(numerotelefonico)).ToList();
        }

        // Ritorna la lista di numeri telefoncici di un artista
        public static List<string> getTelefonos_byMUSICISTA(MUSICISTA mus)
        {
            List<string> result = new List<string>();

            if (mus.telefono1 != null && !mus.telefono1.Equals(""))
            {
                result.Add(mus.telefono1);
            }
            if (mus.telefono2 != null && !mus.telefono2.Equals(""))
            {
                result.Add(mus.telefono2);
            }
            if (mus.telefono3 != null && !mus.telefono3.Equals(""))
            {
                result.Add(mus.telefono3);
            }
            return result;
        }

        // Ritorna il musicista corrispondente all'id specificato
        public MUSICISTA getMUSICISTA_byID(string email)
        {
            return db.MUSICISTA.Where(p => p.email.ToLower().Equals(email.ToLower())).First();
        }

        // Restituisce le BAND con attributo solista = true partendo da un musicista gia presente nel db
        public List<BAND> getBANDSolista_byMUSICISTA(MUSICISTA mus)
        {
            return mus.ENTRATA_USCITA
                .Select(p => p.BAND)
                .Distinct()
                .Where(p => p.solista == true).ToList();
        }
        // Ritorna true se almeno una band associata al musicista è una band da solista. False altrimenti.
        public bool isSolista(MUSICISTA mus)
        {
            return (getBANDSolista_byMUSICISTA(mus)
                .Count() > 0);
        }

        public List<ENTRATA_USCITA> getENTRATA_USCITAs_byBANDs(List<BAND> listaBand)
        {
            List<ENTRATA_USCITA> result = new List<ENTRATA_USCITA>();
            listaBand.ForEach(p => result.AddRange(p.ENTRATA_USCITA));
            return result;
        }
        public BAND getBAND_byID(int idBand)
        {
            return db.BAND.Where(p => p.idBand == idBand).SingleOrDefault();
        }

        public void test()
        {
            var query = (from eu in db.ENTRATA_USCITA
                        join bn in db.BAND on eu.idBand equals bn.idBand
                        join mu in db.MUSICISTA on eu.musicista equals mu.email
                        where bn.dataFormazione > new DateTime(2000, 1, 1)
                        select new {
                            nomeMus = mu.nome, 
                            cognomeMus = mu.cognome,
                            dataNascita = mu.dataNascita,
                            eu = eu.musicista, 
                            nomeBand = bn.nome, 
                            dataFormazione = bn.dataFormazione })
                        .Distinct();
            double average = (from st in db.STRUMENTO_SUONATO
                          join mu in db.MUSICISTA on st.musicista equals mu.email
                          where mu.dataNascita > new DateTime(2001, 1, 1)
                          group st by st.musicista into grouping
                          select new { cnt = grouping.Count() }).Average(p => p.cnt);

                         
        }
    }
}
