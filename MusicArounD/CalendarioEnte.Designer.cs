﻿namespace MusicArounD
{
    partial class CalendarioEnte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanelGenerale = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelCreaPeriodo = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePickerValiditaInizio = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePickerValiditaFine = new System.Windows.Forms.DateTimePicker();
            this.buttonCreaPeriodo = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelCalendario = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelCalendarioSettimanale = new System.Windows.Forms.TableLayoutPanel();
            this.panelDOM = new System.Windows.Forms.Panel();
            this.checkBoxDOM = new System.Windows.Forms.CheckBox();
            this.panelSAB = new System.Windows.Forms.Panel();
            this.checkBoxSAB = new System.Windows.Forms.CheckBox();
            this.panelVEN = new System.Windows.Forms.Panel();
            this.checkBoxVEN = new System.Windows.Forms.CheckBox();
            this.panelGIO = new System.Windows.Forms.Panel();
            this.checkBoxGIO = new System.Windows.Forms.CheckBox();
            this.listBoxLUN = new System.Windows.Forms.ListBox();
            this.listBoxMAR = new System.Windows.Forms.ListBox();
            this.listBoxMER = new System.Windows.Forms.ListBox();
            this.listBoxGIO = new System.Windows.Forms.ListBox();
            this.listBoxVEN = new System.Windows.Forms.ListBox();
            this.listBoxSAB = new System.Windows.Forms.ListBox();
            this.listBoxDOM = new System.Windows.Forms.ListBox();
            this.panelMAR = new System.Windows.Forms.Panel();
            this.checkBoxMAR = new System.Windows.Forms.CheckBox();
            this.panelLUN = new System.Windows.Forms.Panel();
            this.checkBoxLUN = new System.Windows.Forms.CheckBox();
            this.panelMER = new System.Windows.Forms.Panel();
            this.checkBoxMER = new System.Windows.Forms.CheckBox();
            this.buttonRimuoviOrarioLUN = new System.Windows.Forms.Button();
            this.buttonRimuoviOrarioMAR = new System.Windows.Forms.Button();
            this.buttonRimuoviOrarioMER = new System.Windows.Forms.Button();
            this.buttonRimuoviOrarioGIO = new System.Windows.Forms.Button();
            this.buttonRimuoviOrarioVEN = new System.Windows.Forms.Button();
            this.buttonRimuoviOrarioSAB = new System.Windows.Forms.Button();
            this.buttonRimuoviOrarioDOM = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.tableLayoutPanelPeriodoCalendario = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxPeriodoValidita = new System.Windows.Forms.ComboBox();
            this.buttonEliminaCalendario = new System.Windows.Forms.Button();
            this.groupBoxCreazioneOrario = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanelCreazioneOrario = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTimePickerOraInizio = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePickerOraFine = new System.Windows.Forms.DateTimePicker();
            this.buttonAggiungiOrario = new System.Windows.Forms.Button();
            this.tableLayoutPanelButton = new System.Windows.Forms.TableLayoutPanel();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.tableLayoutPanelGenerale.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanelCreaPeriodo.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanelCalendario.SuspendLayout();
            this.tableLayoutPanelCalendarioSettimanale.SuspendLayout();
            this.panelDOM.SuspendLayout();
            this.panelSAB.SuspendLayout();
            this.panelVEN.SuspendLayout();
            this.panelGIO.SuspendLayout();
            this.panelMAR.SuspendLayout();
            this.panelLUN.SuspendLayout();
            this.panelMER.SuspendLayout();
            this.tableLayoutPanelPeriodoCalendario.SuspendLayout();
            this.groupBoxCreazioneOrario.SuspendLayout();
            this.tableLayoutPanelCreazioneOrario.SuspendLayout();
            this.tableLayoutPanelButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelGenerale
            // 
            this.tableLayoutPanelGenerale.ColumnCount = 1;
            this.tableLayoutPanelGenerale.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGenerale.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanelGenerale.Controls.Add(this.groupBox2, 0, 2);
            this.tableLayoutPanelGenerale.Controls.Add(this.tableLayoutPanelButton, 0, 3);
            this.tableLayoutPanelGenerale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelGenerale.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelGenerale.Name = "tableLayoutPanelGenerale";
            this.tableLayoutPanelGenerale.RowCount = 4;
            this.tableLayoutPanelGenerale.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelGenerale.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanelGenerale.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGenerale.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanelGenerale.Size = new System.Drawing.Size(816, 359);
            this.tableLayoutPanelGenerale.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tableLayoutPanelCreaPeriodo);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(810, 53);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Creazione periodo di validità del calendario";
            // 
            // tableLayoutPanelCreaPeriodo
            // 
            this.tableLayoutPanelCreaPeriodo.ColumnCount = 6;
            this.tableLayoutPanelCreaPeriodo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelCreaPeriodo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelCreaPeriodo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelCreaPeriodo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelCreaPeriodo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelCreaPeriodo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelCreaPeriodo.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanelCreaPeriodo.Controls.Add(this.dateTimePickerValiditaInizio, 1, 0);
            this.tableLayoutPanelCreaPeriodo.Controls.Add(this.label2, 3, 0);
            this.tableLayoutPanelCreaPeriodo.Controls.Add(this.dateTimePickerValiditaFine, 4, 0);
            this.tableLayoutPanelCreaPeriodo.Controls.Add(this.buttonCreaPeriodo, 5, 0);
            this.tableLayoutPanelCreaPeriodo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelCreaPeriodo.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanelCreaPeriodo.Name = "tableLayoutPanelCreaPeriodo";
            this.tableLayoutPanelCreaPeriodo.RowCount = 1;
            this.tableLayoutPanelCreaPeriodo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelCreaPeriodo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanelCreaPeriodo.Size = new System.Drawing.Size(804, 34);
            this.tableLayoutPanelCreaPeriodo.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Data inizio validità";
            // 
            // dateTimePickerValiditaInizio
            // 
            this.dateTimePickerValiditaInizio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerValiditaInizio.Location = new System.Drawing.Point(101, 3);
            this.dateTimePickerValiditaInizio.Name = "dateTimePickerValiditaInizio";
            this.dateTimePickerValiditaInizio.Size = new System.Drawing.Size(250, 20);
            this.dateTimePickerValiditaInizio.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(377, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Data fine validità";
            // 
            // dateTimePickerValiditaFine
            // 
            this.dateTimePickerValiditaFine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerValiditaFine.Location = new System.Drawing.Point(469, 3);
            this.dateTimePickerValiditaFine.Name = "dateTimePickerValiditaFine";
            this.dateTimePickerValiditaFine.Size = new System.Drawing.Size(250, 20);
            this.dateTimePickerValiditaFine.TabIndex = 3;
            // 
            // buttonCreaPeriodo
            // 
            this.buttonCreaPeriodo.Location = new System.Drawing.Point(725, 3);
            this.buttonCreaPeriodo.Name = "buttonCreaPeriodo";
            this.buttonCreaPeriodo.Size = new System.Drawing.Size(75, 23);
            this.buttonCreaPeriodo.TabIndex = 5;
            this.buttonCreaPeriodo.Text = "Crea periodo";
            this.buttonCreaPeriodo.UseVisualStyleBackColor = true;
            this.buttonCreaPeriodo.Click += new System.EventHandler(this.buttonCreaPeriodo_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanelCalendario);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 72);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(810, 244);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Calendario";
            // 
            // tableLayoutPanelCalendario
            // 
            this.tableLayoutPanelCalendario.ColumnCount = 1;
            this.tableLayoutPanelCalendario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelCalendario.Controls.Add(this.tableLayoutPanelCalendarioSettimanale, 0, 2);
            this.tableLayoutPanelCalendario.Controls.Add(this.tableLayoutPanelPeriodoCalendario, 0, 0);
            this.tableLayoutPanelCalendario.Controls.Add(this.groupBoxCreazioneOrario, 0, 4);
            this.tableLayoutPanelCalendario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelCalendario.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanelCalendario.Name = "tableLayoutPanelCalendario";
            this.tableLayoutPanelCalendario.RowCount = 5;
            this.tableLayoutPanelCalendario.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelCalendario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanelCalendario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelCalendario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanelCalendario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanelCalendario.Size = new System.Drawing.Size(804, 225);
            this.tableLayoutPanelCalendario.TabIndex = 0;
            // 
            // tableLayoutPanelCalendarioSettimanale
            // 
            this.tableLayoutPanelCalendarioSettimanale.ColumnCount = 9;
            this.tableLayoutPanelCalendarioSettimanale.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelCalendarioSettimanale.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanelCalendarioSettimanale.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanelCalendarioSettimanale.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanelCalendarioSettimanale.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanelCalendarioSettimanale.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanelCalendarioSettimanale.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanelCalendarioSettimanale.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanelCalendarioSettimanale.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.panelDOM, 7, 0);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.panelSAB, 6, 0);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.panelVEN, 5, 0);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.panelGIO, 4, 0);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.listBoxLUN, 1, 1);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.listBoxMAR, 2, 1);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.listBoxMER, 3, 1);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.listBoxGIO, 4, 1);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.listBoxVEN, 5, 1);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.listBoxSAB, 6, 1);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.listBoxDOM, 7, 1);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.panelMAR, 2, 0);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.panelLUN, 1, 0);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.panelMER, 3, 0);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.buttonRimuoviOrarioLUN, 1, 2);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.buttonRimuoviOrarioMAR, 2, 2);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.buttonRimuoviOrarioMER, 3, 2);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.buttonRimuoviOrarioGIO, 4, 2);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.buttonRimuoviOrarioVEN, 5, 2);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.buttonRimuoviOrarioSAB, 6, 2);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.buttonRimuoviOrarioDOM, 7, 2);
            this.tableLayoutPanelCalendarioSettimanale.Controls.Add(this.listBox1, 0, 1);
            this.tableLayoutPanelCalendarioSettimanale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelCalendarioSettimanale.Enabled = false;
            this.tableLayoutPanelCalendarioSettimanale.Location = new System.Drawing.Point(3, 47);
            this.tableLayoutPanelCalendarioSettimanale.Name = "tableLayoutPanelCalendarioSettimanale";
            this.tableLayoutPanelCalendarioSettimanale.RowCount = 3;
            this.tableLayoutPanelCalendarioSettimanale.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelCalendarioSettimanale.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelCalendarioSettimanale.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelCalendarioSettimanale.Size = new System.Drawing.Size(798, 115);
            this.tableLayoutPanelCalendarioSettimanale.TabIndex = 5;
            // 
            // panelDOM
            // 
            this.panelDOM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDOM.Controls.Add(this.checkBoxDOM);
            this.panelDOM.Location = new System.Drawing.Point(645, 3);
            this.panelDOM.Name = "panelDOM";
            this.panelDOM.Size = new System.Drawing.Size(91, 28);
            this.panelDOM.TabIndex = 23;
            // 
            // checkBoxDOM
            // 
            this.checkBoxDOM.AutoSize = true;
            this.checkBoxDOM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxDOM.Location = new System.Drawing.Point(0, 0);
            this.checkBoxDOM.Name = "checkBoxDOM";
            this.checkBoxDOM.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.checkBoxDOM.Size = new System.Drawing.Size(89, 26);
            this.checkBoxDOM.TabIndex = 0;
            this.checkBoxDOM.Text = "DOM";
            this.checkBoxDOM.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxDOM.UseVisualStyleBackColor = true;
            this.checkBoxDOM.CheckedChanged += new System.EventHandler(this.checkBoxDOM_CheckedChanged);
            // 
            // panelSAB
            // 
            this.panelSAB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSAB.Controls.Add(this.checkBoxSAB);
            this.panelSAB.Location = new System.Drawing.Point(548, 3);
            this.panelSAB.Name = "panelSAB";
            this.panelSAB.Size = new System.Drawing.Size(91, 28);
            this.panelSAB.TabIndex = 23;
            // 
            // checkBoxSAB
            // 
            this.checkBoxSAB.AutoSize = true;
            this.checkBoxSAB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxSAB.Location = new System.Drawing.Point(0, 0);
            this.checkBoxSAB.Name = "checkBoxSAB";
            this.checkBoxSAB.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.checkBoxSAB.Size = new System.Drawing.Size(89, 26);
            this.checkBoxSAB.TabIndex = 0;
            this.checkBoxSAB.Text = "SAB";
            this.checkBoxSAB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxSAB.UseVisualStyleBackColor = true;
            this.checkBoxSAB.CheckedChanged += new System.EventHandler(this.checkBoxSAB_CheckedChanged);
            // 
            // panelVEN
            // 
            this.panelVEN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelVEN.Controls.Add(this.checkBoxVEN);
            this.panelVEN.Location = new System.Drawing.Point(451, 3);
            this.panelVEN.Name = "panelVEN";
            this.panelVEN.Size = new System.Drawing.Size(91, 28);
            this.panelVEN.TabIndex = 23;
            // 
            // checkBoxVEN
            // 
            this.checkBoxVEN.AutoSize = true;
            this.checkBoxVEN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxVEN.Location = new System.Drawing.Point(0, 0);
            this.checkBoxVEN.Name = "checkBoxVEN";
            this.checkBoxVEN.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.checkBoxVEN.Size = new System.Drawing.Size(89, 26);
            this.checkBoxVEN.TabIndex = 0;
            this.checkBoxVEN.Text = "VEN";
            this.checkBoxVEN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxVEN.UseVisualStyleBackColor = true;
            this.checkBoxVEN.CheckedChanged += new System.EventHandler(this.checkBoxVEN_CheckedChanged);
            // 
            // panelGIO
            // 
            this.panelGIO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelGIO.Controls.Add(this.checkBoxGIO);
            this.panelGIO.Location = new System.Drawing.Point(354, 3);
            this.panelGIO.Name = "panelGIO";
            this.panelGIO.Size = new System.Drawing.Size(91, 28);
            this.panelGIO.TabIndex = 23;
            // 
            // checkBoxGIO
            // 
            this.checkBoxGIO.AutoSize = true;
            this.checkBoxGIO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxGIO.Location = new System.Drawing.Point(0, 0);
            this.checkBoxGIO.Name = "checkBoxGIO";
            this.checkBoxGIO.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.checkBoxGIO.Size = new System.Drawing.Size(89, 26);
            this.checkBoxGIO.TabIndex = 0;
            this.checkBoxGIO.Text = "GIO";
            this.checkBoxGIO.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxGIO.UseVisualStyleBackColor = true;
            this.checkBoxGIO.CheckedChanged += new System.EventHandler(this.checkBoxGIO_CheckedChanged);
            // 
            // listBoxLUN
            // 
            this.listBoxLUN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxLUN.FormattingEnabled = true;
            this.listBoxLUN.Location = new System.Drawing.Point(63, 37);
            this.listBoxLUN.Name = "listBoxLUN";
            this.listBoxLUN.Size = new System.Drawing.Size(91, 44);
            this.listBoxLUN.Sorted = true;
            this.listBoxLUN.TabIndex = 7;
            // 
            // listBoxMAR
            // 
            this.listBoxMAR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxMAR.FormattingEnabled = true;
            this.listBoxMAR.Location = new System.Drawing.Point(160, 37);
            this.listBoxMAR.Name = "listBoxMAR";
            this.listBoxMAR.Size = new System.Drawing.Size(91, 44);
            this.listBoxMAR.Sorted = true;
            this.listBoxMAR.TabIndex = 8;
            // 
            // listBoxMER
            // 
            this.listBoxMER.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxMER.FormattingEnabled = true;
            this.listBoxMER.Location = new System.Drawing.Point(257, 37);
            this.listBoxMER.Name = "listBoxMER";
            this.listBoxMER.Size = new System.Drawing.Size(91, 44);
            this.listBoxMER.Sorted = true;
            this.listBoxMER.TabIndex = 9;
            // 
            // listBoxGIO
            // 
            this.listBoxGIO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxGIO.FormattingEnabled = true;
            this.listBoxGIO.Location = new System.Drawing.Point(354, 37);
            this.listBoxGIO.Name = "listBoxGIO";
            this.listBoxGIO.Size = new System.Drawing.Size(91, 44);
            this.listBoxGIO.Sorted = true;
            this.listBoxGIO.TabIndex = 10;
            // 
            // listBoxVEN
            // 
            this.listBoxVEN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxVEN.FormattingEnabled = true;
            this.listBoxVEN.Location = new System.Drawing.Point(451, 37);
            this.listBoxVEN.Name = "listBoxVEN";
            this.listBoxVEN.Size = new System.Drawing.Size(91, 44);
            this.listBoxVEN.Sorted = true;
            this.listBoxVEN.TabIndex = 11;
            // 
            // listBoxSAB
            // 
            this.listBoxSAB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxSAB.FormattingEnabled = true;
            this.listBoxSAB.Location = new System.Drawing.Point(548, 37);
            this.listBoxSAB.Name = "listBoxSAB";
            this.listBoxSAB.Size = new System.Drawing.Size(91, 44);
            this.listBoxSAB.Sorted = true;
            this.listBoxSAB.TabIndex = 12;
            // 
            // listBoxDOM
            // 
            this.listBoxDOM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxDOM.FormattingEnabled = true;
            this.listBoxDOM.Location = new System.Drawing.Point(645, 37);
            this.listBoxDOM.Name = "listBoxDOM";
            this.listBoxDOM.Size = new System.Drawing.Size(91, 44);
            this.listBoxDOM.Sorted = true;
            this.listBoxDOM.TabIndex = 13;
            // 
            // panelMAR
            // 
            this.panelMAR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMAR.Controls.Add(this.checkBoxMAR);
            this.panelMAR.Location = new System.Drawing.Point(160, 3);
            this.panelMAR.Name = "panelMAR";
            this.panelMAR.Size = new System.Drawing.Size(91, 28);
            this.panelMAR.TabIndex = 22;
            // 
            // checkBoxMAR
            // 
            this.checkBoxMAR.AutoSize = true;
            this.checkBoxMAR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxMAR.Location = new System.Drawing.Point(0, 0);
            this.checkBoxMAR.Name = "checkBoxMAR";
            this.checkBoxMAR.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.checkBoxMAR.Size = new System.Drawing.Size(89, 26);
            this.checkBoxMAR.TabIndex = 0;
            this.checkBoxMAR.Text = "MAR";
            this.checkBoxMAR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxMAR.UseVisualStyleBackColor = true;
            this.checkBoxMAR.CheckedChanged += new System.EventHandler(this.checkBoxMAR_CheckedChanged);
            // 
            // panelLUN
            // 
            this.panelLUN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelLUN.Controls.Add(this.checkBoxLUN);
            this.panelLUN.Location = new System.Drawing.Point(63, 3);
            this.panelLUN.Name = "panelLUN";
            this.panelLUN.Size = new System.Drawing.Size(91, 28);
            this.panelLUN.TabIndex = 23;
            // 
            // checkBoxLUN
            // 
            this.checkBoxLUN.AutoSize = true;
            this.checkBoxLUN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxLUN.Location = new System.Drawing.Point(0, 0);
            this.checkBoxLUN.Name = "checkBoxLUN";
            this.checkBoxLUN.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.checkBoxLUN.Size = new System.Drawing.Size(89, 26);
            this.checkBoxLUN.TabIndex = 0;
            this.checkBoxLUN.Text = "LUN";
            this.checkBoxLUN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxLUN.UseVisualStyleBackColor = true;
            this.checkBoxLUN.CheckedChanged += new System.EventHandler(this.checkBoxLUN_CheckedChanged);
            // 
            // panelMER
            // 
            this.panelMER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMER.Controls.Add(this.checkBoxMER);
            this.panelMER.Location = new System.Drawing.Point(257, 3);
            this.panelMER.Name = "panelMER";
            this.panelMER.Size = new System.Drawing.Size(91, 28);
            this.panelMER.TabIndex = 24;
            // 
            // checkBoxMER
            // 
            this.checkBoxMER.AutoSize = true;
            this.checkBoxMER.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxMER.Location = new System.Drawing.Point(0, 0);
            this.checkBoxMER.Name = "checkBoxMER";
            this.checkBoxMER.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.checkBoxMER.Size = new System.Drawing.Size(89, 26);
            this.checkBoxMER.TabIndex = 0;
            this.checkBoxMER.Text = "MER";
            this.checkBoxMER.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxMER.UseVisualStyleBackColor = true;
            this.checkBoxMER.CheckedChanged += new System.EventHandler(this.checkBoxMER_CheckedChanged);
            // 
            // buttonRimuoviOrarioLUN
            // 
            this.buttonRimuoviOrarioLUN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRimuoviOrarioLUN.Location = new System.Drawing.Point(63, 87);
            this.buttonRimuoviOrarioLUN.Name = "buttonRimuoviOrarioLUN";
            this.buttonRimuoviOrarioLUN.Size = new System.Drawing.Size(91, 23);
            this.buttonRimuoviOrarioLUN.TabIndex = 25;
            this.buttonRimuoviOrarioLUN.Text = "Rimuovi orario";
            this.buttonRimuoviOrarioLUN.UseVisualStyleBackColor = true;
            this.buttonRimuoviOrarioLUN.Click += new System.EventHandler(this.buttonRimuoviOrarioLUN_Click);
            // 
            // buttonRimuoviOrarioMAR
            // 
            this.buttonRimuoviOrarioMAR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRimuoviOrarioMAR.Location = new System.Drawing.Point(160, 87);
            this.buttonRimuoviOrarioMAR.Name = "buttonRimuoviOrarioMAR";
            this.buttonRimuoviOrarioMAR.Size = new System.Drawing.Size(91, 23);
            this.buttonRimuoviOrarioMAR.TabIndex = 26;
            this.buttonRimuoviOrarioMAR.Text = "Rimuovi orario";
            this.buttonRimuoviOrarioMAR.UseVisualStyleBackColor = true;
            this.buttonRimuoviOrarioMAR.Click += new System.EventHandler(this.buttonRimuoviOrarioMAR_Click);
            // 
            // buttonRimuoviOrarioMER
            // 
            this.buttonRimuoviOrarioMER.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRimuoviOrarioMER.Location = new System.Drawing.Point(257, 87);
            this.buttonRimuoviOrarioMER.Name = "buttonRimuoviOrarioMER";
            this.buttonRimuoviOrarioMER.Size = new System.Drawing.Size(91, 23);
            this.buttonRimuoviOrarioMER.TabIndex = 27;
            this.buttonRimuoviOrarioMER.Text = "Rimuovi orario";
            this.buttonRimuoviOrarioMER.UseVisualStyleBackColor = true;
            this.buttonRimuoviOrarioMER.Click += new System.EventHandler(this.buttonRimuoviOrarioMER_Click);
            // 
            // buttonRimuoviOrarioGIO
            // 
            this.buttonRimuoviOrarioGIO.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRimuoviOrarioGIO.Location = new System.Drawing.Point(354, 87);
            this.buttonRimuoviOrarioGIO.Name = "buttonRimuoviOrarioGIO";
            this.buttonRimuoviOrarioGIO.Size = new System.Drawing.Size(91, 23);
            this.buttonRimuoviOrarioGIO.TabIndex = 28;
            this.buttonRimuoviOrarioGIO.Text = "Rimuovi orario";
            this.buttonRimuoviOrarioGIO.UseVisualStyleBackColor = true;
            this.buttonRimuoviOrarioGIO.Click += new System.EventHandler(this.buttonRimuoviOrarioGIO_Click);
            // 
            // buttonRimuoviOrarioVEN
            // 
            this.buttonRimuoviOrarioVEN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRimuoviOrarioVEN.Location = new System.Drawing.Point(451, 87);
            this.buttonRimuoviOrarioVEN.Name = "buttonRimuoviOrarioVEN";
            this.buttonRimuoviOrarioVEN.Size = new System.Drawing.Size(91, 23);
            this.buttonRimuoviOrarioVEN.TabIndex = 29;
            this.buttonRimuoviOrarioVEN.Text = "Rimuovi orario";
            this.buttonRimuoviOrarioVEN.UseVisualStyleBackColor = true;
            this.buttonRimuoviOrarioVEN.Click += new System.EventHandler(this.buttonRimuoviOrarioVEN_Click);
            // 
            // buttonRimuoviOrarioSAB
            // 
            this.buttonRimuoviOrarioSAB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRimuoviOrarioSAB.Location = new System.Drawing.Point(548, 87);
            this.buttonRimuoviOrarioSAB.Name = "buttonRimuoviOrarioSAB";
            this.buttonRimuoviOrarioSAB.Size = new System.Drawing.Size(91, 23);
            this.buttonRimuoviOrarioSAB.TabIndex = 30;
            this.buttonRimuoviOrarioSAB.Text = "Rimuovi orario";
            this.buttonRimuoviOrarioSAB.UseVisualStyleBackColor = true;
            this.buttonRimuoviOrarioSAB.Click += new System.EventHandler(this.buttonRimuoviOrarioSAB_Click);
            // 
            // buttonRimuoviOrarioDOM
            // 
            this.buttonRimuoviOrarioDOM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRimuoviOrarioDOM.Location = new System.Drawing.Point(645, 87);
            this.buttonRimuoviOrarioDOM.Name = "buttonRimuoviOrarioDOM";
            this.buttonRimuoviOrarioDOM.Size = new System.Drawing.Size(91, 23);
            this.buttonRimuoviOrarioDOM.TabIndex = 31;
            this.buttonRimuoviOrarioDOM.Text = "Rimuovi orario";
            this.buttonRimuoviOrarioDOM.UseVisualStyleBackColor = true;
            this.buttonRimuoviOrarioDOM.Click += new System.EventHandler(this.buttonRimuoviOrarioDOM_Click);
            // 
            // listBox1
            // 
            this.listBox1.BackColor = System.Drawing.SystemColors.Control;
            this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox1.Enabled = false;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "Fascia 1",
            "Fascia 2",
            "Fascia 3"});
            this.listBox1.Location = new System.Drawing.Point(3, 37);
            this.listBox1.Name = "listBox1";
            this.listBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.listBox1.Size = new System.Drawing.Size(54, 44);
            this.listBox1.TabIndex = 32;
            // 
            // tableLayoutPanelPeriodoCalendario
            // 
            this.tableLayoutPanelPeriodoCalendario.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelPeriodoCalendario.ColumnCount = 3;
            this.tableLayoutPanelPeriodoCalendario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelPeriodoCalendario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelPeriodoCalendario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelPeriodoCalendario.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanelPeriodoCalendario.Controls.Add(this.comboBoxPeriodoValidita, 1, 0);
            this.tableLayoutPanelPeriodoCalendario.Controls.Add(this.buttonEliminaCalendario, 2, 0);
            this.tableLayoutPanelPeriodoCalendario.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelPeriodoCalendario.Name = "tableLayoutPanelPeriodoCalendario";
            this.tableLayoutPanelPeriodoCalendario.RowCount = 1;
            this.tableLayoutPanelPeriodoCalendario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelPeriodoCalendario.Size = new System.Drawing.Size(798, 28);
            this.tableLayoutPanelPeriodoCalendario.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 28);
            this.label3.TabIndex = 0;
            this.label3.Text = "Periodo del calendario";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxPeriodoValidita
            // 
            this.comboBoxPeriodoValidita.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxPeriodoValidita.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPeriodoValidita.FormattingEnabled = true;
            this.comboBoxPeriodoValidita.Location = new System.Drawing.Point(121, 3);
            this.comboBoxPeriodoValidita.Name = "comboBoxPeriodoValidita";
            this.comboBoxPeriodoValidita.Size = new System.Drawing.Size(553, 21);
            this.comboBoxPeriodoValidita.Sorted = true;
            this.comboBoxPeriodoValidita.TabIndex = 1;
            this.comboBoxPeriodoValidita.SelectedIndexChanged += new System.EventHandler(this.comboBoxPeriodoValidita_SelectedIndexChanged);
            // 
            // buttonEliminaCalendario
            // 
            this.buttonEliminaCalendario.Location = new System.Drawing.Point(680, 3);
            this.buttonEliminaCalendario.Name = "buttonEliminaCalendario";
            this.buttonEliminaCalendario.Size = new System.Drawing.Size(115, 22);
            this.buttonEliminaCalendario.TabIndex = 2;
            this.buttonEliminaCalendario.Text = "Elimina calendario";
            this.buttonEliminaCalendario.UseVisualStyleBackColor = true;
            // 
            // groupBoxCreazioneOrario
            // 
            this.groupBoxCreazioneOrario.Controls.Add(this.tableLayoutPanelCreazioneOrario);
            this.groupBoxCreazioneOrario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxCreazioneOrario.Enabled = false;
            this.groupBoxCreazioneOrario.Location = new System.Drawing.Point(3, 173);
            this.groupBoxCreazioneOrario.Name = "groupBoxCreazioneOrario";
            this.groupBoxCreazioneOrario.Size = new System.Drawing.Size(798, 49);
            this.groupBoxCreazioneOrario.TabIndex = 7;
            this.groupBoxCreazioneOrario.TabStop = false;
            this.groupBoxCreazioneOrario.Text = "Creazione orario nei giorni selezionati";
            // 
            // tableLayoutPanelCreazioneOrario
            // 
            this.tableLayoutPanelCreazioneOrario.ColumnCount = 6;
            this.tableLayoutPanelCreazioneOrario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelCreazioneOrario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelCreazioneOrario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelCreazioneOrario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelCreazioneOrario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelCreazioneOrario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelCreazioneOrario.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanelCreazioneOrario.Controls.Add(this.dateTimePickerOraInizio, 1, 0);
            this.tableLayoutPanelCreazioneOrario.Controls.Add(this.label5, 3, 0);
            this.tableLayoutPanelCreazioneOrario.Controls.Add(this.dateTimePickerOraFine, 4, 0);
            this.tableLayoutPanelCreazioneOrario.Controls.Add(this.buttonAggiungiOrario, 5, 0);
            this.tableLayoutPanelCreazioneOrario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelCreazioneOrario.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanelCreazioneOrario.Name = "tableLayoutPanelCreazioneOrario";
            this.tableLayoutPanelCreazioneOrario.RowCount = 1;
            this.tableLayoutPanelCreazioneOrario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelCreazioneOrario.Size = new System.Drawing.Size(792, 30);
            this.tableLayoutPanelCreazioneOrario.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Orario inizio";
            // 
            // dateTimePickerOraInizio
            // 
            this.dateTimePickerOraInizio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerOraInizio.CustomFormat = "HH:mm";
            this.dateTimePickerOraInizio.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerOraInizio.Location = new System.Drawing.Point(70, 3);
            this.dateTimePickerOraInizio.Name = "dateTimePickerOraInizio";
            this.dateTimePickerOraInizio.ShowUpDown = true;
            this.dateTimePickerOraInizio.Size = new System.Drawing.Size(275, 20);
            this.dateTimePickerOraInizio.TabIndex = 2;
            this.dateTimePickerOraInizio.ValueChanged += new System.EventHandler(this.dateTimePickerOraInizio_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(371, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Orario fine";
            // 
            // dateTimePickerOraFine
            // 
            this.dateTimePickerOraFine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerOraFine.CustomFormat = "HH:mm";
            this.dateTimePickerOraFine.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerOraFine.Location = new System.Drawing.Point(432, 3);
            this.dateTimePickerOraFine.Name = "dateTimePickerOraFine";
            this.dateTimePickerOraFine.ShowUpDown = true;
            this.dateTimePickerOraFine.Size = new System.Drawing.Size(275, 20);
            this.dateTimePickerOraFine.TabIndex = 3;
            // 
            // buttonAggiungiOrario
            // 
            this.buttonAggiungiOrario.Enabled = false;
            this.buttonAggiungiOrario.Location = new System.Drawing.Point(713, 3);
            this.buttonAggiungiOrario.Name = "buttonAggiungiOrario";
            this.buttonAggiungiOrario.Size = new System.Drawing.Size(75, 23);
            this.buttonAggiungiOrario.TabIndex = 4;
            this.buttonAggiungiOrario.Text = "Aggiungi orario";
            this.buttonAggiungiOrario.UseVisualStyleBackColor = true;
            this.buttonAggiungiOrario.Click += new System.EventHandler(this.buttonAggiungiOrario_Click);
            // 
            // tableLayoutPanelButton
            // 
            this.tableLayoutPanelButton.ColumnCount = 2;
            this.tableLayoutPanelButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelButton.Controls.Add(this.buttonCancel, 1, 0);
            this.tableLayoutPanelButton.Controls.Add(this.buttonSave, 0, 0);
            this.tableLayoutPanelButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelButton.Location = new System.Drawing.Point(3, 322);
            this.tableLayoutPanelButton.Name = "tableLayoutPanelButton";
            this.tableLayoutPanelButton.RowCount = 1;
            this.tableLayoutPanelButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelButton.Size = new System.Drawing.Size(810, 34);
            this.tableLayoutPanelButton.TabIndex = 4;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(732, 8);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 0;
            this.buttonCancel.Text = "Cancella";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonSave.Location = new System.Drawing.Point(651, 8);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 1;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // CalendarioEnte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 359);
            this.Controls.Add(this.tableLayoutPanelGenerale);
            this.Name = "CalendarioEnte";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calendario dell\'ente";
            this.Load += new System.EventHandler(this.CalendarioEnte_Load);
            this.tableLayoutPanelGenerale.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanelCreaPeriodo.ResumeLayout(false);
            this.tableLayoutPanelCreaPeriodo.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanelCalendario.ResumeLayout(false);
            this.tableLayoutPanelCalendarioSettimanale.ResumeLayout(false);
            this.panelDOM.ResumeLayout(false);
            this.panelDOM.PerformLayout();
            this.panelSAB.ResumeLayout(false);
            this.panelSAB.PerformLayout();
            this.panelVEN.ResumeLayout(false);
            this.panelVEN.PerformLayout();
            this.panelGIO.ResumeLayout(false);
            this.panelGIO.PerformLayout();
            this.panelMAR.ResumeLayout(false);
            this.panelMAR.PerformLayout();
            this.panelLUN.ResumeLayout(false);
            this.panelLUN.PerformLayout();
            this.panelMER.ResumeLayout(false);
            this.panelMER.PerformLayout();
            this.tableLayoutPanelPeriodoCalendario.ResumeLayout(false);
            this.tableLayoutPanelPeriodoCalendario.PerformLayout();
            this.groupBoxCreazioneOrario.ResumeLayout(false);
            this.tableLayoutPanelCreazioneOrario.ResumeLayout(false);
            this.tableLayoutPanelCreazioneOrario.PerformLayout();
            this.tableLayoutPanelButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelGenerale;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCreaPeriodo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePickerValiditaInizio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePickerValiditaFine;
        private System.Windows.Forms.Button buttonCreaPeriodo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCalendario;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCalendarioSettimanale;
        private System.Windows.Forms.Panel panelDOM;
        private System.Windows.Forms.CheckBox checkBoxDOM;
        private System.Windows.Forms.Panel panelSAB;
        private System.Windows.Forms.CheckBox checkBoxSAB;
        private System.Windows.Forms.Panel panelVEN;
        private System.Windows.Forms.CheckBox checkBoxVEN;
        private System.Windows.Forms.Panel panelGIO;
        private System.Windows.Forms.CheckBox checkBoxGIO;
        private System.Windows.Forms.ListBox listBoxLUN;
        private System.Windows.Forms.ListBox listBoxMAR;
        private System.Windows.Forms.ListBox listBoxMER;
        private System.Windows.Forms.ListBox listBoxGIO;
        private System.Windows.Forms.ListBox listBoxVEN;
        private System.Windows.Forms.ListBox listBoxSAB;
        private System.Windows.Forms.ListBox listBoxDOM;
        private System.Windows.Forms.Panel panelMAR;
        private System.Windows.Forms.CheckBox checkBoxMAR;
        private System.Windows.Forms.Panel panelLUN;
        private System.Windows.Forms.CheckBox checkBoxLUN;
        private System.Windows.Forms.Panel panelMER;
        private System.Windows.Forms.CheckBox checkBoxMER;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelPeriodoCalendario;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxPeriodoValidita;
        private System.Windows.Forms.Button buttonEliminaCalendario;
        private System.Windows.Forms.GroupBox groupBoxCreazioneOrario;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCreazioneOrario;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dateTimePickerOraInizio;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePickerOraFine;
        private System.Windows.Forms.Button buttonAggiungiOrario;
        private System.Windows.Forms.Button buttonRimuoviOrarioLUN;
        private System.Windows.Forms.Button buttonRimuoviOrarioMAR;
        private System.Windows.Forms.Button buttonRimuoviOrarioMER;
        private System.Windows.Forms.Button buttonRimuoviOrarioGIO;
        private System.Windows.Forms.Button buttonRimuoviOrarioVEN;
        private System.Windows.Forms.Button buttonRimuoviOrarioSAB;
        private System.Windows.Forms.Button buttonRimuoviOrarioDOM;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelButton;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonSave;


    }
}