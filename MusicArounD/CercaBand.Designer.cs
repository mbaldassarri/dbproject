﻿namespace MusicArounD
{
    partial class CercaBand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.madDBMapperDataContextBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxRicercaBand = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewBand = new System.Windows.Forms.DataGridView();
            this.Check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataNascita = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comune = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Provincia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MusicistaReferente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idBand = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btOk = new System.Windows.Forms.Button();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet11 = new MusicArounD.DataSet1();
            ((System.ComponentModel.ISupportInitialize)(this.madDBMapperDataContextBindingSource)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet11)).BeginInit();
            this.SuspendLayout();
            // 
            // madDBMapperDataContextBindingSource
            // 
            this.madDBMapperDataContextBindingSource.DataSource = typeof(MusicArounD.MadDBMapperDataContext);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.textBoxRicercaBand, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dataGridViewBand, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btOk, 0, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 1.805869F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86.51685F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.79775F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(828, 552);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // textBoxRicercaBand
            // 
            this.textBoxRicercaBand.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxRicercaBand.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRicercaBand.ForeColor = System.Drawing.Color.Black;
            this.textBoxRicercaBand.Location = new System.Drawing.Point(2, 30);
            this.textBoxRicercaBand.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxRicercaBand.Name = "textBoxRicercaBand";
            this.textBoxRicercaBand.Size = new System.Drawing.Size(824, 24);
            this.textBoxRicercaBand.TabIndex = 1;
            this.textBoxRicercaBand.TextChanged += new System.EventHandler(this.searchEvent);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(272, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Inserire Band (nome, provincia, comune..)";
            // 
            // dataGridViewBand
            // 
            this.dataGridViewBand.AllowUserToAddRows = false;
            this.dataGridViewBand.AllowUserToDeleteRows = false;
            this.dataGridViewBand.AllowUserToOrderColumns = true;
            this.dataGridViewBand.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewBand.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBand.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Check,
            this.Nome,
            this.dataNascita,
            this.Comune,
            this.Provincia,
            this.MusicistaReferente,
            this.idBand});
            this.dataGridViewBand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewBand.Location = new System.Drawing.Point(2, 70);
            this.dataGridViewBand.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewBand.Name = "dataGridViewBand";
            this.dataGridViewBand.RowTemplate.Height = 24;
            this.dataGridViewBand.Size = new System.Drawing.Size(824, 421);
            this.dataGridViewBand.TabIndex = 2;
            this.dataGridViewBand.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewClickEvent);
            // 
            // Check
            // 
            this.Check.HeaderText = "";
            this.Check.Name = "Check";
            this.Check.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Check.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Check.Width = 40;
            // 
            // Nome
            // 
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            this.Nome.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Nome.Width = 200;
            // 
            // dataNascita
            // 
            this.dataNascita.HeaderText = "Data Formazione";
            this.dataNascita.Name = "dataNascita";
            this.dataNascita.ReadOnly = true;
            this.dataNascita.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataNascita.Width = 200;
            // 
            // Comune
            // 
            this.Comune.HeaderText = "Comune";
            this.Comune.Name = "Comune";
            this.Comune.ReadOnly = true;
            this.Comune.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Comune.Width = 200;
            // 
            // Provincia
            // 
            this.Provincia.HeaderText = "Provincia";
            this.Provincia.Name = "Provincia";
            this.Provincia.ReadOnly = true;
            this.Provincia.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Provincia.Width = 200;
            // 
            // MusicistaReferente
            // 
            this.MusicistaReferente.HeaderText = "MusicistaReferente";
            this.MusicistaReferente.Name = "MusicistaReferente";
            this.MusicistaReferente.Width = 200;
            // 
            // idBand
            // 
            this.idBand.HeaderText = "idBand";
            this.idBand.Name = "idBand";
            this.idBand.Visible = false;
            // 
            // btOk
            // 
            this.btOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btOk.Location = new System.Drawing.Point(744, 527);
            this.btOk.Margin = new System.Windows.Forms.Padding(2);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(82, 23);
            this.btOk.TabIndex = 3;
            this.btOk.Text = "Avanti";
            this.btOk.UseVisualStyleBackColor = true;
            this.btOk.Click += new System.EventHandler(this.btOKClick);
            // 
            // dataSet11
            // 
            this.dataSet11.DataSetName = "DataSet1";
            this.dataSet11.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // CercaBand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 552);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CercaBand";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = ".. Cerca una Band ..";
            this.Load += new System.EventHandler(this.CercaMusicista_Load);
            ((System.ComponentModel.ISupportInitialize)(this.madDBMapperDataContextBindingSource)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet11)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource madDBMapperDataContextBindingSource;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox textBoxRicercaBand;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private DataSet1 dataSet11;
        private System.Windows.Forms.Button btOk;
        private System.Windows.Forms.DataGridView dataGridViewBand;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Check;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataNascita;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comune;
        private System.Windows.Forms.DataGridViewTextBoxColumn Provincia;
        private System.Windows.Forms.DataGridViewTextBoxColumn MusicistaReferente;
        private System.Windows.Forms.DataGridViewTextBoxColumn idBand;
    }
}