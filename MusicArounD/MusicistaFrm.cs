﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusicArounD
{
    public partial class MusicistaFrm : DBEngineForm
    {
        private List<TreeNode> alberoCategorie;
        private List<StrumentiUtilizzatiItem> strumentiUtilizzati = new List<StrumentiUtilizzatiItem>();

        private List<BAND> bandUtilizzate;

        private callType internalCallType;
        private MUSICISTA internalMusicista;

        private const int LOOKFORWARD_BIRTHYEAR = -5;
        
        public enum callType
        {
            UPDATE,
            UPDATE_AND_NEW,
            INSERT,
            INSERT_AND_NEW
        }

        // Costruttore di insert.
        // Se isNewActionEnabled è true allora viene abilitato il bottone "Inserisci e crea nuovo..."
        // Se isNewActionEnabled è false allora viene disabilitato il bottone "Inserisci e crea nuovo..."
        public MusicistaFrm(bool isNewActionEnabled)
        {
            InitializeComponent();
            internalCallType = isNewActionEnabled ? callType.INSERT_AND_NEW : callType.INSERT;
            internalMusicista = new MUSICISTA();
            loadData(internalMusicista);
            fillUI(internalMusicista);
        }
        
        // Costruttore di insert.
        // Viene disabilitato il bottone "Inserisci e crea nuovo..."
        public MusicistaFrm() : this(false) { }
        
        // Costruttore di update.
        // Viene disabilitato il bottone "Aggiorna e continua a modificare..."
        public MusicistaFrm(string idMusicista) : this(idMusicista, false) { }
        
        // Costruttore di update.
        // Se isNewActionEnabled è true allora viene abilitato il bottone "Aggiorna e continua a modificare..."
        // Se isNewActionEnabled è false allora viene disabilitato il bottone "Aggiorna e continua a modificare..."
        public MusicistaFrm(string idMusicista, bool isNewActionEnabled) : this()
        {
            internalCallType = internalCallType = isNewActionEnabled ? callType.UPDATE_AND_NEW : callType.UPDATE;
            internalMusicista = madEngine.getMUSICISTA_byID(idMusicista);
            loadData(internalMusicista);
            fillUI(internalMusicista);
        }

        private void changeActionButtons()
        {
            switch (internalCallType)
            {
                case callType.INSERT:
                    buttonAction.Text = MusicArounD.Properties.Resources.BUTTON_INSERT;
                    buttonAction_and_new.Visible = false;
                    break;
                case callType.INSERT_AND_NEW:
                    buttonAction.Text = MusicArounD.Properties.Resources.BUTTON_INSERT;
                    buttonAction_and_new.Text = MusicArounD.Properties.Resources.BUTTON_INSERT_AND_NEW;
                    buttonAction_and_new.Visible = true;
                    break;
                case callType.UPDATE:
                    buttonAction.Text = MusicArounD.Properties.Resources.BUTTON_UPDATE;
                    buttonAction_and_new.Visible = false;
                    break;
                case callType.UPDATE_AND_NEW:
                    buttonAction.Text = MusicArounD.Properties.Resources.BUTTON_UPDATE;
                    buttonAction_and_new.Text = MusicArounD.Properties.Resources.BUTTON_UPDATE_AND_NEW;
                    buttonAction_and_new.Visible = true;
                    break;

            }
        }
        private void loadData (MUSICISTA mus)
        {
            // Popolo la cache delle band utilizzate
            bandUtilizzate = madEngine.getUsedBANDs(mus.email);

            // Popolo la cache degli strumenti disponibili
            alberoCategorie = madEngine.getCATEGORIAChilds(null);
        }
        private void fillUI(MUSICISTA mus)
        {
            // Popolo le band utilizzate dal musicista
            listBoxBandUtilizzate.Items.Clear();
            listBoxBandUtilizzate.Items.AddRange(bandUtilizzate.ToArray());

            // Popolo gli strumenti suonati dal musicista
            listBoxStrumentiUtilizzati1.Clear();
            listBoxStrumentiUtilizzati1.AddRange(mus.STRUMENTO_SUONATO.ToList());

            this.textBoxEmail.Text = mus.email;
            if (internalCallType == callType.UPDATE || internalCallType == callType.UPDATE_AND_NEW) { this.textBoxEmail.Enabled = false; }
            this.textBoxNome.Text = mus.nome;
            this.textBoxCognome.Text = mus.cognome;
            this.textBoxNomeArte.Text = mus.nomeArte;

            this.checkBoxDataNascita.Checked = (mus.dataNascita != null);
            this.dateTimePickerNascita.Value = mus.dataNascita == null ? DateTime.Now : mus.dataNascita.Value;
            this.textBoxSitoWeb.Text = mus.sitoWeb;
            this.textBoxTelefono1.Text = mus.telefono1;
            this.textBoxTelefono2.Text = mus.telefono2;
            this.textBoxTelefono3.Text = mus.telefono3;
            
            this.checkBoxBandSolista.Checked = madEngine.isSolista(mus);

            // Carica lista provincie (ed implicitamente in cascata i comuni)
            comboBoxProvincia.Items.Clear();
            comboBoxComune.Items.Clear();
            comboBoxProvincia.Items.AddRange(madEngine.getProvincie().ToArray());

            // Seleziona la provincia ed il comune
            if (mus.COMUNE != null)
            {
                comboBoxProvincia.SelectedItem = mus.COMUNE.PROVINCIA;
                comboBoxComune.SelectedItem = mus.COMUNE;
            }
            else
            {
                comboBoxProvincia.Text = "";
                comboBoxComune.Text = "";
            }

            // Pulizia dei filtri di ricerca
            this.textBoxFiltroStrumentiDisponibili.Text = "";
            this.textBoxFiltroStrumentiUtilizzati.Text = "";

            // Pulizia e caricamento dati dell'albero delle categorie
            treeViewStrumentiDisponibili.Nodes.Clear();
            treeViewStrumentiDisponibili.Nodes.AddRange(MDE.treeNodeFilter(alberoCategorie, "", true).ToArray());

        }

        private void doInsertData_solista(MUSICISTA musicista)
        {
            DateTime timeNow;
            // Creazione della partecipazione solista nel db.
            if (checkBoxBandSolista.Checked)
            {
                timeNow = DateTime.Now;
                musicista.ENTRATA_USCITA.Add(new ENTRATA_USCITA
                {
                    data = timeNow,
                    e_u = true,
                    BAND = new BAND
                    {
                        comuneBand = musicista.comuneMusicista,
                        provinciaBand = musicista.provinciaMusicista,
                        nome = null,
                        dataFormazione = timeNow,
                        solista = true
                    }
                });
            }
        }

        private void doUpdateData_solista(MUSICISTA musicista)
        {
            List<BAND> listaBand;
            List<ENTRATA_USCITA> listaEntrataUscita;
            if (madEngine.isSolista(musicista))
            {
                // Cancellazione del solista
                if (!checkBoxBandSolista.Checked)
                {
                    listaBand = madEngine.getBANDSolista_byMUSICISTA(musicista);
                    listaEntrataUscita = madEngine.getENTRATA_USCITAs_byBANDs(listaBand);
                    // Questa istruzione è equivalente a quella sottostante. Questa è implicita. Quella sottostante è esplicita
                    //listaEntrataUscita.ForEach(p => musicista.ENTRATA_USCITA.Remove(p));
                    madEngine.getDataContext().ENTRATA_USCITA.DeleteAllOnSubmit(listaEntrataUscita);
                    madEngine.getDataContext().BAND.DeleteAllOnSubmit(listaBand);
                }
            }
            else
            {
                // Creazione del solista
                if (checkBoxBandSolista.Checked)
                {
                    doInsertData_solista(musicista);
                }
            }
        }

        private void doInsertOrUpdateData(MUSICISTA musicista)
        {
            // Compilazione dei dati base
            musicista.nome = !textBoxNome.Text.Equals("") ? textBoxNome.Text : null;
            musicista.cognome = !textBoxCognome.Text.Equals("") ? textBoxCognome.Text : null;
            musicista.nomeArte = !textBoxNomeArte.Text.Equals("") ? textBoxNomeArte.Text : null;
            musicista.email = !textBoxEmail.Text.Equals("") ? textBoxEmail.Text : null;
            musicista.sitoWeb = !textBoxSitoWeb.Text.Equals("") ? textBoxSitoWeb.Text : null;
            musicista.dataNascita = checkBoxDataNascita.Checked ? dateTimePickerNascita.Value.Date : (DateTime?)null;
            musicista.telefono1 = !textBoxTelefono1.Text.Equals("") ? textBoxTelefono1.Text : null;
            musicista.telefono2 = !textBoxTelefono2.Text.Equals("") ? textBoxTelefono2.Text : null;
            musicista.telefono3 = !textBoxTelefono3.Text.Equals("") ? textBoxTelefono3.Text : null;
            musicista.COMUNE = (COMUNE)comboBoxComune.SelectedItem;

            // Aggiunta degli strumenti all'artista (se non sono gia assegnati)
            musicista.STRUMENTO_SUONATO.AddRange(listBoxStrumentiUtilizzati1.getStrumentiSuonati().Where(p => p.MUSICISTA1 == null));

            switch (internalCallType)
            {
                case callType.INSERT:
                case callType.INSERT_AND_NEW:
                    doInsertData_solista(musicista);
                    break;
                case callType.UPDATE:
                case callType.UPDATE_AND_NEW:
                    doUpdateData_solista(musicista);
                    break;
                default:
                    // Non fare nulla
                    // codice che non dovrebbe mai essere raggiunto
                    break;
            }
        }

        private void MusicistaFrm_Load(object sender, EventArgs e)
        {
            // Cambia il nome dei bottoni
            changeActionButtons();

            // Setto la data massima gestibile dal datetimepicker
            dateTimePickerNascita.MaxDate = DateTime.Now.AddYears(LOOKFORWARD_BIRTHYEAR);
        }

        // Updates all child tree nodes recursively.
        private void CheckAllChildNodes(TreeNode treeNode, bool nodeChecked)
        {
            foreach (TreeNode node in treeNode.Nodes)
            {
                node.Checked = nodeChecked;
                ((TreeNode)node.Tag).Checked = nodeChecked;
                if (node.Nodes.Count > 0)
                {
                    // If the current node has child nodes, call the CheckAllChildsNodes method recursively.
                    this.CheckAllChildNodes(node, nodeChecked);
                }
            }
        }

        private bool doSaveData()
        {
            bool result = false;

            try
            {
                madEngine.getDataContext().SubmitChanges(ConflictMode.FailOnFirstConflict);
                switch (internalCallType)
                {
                    case callType.INSERT:
                    case callType.INSERT_AND_NEW:
                        MessageBox.Show(MusicArounD.Properties.Resources.SUCCESS_QUERY_INSERT,
                            MusicArounD.Properties.Resources.MESSAGETITLE_MUSICISTA_NEW,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    case callType.UPDATE:
                    case callType.UPDATE_AND_NEW:
                        MessageBox.Show(MusicArounD.Properties.Resources.SUCCESS_QUERY_UPDATE,
                            MusicArounD.Properties.Resources.MESSAGETITLE_MUSICISTA_UPDATE,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    default:
                        break;
                }
                result = true;

            }
            catch (SqlException sqlex)
            {
                switch (internalCallType)
                {
                    case callType.INSERT:
                    case callType.INSERT_AND_NEW:
                        MessageBox.Show(MusicArounD.Properties.Resources.ERROR_QUERY_INSERT + "\n" + sqlex,
                            MusicArounD.Properties.Resources.MESSAGETITLE_ERRORNAME,
                            MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        break;
                    case callType.UPDATE:
                    case callType.UPDATE_AND_NEW:
                        MessageBox.Show(MusicArounD.Properties.Resources.ERROR_QUERY_UPDATE + "\n" + sqlex,
                            MusicArounD.Properties.Resources.MESSAGETITLE_ERRORNAME,
                            MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        break;
                    default:
                        break;
                }

                textBoxEmail.SelectAll();
            }
            return result;
        }
        private bool doSaveProcedure()
        {
            bool result = false;
            if (checkFields())
            {
                
                //switch (internalCallType)
                //{
                //    case callType.INSERT:
                //    case callType.INSERT_AND_NEW:
                //        musicista = doInsertOrUpdateData(new MUSICISTA());
                //        break;
                //    case callType.UPDATE:
                //    case callType.UPDATE_AND_NEW:
                //        musicista = doInsertOrUpdateData(internalMusicista);
                //        break;
                //    default:
                //        // codice che non dovrebbe mai essere raggiunto
                //        musicista = null;
                //        break;
                //}
                doInsertOrUpdateData(internalMusicista);
                result = doSaveData();
            }
            else
            {
                result = false;
            }
            return result;
        }

        //se esiste qualche duplicazione di chiave, ad esempio la chiave primaria email, viene lanciata un'eccezione, qui gestita
        private void buttonAction_Click(object sender, EventArgs e)
        {
            if (doSaveProcedure())
            {
                this.Close();
            }
        }

        private bool checkFields()
        {

            bool result = true;
            String message = "";

            if (!MDE.RegexUtils.IsValidEmail(textBoxEmail.Text))
            {
                message += "- " + MusicArounD.Properties.Resources.ERROR_NOTMAIL + "\n";
                result = false;
            }
            if (textBoxNome.Text.Equals(""))
            {
                message += "- " + MusicArounD.Properties.Resources.ERROR_NOENTENOME + "\n";

                result = false;
            }

            if (textBoxCognome.Text.Equals(""))
            {
                message += "- " + MusicArounD.Properties.Resources.ERROR_NOCOGNOME + "\n";

                result = false;
            }

            if (!MDE.RegexUtils.isValidTelephoneNumber(textBoxTelefono1.Text, true) ||
                !MDE.RegexUtils.isValidTelephoneNumber(textBoxTelefono2.Text, true) ||
                !MDE.RegexUtils.isValidTelephoneNumber(textBoxTelefono3.Text, true))
            {
                message += "- " + MusicArounD.Properties.Resources.ERROR_NOTTEL + "\n";
                result = false;
            }
            if (!MDE.RegexUtils.isValidURI(textBoxSitoWeb.Text, true))
            {
                message += "- " + MusicArounD.Properties.Resources.ERROR_NOTURI + "\n";
                result = false;
            }

            if (comboBoxProvincia.SelectedIndex == -1 || comboBoxComune.SelectedIndex == -1)
            {
                message += "- " + MusicArounD.Properties.Resources.ERROR_MUSICISTA_NOTADDRESS + "\n";
                result = false;
            }
            if (!result)
            {
                MessageBox.Show(message, MusicArounD.Properties.Resources.ERROR_NODATA, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return result;
        }

        private void buttonRemoveStrumento_Click(object sender, EventArgs e)
        {
            foreach (StrumentiUtilizzatiItem item in new System.Collections.ArrayList(listBoxStrumentiUtilizzati1.Items))
            {
                if(item.ToRemove ) {
                    madEngine.getDataContext().STRUMENTO_SUONATO.DeleteOnSubmit(item.strumentoSuonato);
                    listBoxStrumentiUtilizzati1.Remove(item);
                }
            }
        }

        private void textBoxFiltroStrumentiDisponibili_TextChanged(object sender, EventArgs e)
        {
            List<TreeNode> tmpList = MDE.treeNodeFilter(alberoCategorie, textBoxFiltroStrumentiDisponibili.Text, true);
            treeViewStrumentiDisponibili.Nodes.Clear();
            treeViewStrumentiDisponibili.Nodes.AddRange(tmpList.ToArray());
            treeViewStrumentiDisponibili.ExpandAll();
        }

        private void treeViewStrumentiDisponibili_AfterCheck(object sender, TreeViewEventArgs e)
        {
            // The code only executes if the user caused the checked state to change.
            if (e.Action != TreeViewAction.Unknown)
            {
                if (e.Node.Nodes.Count > 0)
                {
                    /* Calls the CheckAllChildNodes method, passing in the current 
                    Checked value of the TreeNode whose checked state changed. */
                    this.CheckAllChildNodes(e.Node, e.Node.Checked);
                }
                else
                {
                    // Viene cambiato anche il nodo dell'albero originale.
                    // NOTA BENE:
                    // - Ogni nodo "copia" della treview filtro punta tramite il campo "Tag" al nodo dell'albero originale
                    // - Ogni nodo "originale" dell'albero originale punta tramite il campo "Tag" ad una tupla nel database.
                    ((TreeNode)e.Node.Tag).Checked = e.Node.Checked;
                }
            }
        }

        private void listBoxStrumentiUtilizzati_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private List<TreeNode> getSelectedNodes(IEnumerable<TreeNode> tnodes)
        {
            List<TreeNode> result = new List<TreeNode>();
            foreach (TreeNode tn in tnodes)
            {
                if (tn.Nodes.Count == 0)
                {
                    if (tn.Checked)
                    {
                        result.Add(tn);
                    }
                }
                else
                {
                    result.AddRange(getSelectedNodes(tn.Nodes.Cast<TreeNode>()));
                }
            }
            return result;
        }
        private void buttonAddStrumento_Click(object sender, EventArgs e)
        {
            List<CATEGORIA> listaCategorie = getSelectedNodes(alberoCategorie).Select(p => p.Tag).Cast<CATEGORIA>().ToList();
            madEngine.getDataContext().STRUMENTO_SUONATO.InsertAllOnSubmit(
                listBoxStrumentiUtilizzati1.AddRange(listaCategorie)
                );
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void textBoxFiltroStrumentiUtilizzati_TextChanged(object sender, EventArgs e)
        {
            listBoxStrumentiUtilizzati1.Filter = textBoxFiltroStrumentiUtilizzati.Text;
        }

        private List<BAND> filterBAND(List<BAND> listaBand, String filter)
        {
            List<BAND> result = new List<BAND>();
            foreach (BAND b in listaBand)
            {
                if (b.ToString().ToLower().Contains(filter.ToLower()))
                {
                    result.Add(b);
                }
            }
            return result;
        } 
        //private void textBoxFiltroBandDisponibili_TextChanged(object sender, EventArgs e)
        //{
        //    List<BAND> tmpList = filterBAND(bandDisponibili, textBoxFiltroBandDisponibili.Text);
        //    checkedListBoxBandDisponibili.Items.Clear();
        //    checkedListBoxBandDisponibili.Items.AddRange(tmpList.ToArray());
        //}

        private bool isBANDUsed(BAND band)
        {
            bool result = false;
            foreach (BAND b in listBoxBandUtilizzate.Items)
            {
                if (b.Equals(band))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        private void checkBoxDataNascita_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerNascita.Enabled = !dateTimePickerNascita.Enabled;
        }

        private void buttonAction_and_new_Click(object sender, EventArgs e)
        {
            bool result = doSaveProcedure();
            if (internalCallType == callType.INSERT_AND_NEW && result)
            {
                madEngine.renewDataContext();
                internalMusicista = new MUSICISTA();

                dateTimePickerNascita.MaxDate = DateTime.Now.AddYears(LOOKFORWARD_BIRTHYEAR);
                internalMusicista.dataNascita = DateTime.Now.AddYears(LOOKFORWARD_BIRTHYEAR).AddHours(-1);
                loadData(internalMusicista);
                fillUI(internalMusicista);
            }
        }

        private void comboBoxProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxProvincia.SelectedIndex != -1)
            {
                comboBoxComune.Items.Clear();
                comboBoxComune.Items.AddRange(madEngine.getComune_ByProvincia(
                    ((PROVINCIA)comboBoxProvincia.SelectedItem).nome).ToArray());
            }
        }
    }
}
