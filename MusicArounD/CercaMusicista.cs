﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace MusicArounD
{
    public partial class CercaMusicista : DBEngineForm
    {
        private List<MUSICISTA> listaMusicisti;
        private string idMusicista = null;
        public CercaMusicista()
        {
            InitializeComponent();
            listaMusicisti = madEngine.getMusicista_byFilter("");
            doSearch();

        }

        private void doSearch()
        {
            //if (textBoxRicercaMusicista.Text.Length == 0)
            //{
            //    dataGridViewMusicista.Rows.Clear();

            //}

            //if (textBoxRicercaMusicista.Text.Length >= 2)
            //{
                string testo = textBoxRicercaMusicista.Text;
                List<MUSICISTA> list = listaMusicisti.Where(c =>
                    c.nome != null && c.nome.ToLower().Contains(testo.ToLower()) ||
                    c.cognome != null && c.cognome.ToLower().Contains(testo.ToLower()) ||
                    c.nomeArte != null && c.nomeArte.ToLower().Contains(testo.ToLower()) ||
                    c.telefono1 != null && c.telefono1.ToLower().Contains(testo.ToLower()) ||
                    c.email != null && c.email.ToLower().Contains(testo.ToLower()) ||
                    c.dataNascita != null && c.dataNascita.Value.ToString().Contains(testo.ToLower()) ||
                    c.comuneMusicista.ToLower().Contains(testo.ToLower()) ||
                    c.provinciaMusicista.ToLower().Equals(testo.ToLower())).ToList();

                dataGridViewMusicista.Rows.Clear();
                foreach (MUSICISTA mus in list)
                {
                    this.dataGridViewMusicista.Rows.Add(false, mus.nome, mus.cognome, mus.nomeArte, mus.email, mus.dataNascita, mus.telefono1,
                        mus.comuneMusicista, mus.provinciaMusicista);
                }
            //}

        }

        private void searchEvent(object sender, EventArgs e)
        {
            doSearch();
        }


        private void btOK_Click(object sender, EventArgs e)
        {
            new ProgressBar().Show();
            MusicistaFrm frm = new MusicistaFrm(idMusicista, true);
            frm.ShowDialog(this);
        }

        private void CercaMusicista_Load(object sender, EventArgs e)
        {
            btOk.Enabled = false;

        }

        private void dataGridViewClickEvent(object sender, DataGridViewCellEventArgs e)
        {
            int count = 0;
            foreach (DataGridViewRow row in dataGridViewMusicista.Rows)
            {

                if (dataGridViewMusicista.IsCurrentCellDirty)
                {
                    dataGridViewMusicista.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }

                if ((Boolean)row.Cells[0].Value == true)
                {
                    idMusicista = (string)row.Cells[4].Value;
                    count = count + 1;

                }

            }
            if (count > 0)
            {
                btOk.Enabled = true;
                if (count > 1)
                {
                    btOk.Enabled = false;
                    MessageBox.Show("Attenzione, è possibile selezionare un solo Musicista", "Scelta Musicista", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                btOk.Enabled = false;
            }

            //if (count > 1)
            //{
            //    btOk.Enabled = false;
            //    MessageBox.Show("Attenzione, è possibile selezionare un solo Musicista", "Scelta Musicista", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
            //else if(count == 1)
            //{
            //    btOk.Enabled = true;
            //}
            //else
            //{
            //    btOk.Enabled = false;
            //}
        }
    }
}
