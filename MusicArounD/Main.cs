﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusicArounD
{
    public partial class Main : DBEngineForm
    {
        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }

        private void nuovoArtistaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void nuovaScuolaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void aggiungiEnteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ente ne = new Ente();
            ne.MdiParent = this;
            ne.Show();
        }

        private void nuovoEnteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ente ne = new Ente();
            ne.MdiParent = this;
            ne.Show();
        }

        private void nuovoMusicistaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MusicistaFrm m = new MusicistaFrm(true);
            m.MdiParent = this;
            m.Show();
            
        }

        private void nuovaBandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BandFrm band = new BandFrm(true);
            band.MdiParent = this;
            band.Show();
        }

        private void storicoBandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //StoricoBand band = new StoricoBand();
            //band.MdiParent = this;
            //band.Show();
        }

        private void musicistiBandToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            madEngine.getUsedBANDs("email@email.it");
        }

        private void cercaMusicistaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CercaMusicista cercaMus = new CercaMusicista();
            cercaMus.MdiParent = this;
            cercaMus.Show();
        }

        private void cercaBandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CercaBand cercaBand = new CercaBand();
            cercaBand.MdiParent = this;
            cercaBand.Show();

        }

    }
}
