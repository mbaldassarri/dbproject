﻿namespace MusicArounD
{
    partial class MusicistaFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanelStrumenti = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxFiltroStrumentiDisponibili = new System.Windows.Forms.TextBox();
            this.treeViewStrumentiDisponibili = new System.Windows.Forms.TreeView();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonAddStrumento = new System.Windows.Forms.Button();
            this.buttonRemoveStrumento = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxFiltroStrumentiUtilizzati = new System.Windows.Forms.TextBox();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.listBoxStrumentiUtilizzati1 = new MusicArounD.ListBoxStrumentiUtilizzati();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanelBand = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxFiltroBandUtilizzate = new System.Windows.Forms.TextBox();
            this.listBoxBandUtilizzate = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonAction = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonAction_and_new = new System.Windows.Forms.Button();
            this.email = new System.Windows.Forms.Label();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxCognome = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxNomeArte = new System.Windows.Forms.TextBox();
            this.dateTimePickerNascita = new System.Windows.Forms.DateTimePicker();
            this.sitoWeb = new System.Windows.Forms.Label();
            this.textBoxSitoWeb = new System.Windows.Forms.TextBox();
            this.telefono1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxTelefono1 = new System.Windows.Forms.TextBox();
            this.textBoxTelefono2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxTelefono3 = new System.Windows.Forms.TextBox();
            this.checkBoxBandSolista = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxComune = new System.Windows.Forms.ComboBox();
            this.checkBoxDataNascita = new System.Windows.Forms.CheckBox();
            this.comboBoxProvincia = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanelStrumenti.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanelBand.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(755, 596);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nuovo musicista";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.tabControl1, 0, 12);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel1, 0, 13);
            this.tableLayoutPanel3.Controls.Add(this.email, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.textBoxEmail, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.textBoxNome, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.textBoxCognome, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.textBoxNomeArte, 1, 6);
            this.tableLayoutPanel3.Controls.Add(this.dateTimePickerNascita, 1, 8);
            this.tableLayoutPanel3.Controls.Add(this.sitoWeb, 0, 10);
            this.tableLayoutPanel3.Controls.Add(this.textBoxSitoWeb, 1, 10);
            this.tableLayoutPanel3.Controls.Add(this.telefono1, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label12, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.textBoxTelefono1, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.textBoxTelefono2, 4, 2);
            this.tableLayoutPanel3.Controls.Add(this.label13, 3, 4);
            this.tableLayoutPanel3.Controls.Add(this.textBoxTelefono3, 4, 4);
            this.tableLayoutPanel3.Controls.Add(this.checkBoxBandSolista, 3, 6);
            this.tableLayoutPanel3.Controls.Add(this.label5, 3, 10);
            this.tableLayoutPanel3.Controls.Add(this.label8, 3, 8);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxComune, 4, 10);
            this.tableLayoutPanel3.Controls.Add(this.checkBoxDataNascita, 0, 8);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxProvincia, 4, 8);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 14;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(749, 577);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.tabControl1, 5);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 221);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(743, 313);
            this.tabControl1.TabIndex = 24;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanelStrumenti);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(735, 287);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Strumenti";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelStrumenti
            // 
            this.tableLayoutPanelStrumenti.ColumnCount = 3;
            this.tableLayoutPanelStrumenti.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelStrumenti.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelStrumenti.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelStrumenti.Controls.Add(this.groupBox2, 0, 0);
            this.tableLayoutPanelStrumenti.Controls.Add(this.tableLayoutPanel6, 1, 0);
            this.tableLayoutPanelStrumenti.Controls.Add(this.groupBox3, 2, 0);
            this.tableLayoutPanelStrumenti.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelStrumenti.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelStrumenti.Name = "tableLayoutPanelStrumenti";
            this.tableLayoutPanelStrumenti.RowCount = 1;
            this.tableLayoutPanelStrumenti.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelStrumenti.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 281F));
            this.tableLayoutPanelStrumenti.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 281F));
            this.tableLayoutPanelStrumenti.Size = new System.Drawing.Size(729, 281);
            this.tableLayoutPanelStrumenti.TabIndex = 12;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel7);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(333, 275);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Strumenti disponibili";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.textBoxFiltroStrumentiDisponibili, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.treeViewStrumentiDisponibili, 0, 2);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(327, 256);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Filtro";
            // 
            // textBoxFiltroStrumentiDisponibili
            // 
            this.textBoxFiltroStrumentiDisponibili.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFiltroStrumentiDisponibili.Location = new System.Drawing.Point(3, 16);
            this.textBoxFiltroStrumentiDisponibili.Name = "textBoxFiltroStrumentiDisponibili";
            this.textBoxFiltroStrumentiDisponibili.Size = new System.Drawing.Size(321, 20);
            this.textBoxFiltroStrumentiDisponibili.TabIndex = 13;
            this.textBoxFiltroStrumentiDisponibili.TextChanged += new System.EventHandler(this.textBoxFiltroStrumentiDisponibili_TextChanged);
            // 
            // treeViewStrumentiDisponibili
            // 
            this.treeViewStrumentiDisponibili.CheckBoxes = true;
            this.treeViewStrumentiDisponibili.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewStrumentiDisponibili.Location = new System.Drawing.Point(3, 42);
            this.treeViewStrumentiDisponibili.Name = "treeViewStrumentiDisponibili";
            this.treeViewStrumentiDisponibili.Size = new System.Drawing.Size(321, 211);
            this.treeViewStrumentiDisponibili.TabIndex = 2;
            this.treeViewStrumentiDisponibili.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeViewStrumentiDisponibili_AfterCheck);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.buttonAddStrumento, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.buttonRemoveStrumento, 0, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(342, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(44, 275);
            this.tableLayoutPanel6.TabIndex = 2;
            // 
            // buttonAddStrumento
            // 
            this.buttonAddStrumento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAddStrumento.AutoSize = true;
            this.buttonAddStrumento.Location = new System.Drawing.Point(3, 107);
            this.buttonAddStrumento.Name = "buttonAddStrumento";
            this.buttonAddStrumento.Size = new System.Drawing.Size(38, 27);
            this.buttonAddStrumento.TabIndex = 14;
            this.buttonAddStrumento.Text = ">>";
            this.buttonAddStrumento.UseVisualStyleBackColor = true;
            this.buttonAddStrumento.Click += new System.EventHandler(this.buttonAddStrumento_Click);
            // 
            // buttonRemoveStrumento
            // 
            this.buttonRemoveStrumento.AutoSize = true;
            this.buttonRemoveStrumento.Location = new System.Drawing.Point(3, 140);
            this.buttonRemoveStrumento.Name = "buttonRemoveStrumento";
            this.buttonRemoveStrumento.Size = new System.Drawing.Size(38, 27);
            this.buttonRemoveStrumento.TabIndex = 1;
            this.buttonRemoveStrumento.Text = "<<";
            this.buttonRemoveStrumento.UseVisualStyleBackColor = true;
            this.buttonRemoveStrumento.Click += new System.EventHandler(this.buttonRemoveStrumento_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel8);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(392, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(334, 275);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Strumenti utilizzati";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.textBoxFiltroStrumentiUtilizzati, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.elementHost1, 0, 2);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 3;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(328, 256);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Filtro";
            // 
            // textBoxFiltroStrumentiUtilizzati
            // 
            this.textBoxFiltroStrumentiUtilizzati.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFiltroStrumentiUtilizzati.Location = new System.Drawing.Point(3, 16);
            this.textBoxFiltroStrumentiUtilizzati.Name = "textBoxFiltroStrumentiUtilizzati";
            this.textBoxFiltroStrumentiUtilizzati.Size = new System.Drawing.Size(322, 20);
            this.textBoxFiltroStrumentiUtilizzati.TabIndex = 15;
            this.textBoxFiltroStrumentiUtilizzati.TextChanged += new System.EventHandler(this.textBoxFiltroStrumentiUtilizzati_TextChanged);
            // 
            // elementHost1
            // 
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(3, 42);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(322, 211);
            this.elementHost1.TabIndex = 2;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = this.listBoxStrumentiUtilizzati1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanelBand);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(735, 287);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Band";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelBand
            // 
            this.tableLayoutPanelBand.ColumnCount = 1;
            this.tableLayoutPanelBand.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelBand.Controls.Add(this.groupBox5, 0, 0);
            this.tableLayoutPanelBand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelBand.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelBand.Name = "tableLayoutPanelBand";
            this.tableLayoutPanelBand.RowCount = 1;
            this.tableLayoutPanelBand.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelBand.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 284F));
            this.tableLayoutPanelBand.Size = new System.Drawing.Size(729, 281);
            this.tableLayoutPanelBand.TabIndex = 15;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tableLayoutPanel11);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(723, 275);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Fai parte di queste band";
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.textBoxFiltroBandUtilizzate, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.listBoxBandUtilizzate, 0, 2);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 3;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(717, 256);
            this.tableLayoutPanel11.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Filtro";
            // 
            // textBoxFiltroBandUtilizzate
            // 
            this.textBoxFiltroBandUtilizzate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFiltroBandUtilizzate.Location = new System.Drawing.Point(3, 16);
            this.textBoxFiltroBandUtilizzate.Name = "textBoxFiltroBandUtilizzate";
            this.textBoxFiltroBandUtilizzate.Size = new System.Drawing.Size(711, 20);
            this.textBoxFiltroBandUtilizzate.TabIndex = 1;
            // 
            // listBoxBandUtilizzate
            // 
            this.listBoxBandUtilizzate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxBandUtilizzate.FormattingEnabled = true;
            this.listBoxBandUtilizzate.Location = new System.Drawing.Point(3, 42);
            this.listBoxBandUtilizzate.Name = "listBoxBandUtilizzate";
            this.listBoxBandUtilizzate.Size = new System.Drawing.Size(711, 211);
            this.listBoxBandUtilizzate.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel3.SetColumnSpan(this.tableLayoutPanel1, 5);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.buttonAction, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonCancel, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonAction_and_new, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 540);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(743, 34);
            this.tableLayoutPanel1.TabIndex = 25;
            // 
            // buttonAction
            // 
            this.buttonAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAction.AutoSize = true;
            this.buttonAction.Location = new System.Drawing.Point(585, 9);
            this.buttonAction.Margin = new System.Windows.Forms.Padding(2);
            this.buttonAction.Name = "buttonAction";
            this.buttonAction.Size = new System.Drawing.Size(75, 23);
            this.buttonAction.TabIndex = 16;
            this.buttonAction.Text = "Action";
            this.buttonAction.UseVisualStyleBackColor = true;
            this.buttonAction.Click += new System.EventHandler(this.buttonAction_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(665, 8);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 17;
            this.buttonCancel.Text = "Cancella";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonAction_and_new
            // 
            this.buttonAction_and_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAction_and_new.AutoSize = true;
            this.buttonAction_and_new.Location = new System.Drawing.Point(480, 8);
            this.buttonAction_and_new.Name = "buttonAction_and_new";
            this.buttonAction_and_new.Size = new System.Drawing.Size(100, 23);
            this.buttonAction_and_new.TabIndex = 18;
            this.buttonAction_and_new.Text = "Action and new...";
            this.buttonAction_and_new.UseVisualStyleBackColor = true;
            this.buttonAction_and_new.Click += new System.EventHandler(this.buttonAction_and_new_Click);
            // 
            // email
            // 
            this.email.AutoSize = true;
            this.email.Location = new System.Drawing.Point(2, 0);
            this.email.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(39, 13);
            this.email.TabIndex = 18;
            this.email.Text = "Email *";
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxEmail.Location = new System.Drawing.Point(106, 3);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(276, 20);
            this.textBoxEmail.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome *";
            // 
            // textBoxNome
            // 
            this.textBoxNome.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxNome.Location = new System.Drawing.Point(106, 39);
            this.textBoxNome.Name = "textBoxNome";
            this.textBoxNome.Size = new System.Drawing.Size(276, 20);
            this.textBoxNome.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Cognome *";
            // 
            // textBoxCognome
            // 
            this.textBoxCognome.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxCognome.Location = new System.Drawing.Point(106, 75);
            this.textBoxCognome.Name = "textBoxCognome";
            this.textBoxCognome.Size = new System.Drawing.Size(276, 20);
            this.textBoxCognome.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Nome d\'arte";
            // 
            // textBoxNomeArte
            // 
            this.textBoxNomeArte.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxNomeArte.Location = new System.Drawing.Point(106, 111);
            this.textBoxNomeArte.Name = "textBoxNomeArte";
            this.textBoxNomeArte.Size = new System.Drawing.Size(276, 20);
            this.textBoxNomeArte.TabIndex = 4;
            // 
            // dateTimePickerNascita
            // 
            this.dateTimePickerNascita.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerNascita.Location = new System.Drawing.Point(106, 147);
            this.dateTimePickerNascita.Name = "dateTimePickerNascita";
            this.dateTimePickerNascita.Size = new System.Drawing.Size(276, 20);
            this.dateTimePickerNascita.TabIndex = 5;
            // 
            // sitoWeb
            // 
            this.sitoWeb.AutoSize = true;
            this.sitoWeb.Location = new System.Drawing.Point(2, 181);
            this.sitoWeb.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sitoWeb.Name = "sitoWeb";
            this.sitoWeb.Size = new System.Drawing.Size(51, 13);
            this.sitoWeb.TabIndex = 22;
            this.sitoWeb.Text = "Sito Web";
            // 
            // textBoxSitoWeb
            // 
            this.textBoxSitoWeb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSitoWeb.Location = new System.Drawing.Point(106, 184);
            this.textBoxSitoWeb.Name = "textBoxSitoWeb";
            this.textBoxSitoWeb.Size = new System.Drawing.Size(276, 20);
            this.textBoxSitoWeb.TabIndex = 6;
            // 
            // telefono1
            // 
            this.telefono1.AutoSize = true;
            this.telefono1.Location = new System.Drawing.Point(407, 0);
            this.telefono1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.telefono1.Name = "telefono1";
            this.telefono1.Size = new System.Drawing.Size(58, 13);
            this.telefono1.TabIndex = 19;
            this.telefono1.Text = "Telefono 1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(407, 36);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "Telefono 2";
            // 
            // textBoxTelefono1
            // 
            this.textBoxTelefono1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTelefono1.Location = new System.Drawing.Point(470, 3);
            this.textBoxTelefono1.Name = "textBoxTelefono1";
            this.textBoxTelefono1.Size = new System.Drawing.Size(276, 20);
            this.textBoxTelefono1.TabIndex = 7;
            // 
            // textBoxTelefono2
            // 
            this.textBoxTelefono2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTelefono2.Location = new System.Drawing.Point(470, 39);
            this.textBoxTelefono2.Name = "textBoxTelefono2";
            this.textBoxTelefono2.Size = new System.Drawing.Size(276, 20);
            this.textBoxTelefono2.TabIndex = 8;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(407, 72);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "Telefono 3";
            // 
            // textBoxTelefono3
            // 
            this.textBoxTelefono3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTelefono3.Location = new System.Drawing.Point(470, 75);
            this.textBoxTelefono3.Name = "textBoxTelefono3";
            this.textBoxTelefono3.Size = new System.Drawing.Size(276, 20);
            this.textBoxTelefono3.TabIndex = 9;
            // 
            // checkBoxBandSolista
            // 
            this.checkBoxBandSolista.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBoxBandSolista.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.checkBoxBandSolista, 2);
            this.checkBoxBandSolista.Location = new System.Drawing.Point(408, 111);
            this.checkBoxBandSolista.Name = "checkBoxBandSolista";
            this.checkBoxBandSolista.Size = new System.Drawing.Size(98, 20);
            this.checkBoxBandSolista.TabIndex = 10;
            this.checkBoxBandSolista.Text = "Sono un solista";
            this.checkBoxBandSolista.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(408, 181);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Comune*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(408, 144);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Provincia*";
            // 
            // comboBoxComune
            // 
            this.comboBoxComune.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxComune.FormattingEnabled = true;
            this.comboBoxComune.Location = new System.Drawing.Point(470, 184);
            this.comboBoxComune.Name = "comboBoxComune";
            this.comboBoxComune.Size = new System.Drawing.Size(276, 21);
            this.comboBoxComune.TabIndex = 12;
            // 
            // checkBoxDataNascita
            // 
            this.checkBoxDataNascita.AutoSize = true;
            this.checkBoxDataNascita.Checked = true;
            this.checkBoxDataNascita.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDataNascita.Location = new System.Drawing.Point(3, 147);
            this.checkBoxDataNascita.Name = "checkBoxDataNascita";
            this.checkBoxDataNascita.Size = new System.Drawing.Size(97, 17);
            this.checkBoxDataNascita.TabIndex = 28;
            this.checkBoxDataNascita.Text = "Data di nascita";
            this.checkBoxDataNascita.UseVisualStyleBackColor = true;
            this.checkBoxDataNascita.CheckedChanged += new System.EventHandler(this.checkBoxDataNascita_CheckedChanged);
            // 
            // comboBoxProvincia
            // 
            this.comboBoxProvincia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxProvincia.FormattingEnabled = true;
            this.comboBoxProvincia.Location = new System.Drawing.Point(470, 147);
            this.comboBoxProvincia.Name = "comboBoxProvincia";
            this.comboBoxProvincia.Size = new System.Drawing.Size(276, 21);
            this.comboBoxProvincia.TabIndex = 29;
            this.comboBoxProvincia.SelectedIndexChanged += new System.EventHandler(this.comboBoxProvincia_SelectedIndexChanged);
            // 
            // MusicistaFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(755, 596);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(770, 595);
            this.Name = "MusicistaFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Musicista";
            this.Load += new System.EventHandler(this.MusicistaFrm_Load);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanelStrumenti.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanelBand.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxCognome;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxNomeArte;
        private System.Windows.Forms.DateTimePicker dateTimePickerNascita;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.TextBox textBoxSitoWeb;
        private System.Windows.Forms.TextBox textBoxTelefono1;
        private System.Windows.Forms.TextBox textBoxTelefono2;
        private System.Windows.Forms.TextBox textBoxTelefono3;
        private System.Windows.Forms.Label email;
        private System.Windows.Forms.Label telefono1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label sitoWeb;
        private System.Windows.Forms.Button buttonAction;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelStrumenti;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button buttonAddStrumento;
        private System.Windows.Forms.Button buttonRemoveStrumento;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxFiltroStrumentiUtilizzati;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TextBox textBoxFiltroStrumentiDisponibili;
        private System.Windows.Forms.TreeView treeViewStrumentiDisponibili;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private ListBoxStrumentiUtilizzati listBoxStrumentiUtilizzati1;
        private System.Windows.Forms.CheckBox checkBoxBandSolista;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelBand;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxFiltroBandUtilizzate;
        private System.Windows.Forms.ListBox listBoxBandUtilizzate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxComune;
        private System.Windows.Forms.CheckBox checkBoxDataNascita;
        private System.Windows.Forms.Button buttonAction_and_new;
        private System.Windows.Forms.ComboBox comboBoxProvincia;
    }
}