using System;
namespace MusicArounD
{
    partial class PERIODO
    {
        public override string ToString()
        {
            return this.dataInizio.ToShortDateString() + " - " + this.dataFine.ToShortDateString();
        }
    }
    partial class PROVINCIA
    {
        public override string ToString()
        {
            return this.nome;
        }
    }
    partial class CATENA
    {
        public override string ToString()
        {
            return this.nome;
        }
    }
    partial class CATEGORIA
    {
        public override string ToString()
        {
            return this.nome;
        }
    }
    partial class DOMINIO_ENTI
    {
        public override string ToString()
        {
            return this.nome + " (" + this.email + ")";
        }
    }

    partial class MUSICISTA
    {
        public override string ToString()
        {
            return (this.nome != null ? this.nome + " " : "") + 
                (this.cognome != null ? this.cognome + " " : "") +
                (this.nomeArte != null ? "(" + this.nomeArte + ")" : "") +
                (!(this.nome == null && this.cognome == null && this.nomeArte == null) ? ", " : "") +
                this.email;
        }
        public bool Equals(MUSICISTA mus)
        {
            //MUSICISTA mus = (MUSICISTA)obj;
            bool bCognome = this.cognome != null && mus.cognome != null ? this.cognome.Equals(mus.cognome) : (this.cognome == null && mus.cognome == null ? true : false);
            bool bNome = this.nome != null && mus.nome != null ? this.nome.Equals(mus.nome) : (this.nome == null && mus.nome == null ? true : false);
            bool bNomeArte = this.nomeArte != null && mus.nomeArte != null ? this.nomeArte.Equals(mus.nomeArte) : (this.nomeArte == null && mus.nomeArte == null ? true : false);
            bool bSitoweb = this.sitoWeb != null && mus.sitoWeb != null ? this.sitoWeb.Equals(mus.sitoWeb) : (this.sitoWeb == null && mus.sitoWeb == null ? true : false);
            bool bEmail = this.email != null && mus.email != null ? this.email.Equals(mus.email) : (this.email == null && mus.email == null ? true : false);
            bool bDataNascita = this.dataNascita != null && mus.dataNascita != null ? this.dataNascita.Equals(mus.dataNascita) : (this.dataNascita == null && mus.dataNascita == null ? true : false);
            bool bTel1 = this.telefono1 != null && mus.telefono1 != null ? this.telefono1.Equals(mus.telefono1) : (this.telefono1 == null && mus.telefono1 == null ? true : false);
            bool bTel2 = this.telefono2 != null && mus.telefono2 != null ? this.telefono2.Equals(mus.telefono2) : (this.telefono2 == null && mus.telefono2 == null ? true : false);
            bool bTel3 = this.telefono3 != null && mus.telefono3 != null ? this.telefono3.Equals(mus.telefono3) : (this.telefono3 == null && mus.telefono3 == null ? true : false);
            bool bProv = this.provinciaMusicista != null && mus.provinciaMusicista != null ? this.provinciaMusicista.Equals(mus.provinciaMusicista) : (this.provinciaMusicista == null && mus.provinciaMusicista == null ? true : false);
            bool bCom = this.comuneMusicista != null && mus.comuneMusicista != null ? this.comuneMusicista.Equals(mus.comuneMusicista) : (this.comuneMusicista == null && mus.comuneMusicista == null ? true : false);


            return
                bCognome &&
                bNome &&
                bNomeArte &&
                bSitoweb &&
                bEmail &&
                bDataNascita &&
                bTel1 &&
                bTel2 &&
                bTel3 &&
                bProv &&
                bCom;
        }
    }

    partial class COMUNE
    {
        public override string ToString()
        {
            return this.nome + " (" + this.idProvincia + ")";
        }
    }
    partial class ORARIO_GIORNALIERO
    {
        private bool isTimeSpanEqualOrNull(TimeSpan? t1, TimeSpan? t2)
        {
            bool result = false;
            if (t1 == null && t2 == null)
            {
                result = true;
            }
            if (t1 != null && t2 != null)
            {
                result = t1.Equals(t2);
            }
            return result;

        }
        public bool EqualsWithoutPK(ORARIO_GIORNALIERO og)
        { 
            bool gs = this.giornoSettimana.Equals(og.giornoSettimana);
            //bool en = this.ente.Equals(og.ente);
            bool o1i = this.ora1_inizio.Equals(og.ora1_inizio);
            bool o1f = this.ora1_fine.Equals(og.ora1_fine);
            bool o2i = isTimeSpanEqualOrNull(this.ora2_inizio, og.ora2_inizio);
            bool o2f = isTimeSpanEqualOrNull(this.ora2_fine,   og.ora2_fine);
            bool o3i = isTimeSpanEqualOrNull(this.ora3_inizio, og.ora3_inizio);
            bool o3f = isTimeSpanEqualOrNull(this.ora3_fine,   og.ora3_fine);

            return 
                gs &&
                //en &&
                o1i && 
                o1f &&
                o2i &&
                o2f &&
                o3i &&
                o3f;
        }
    }
    partial class BAND
    {
        public override string ToString()
        {
            return this.nome + " (" + comuneBand + ", " + provinciaBand + ")";
        }
        public BAND Clone()
        {
            return new BAND
            {
                comuneBand = this.comuneBand,
                dataFormazione = this.dataFormazione,
                idBand = this.idBand,
                musicistaReferente = this.musicistaReferente,
                nome = this.nome,
                nomeReferente = this.nomeReferente,
                provinciaBand = this.provinciaBand,
                solista = this.solista,
                telefonoReferente = this.telefonoReferente
            };
        }
    }


}
