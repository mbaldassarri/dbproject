﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace MusicArounD 
{
    public partial class CercaBand : DBEngineForm
    {
        private List<BAND> listaBand;
        private int id;
        public CercaBand()
        {
            InitializeComponent();
            listaBand = madEngine.getBand_byFilter("");
            doSearch();            
        }
        private void doSearch()
        {
            if (textBoxRicercaBand.Text.Length == 0)
            {
                dataGridViewBand.Rows.Clear();
            }

            if (textBoxRicercaBand.Text.Length >= 2)
            {
                string testo = textBoxRicercaBand.Text;
               
                List<BAND> list = listaBand.Where(c => c.nome.ToLower().Contains(testo.ToLower()) ||
                     c.comuneBand.ToLower().Contains(testo.ToLower())
                    || c.provinciaBand.ToLower().Equals(testo.ToLower()) ).ToList();

                dataGridViewBand.Rows.Clear();
                foreach (BAND b in list)
                {
                    this.dataGridViewBand.Rows.Add(false, b.nome, b.dataFormazione, b.comuneBand, b.provinciaBand, b.musicistaReferente, b.idBand);
                }
             }
        }
        private void searchEvent(object sender, EventArgs e)
        {
            doSearch();
        }


        private void btOKClick(object sender, EventArgs e)
        {
            BandFrm frm = new BandFrm(id, true);
            frm.ShowDialog(this);
        }

        private void CercaMusicista_Load(object sender, EventArgs e)
        {
            btOk.Enabled = false;
            
        }

        private void dataGridViewClickEvent(object sender, DataGridViewCellEventArgs e)
        {
            int count = 0;
            foreach (DataGridViewRow row in dataGridViewBand.Rows)
            {

                if (dataGridViewBand.IsCurrentCellDirty)
                {
                    dataGridViewBand.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
                
                if ((Boolean)row.Cells[0].Value == true)
                {
                   
                    id = (int)row.Cells[6].Value;  
                    count = count + 1;

                }
                
            }
            if (count > 0)
            {
                btOk.Enabled = true;
                if (count > 1)
                {
                    btOk.Enabled = false;
                    MessageBox.Show("Attenzione, è possibile selezionare un solo Musicista", "Scelta Musicista", MessageBoxButtons.OK, MessageBoxIcon.Information);            
                }
            }
            else
            {
                btOk.Enabled = false;
            }
        }

    }
}
