﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MusicArounD
{
    /// <summary>
    /// Logica di interazione per ListBoxStoricoBand.xaml
    /// </summary>
    public partial class ListBoxStoricoBand : UserControl, IInvalidateSort
    {
        private List<StoricoBandItem> fullSet = new List<StoricoBandItem>();
        private String _Filter = "";

        public System.Collections.IList SelectedItems
        {
            get
            {
                return lbStoricoBand.SelectedItems;
            }
        }
        public object SelectedItem
        {
            get
            {
                return lbStoricoBand.SelectedItem;
            }
        }
        public ItemCollection Items
        {
            get
            {
                return lbStoricoBand.Items;
            }
        }
        public List<ENTRATA_USCITA> StoricoItems
        {
            get 
            {
                return fullSet.Select(p => p.entrata_uscita).ToList();
            }
        }
        public List<ENTRATA_USCITA> StoricoSanedItems
        {
            get
            {
                return fullSet.Where(p => p.ToRecreate == false).Select(p => p.entrata_uscita).ToList();
            }
        }
        public List<ENTRATA_USCITA> RecreateItems
        {
            get
            {
                return fullSet.Where(p => p.ToRecreate == true).Select(p => p.entrata_uscita).ToList();
            }
        }

        //public StoricoBandItem getStoricoBandItem(DateTime data, MUSICISTA mus, BAND band)
        //{
        //    return new StoricoBandItem(EU_byMUSICISTA(mus), data, mus, band, this);
        //}

        public void Add(MUSICISTA musicista)
        {
            StoricoBandItem storicoItem = new StoricoBandItem(musicista, this);
            storicoItem.entrata_uscita.e_u = EU_byMUSICISTA(musicista);
            fullSet.Add(storicoItem);
            fullSet = fullSet.OrderBy(p => p.entrata_uscita.data).ToList();
            fixEU(musicista);
            doFilter();

        }
        public void Add(ENTRATA_USCITA entrataUscita)
        {
            StoricoBandItem storicoItem = new StoricoBandItem(entrataUscita, this);
            //storicoItem.entrata_uscita.e_u = EU_byMUSICISTA(entrataUscita.MUSICISTA1);
            fullSet.Add(storicoItem);
            fullSet = fullSet.OrderBy(p => p.entrata_uscita.data).ToList();
            fixEU(entrataUscita.MUSICISTA1);
            doFilter();
        }

        public void AddRange(List<MUSICISTA> listaMus)
        {
            foreach (MUSICISTA musicista in listaMus)
            {
                Add(musicista);
            }
        }
        public void AddRange(List<ENTRATA_USCITA> listaEU)
        {
            foreach (ENTRATA_USCITA entrataUscita in listaEU)
            {
                Add(entrataUscita);
            }
        }

        public void Remove(object item)
        {
            List<StoricoBandItem> lista;
            StoricoBandItem removeItem = (StoricoBandItem) item;
            
            MUSICISTA mus = removeItem.entrata_uscita.MUSICISTA1;
            lbStoricoBand.Items.Remove(removeItem);
            fullSet.Remove(removeItem);
            lista = getStoricoBand_byMUSICISTA(mus);
            if (lista.Count > 0)
            {
                lista.ElementAt(lista.Count - 1).EnabledCheck = true;
            }
        }
        public void Clear()
        {
            fullSet.Clear();
            lbStoricoBand.Items.Clear();
            _Filter = "";
        }
        public String Filter
        {
            get
            {
                return _Filter;
            }
            set
            {
                this._Filter = value;
                doFilter();
            }
        }
        public List<MUSICISTA> getFormazioneAttiva()
        {
            List<MUSICISTA> result = new List<MUSICISTA>();
            List<MUSICISTA> listaMusicisti = fullSet.Select(p => p.entrata_uscita.MUSICISTA1).Distinct().ToList();
            foreach (MUSICISTA mus in listaMusicisti)
            {
                if (fullSet.Where(p => p.entrata_uscita.MUSICISTA1.Equals(mus)).Count() % 2 != 0)
                {
                    result.Add(mus);
                }
            }
            return result;
        }

        private void fixEU()
        {
            List<MUSICISTA> listaMusicisti = fullSet.Select(p => p.entrata_uscita.MUSICISTA1).Distinct().ToList();
            foreach (MUSICISTA mus in listaMusicisti)
            {
                fixEU(mus);
            }
        }
        private void fixEU(MUSICISTA mus)
        {
            List<StoricoBandItem> lista = getStoricoBand_byMUSICISTA(mus); ;

            for (int k = 0; k < lista.Count; k++)
            {
                if (lista.ElementAt(k).entrata_uscita.MUSICISTA1.Equals(mus))
                {
                    lista.ElementAt(k).EnabledCheck = false;
                    lista.ElementAt(k).ToRemove  = false;
                    lista.ElementAt(k).entrata_uscita.e_u = (k % 2 == 0);
                }
            }
            lista.ElementAt(lista.Count - 1).EnabledCheck = true;

        }
        private void doFilter() {
            lbStoricoBand.Items.Clear();
            foreach (StoricoBandItem sb in fullSet)
            {
                if (sb.Title.ToLower().Contains(Filter.ToLower()))
                {
                    lbStoricoBand.Items.Add(sb);
                }
            }
        }
        private bool EU_byMUSICISTA(MUSICISTA mus)
        {
            int counter = 0; 
            foreach (StoricoBandItem storico in fullSet)
            {
                if (storico.entrata_uscita.MUSICISTA1.Equals(mus)) counter++;

            }
            return counter % 2 == 0;
        }
        private List<StoricoBandItem> getStoricoBand_byMUSICISTA(MUSICISTA filter)
        {
            List<StoricoBandItem> result = new List<StoricoBandItem>();
            foreach (StoricoBandItem stor in fullSet)
            {
                if (stor.entrata_uscita.MUSICISTA1.Equals(filter))
                {
                    result.Add(stor);
                }
            }
            return result;
        }
        public List<ENTRATA_USCITA> getENTRATA_USCITAs_byMUSICISTA(MUSICISTA musicista)
        {
            return getStoricoBand_byMUSICISTA(musicista).Select(p => p.entrata_uscita).ToList();
        }
        public ListBoxStoricoBand()
        {
            InitializeComponent();
        }

        public void doInvalidateSort()
        {
            fullSet = fullSet.OrderBy(p => p.entrata_uscita.data).ToList();
            fixEU();
            doFilter();
        }
    }
    public class StoricoBandItem : INotifyPropertyChanged
    {
        public ENTRATA_USCITA entrata_uscita;
        private IInvalidateSort listbox;

        private const int LOOKFORWARD_MINYEAR = -100;
        public StoricoBandItem(MUSICISTA mus, IInvalidateSort listbox)
        {
            this.entrata_uscita = new ENTRATA_USCITA
            {
                MUSICISTA1 = mus
            };
            this.listbox = listbox;
            this.entrata_uscita.data = DateTime.Now;
            this.ToRecreate = false;
        }
        public StoricoBandItem(ENTRATA_USCITA entrata_uscita, IInvalidateSort listbox)
        {
            this.entrata_uscita = entrata_uscita;
            this.listbox = listbox;
            this.ToRecreate = false;
        }
        public string Title
        {
            get
            {
                //return entrata_uscita.MUSICISTA1.cognome + " " + entrata_uscita.MUSICISTA1.nome + " (" + entrata_uscita.MUSICISTA1.nomeArte + ")" + 
                //    (entrata_uscita.BAND != null ? " - " + entrata_uscita.BAND.nome : "");
                return entrata_uscita.MUSICISTA1.ToString();
            }
        }
        public DateTime DataEU
        {
            get
            {
                return entrata_uscita.data;
            }
            set
            {
                DateTime tmpData = value;
                if (tmpData > DateTime.Now)
                {
                    entrata_uscita.data = DateTime.Now;
                }
                else
                {
                    if (entrata_uscita.MUSICISTA1.dataNascita != null && tmpData < entrata_uscita.MUSICISTA1.dataNascita)
                    {
                        entrata_uscita.data = entrata_uscita.MUSICISTA1.dataNascita.Value;
                    }
                    else
                    {
                        if (tmpData < DateTime.Now.AddYears(LOOKFORWARD_MINYEAR))
                        {
                            entrata_uscita.data = DateTime.Now.AddYears(LOOKFORWARD_MINYEAR);
                        }
                        else
                        {
                            entrata_uscita.data = value;
                        }

                    }
                }
                OnPropertyChanged("DataEU");
                listbox.doInvalidateSort();
            }
        }
        public string TipoEU
        {
            get
            {
                return entrata_uscita.e_u ? "Entrata:" : "Uscita:";
            }
        }
        public bool ToRemove { get; set; }
        public bool ToRecreate { get; set; }
        private bool _EnabledCheck;
        public bool EnabledCheck {
            get
            {
                return _EnabledCheck;
            }
            set {
                _EnabledCheck = value;
                OnPropertyChanged("EnabledCheck"); 
            }
        }

       public event PropertyChangedEventHandler PropertyChanged;  
       public void OnPropertyChanged(string Property)  
       {
           if (Property.Equals("DataEU") && this.entrata_uscita.BAND != null)
           {
               this.ToRecreate = true;
           }
           if (PropertyChanged != null)  
           {  
               PropertyChanged(this, new PropertyChangedEventArgs(Property));  
           }  
       }  
    }
    public interface IInvalidateSort
    {
        void doInvalidateSort();
    }
}
